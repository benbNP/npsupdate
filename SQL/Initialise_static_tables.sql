﻿USE [NPSUpdate]
GO
/****** Object:  Table [dbo].[StatsForLatestRun]    Script Date: 25/06/2021 13:34:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StatsForLatestRun](
	[StatID] [int] NOT NULL,
	[StatDescription] [nvarchar](250) NULL,
	[Value] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TitleChangeType]    Script Date: 25/06/2021 13:34:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleChangeType](
	[ChangeTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ChangeTypeDescription] [varchar](150) NOT NULL,
 CONSTRAINT [PK_TitleChangeType] PRIMARY KEY CLUSTERED 
(
	[ChangeTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (1, N'Titles changed in [TitleInfo]', 79631)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (2, N'Titles added in [TitleInfo]', 39138)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (3, N'Titles deleted in [TitleInfo]', 13132)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (4, N'Existing Land Registry TitleNumber/UPRN records deleted', 17829)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (5, N'Existing Land Registry TitleNumber/UPRN records added', 39848)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (6, N'TitleNumber/UPRNs with a delivery point', 41741124)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (7, N'Polygons skipped as data is older than current Nimbus Polygon data', 0)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (8, N'Invalid polygons detected and corrected by SQL', 3229)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (9, N'Multi polygons detected by SQL', 23)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (10, N'New polygons created from exploded multi-polygons', 48)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (11, N'Polygons changed in [Polygon]', 12450)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (12, N'Polygons added in [Polygon]', 35079)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (13, N'Polygons deleted in [Polygon]', 19910)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (15, N'Polygons with unreadable geography skipped', 2)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (16, N'Orphan polygons detected', 4)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (17, N'Duplicates within individual shapefiles', 0)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (18, N'Polygons Received', 67431)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (19, N'Leasehold spatial matches added', 1392)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (20, N'Freehold spatial matches added', 4304)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (21, N'Duplicate polygons removed', 2)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (22, N'Existing leashold spatial matches deleted', 196539)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (23, N'Existing freehold spatial matches deleted', 285563)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (24, N'Freehold to leashold spatial matches added', 113444)
GO
INSERT [dbo].[StatsForLatestRun] ([StatID], [StatDescription], [Value]) VALUES (25, N'Freehold to leashold spatial matches added', 115520)
GO
SET IDENTITY_INSERT [dbo].[TitleChangeType] ON 
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (1, N'Title added (COU)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (2, N'Title deleted')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (3, N'Title changed')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (4, N'Polygon(s) added (COU)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (5, N'Polygon(s) deleted')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (6, N'Polygon(s) changed')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (7, N'LR (Source 1) UPRN match added (COU)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (8, N'LR (Source 1) UPRN match deleted')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (9, N'Source 3 UPRN freehold spatial match added')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (10, N'Source 3 UPRN leasehold spatial match added')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (11, N'Source 3 UPRN freehold spatial match deleted')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (12, N'Source 3 UPRN leasehold spatial match deleted')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (13, N'Title added (full rebuild)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (14, N'Polygon added (full rebuild)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (15, N'LR (Source 1) UPRN match added (full rebuild)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (16, N'VOA-ChangeCode:1 - DEL - Demolished')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (17, N'VOA-ChangeCode:2 - DEL - Reconstituted')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (18, N'VOA-ChangeCode:3 - DEL - Exempt from rating')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (19, N'VOA-ChangeCode:4 - DEL - Ceased to be relevant')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (20, N'VOA-ChangeCode:5 - DEL - Boundary change')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (21, N'VOA-ChangeCode:6 - DEL - Address change and review of assessment')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (22, N'VOA-ChangeCode:7 - DEL - Deletion of draft list assessment')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (23, N'VOA-ChangeCode:8 - DEL - Reconstituted REVAL assessment (deletion)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (24, N'VOA-ChangeCode:9 - DEL - Other (deletion)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (25, N'VOA-ChangeCode:10 - ADD - New hereditament')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (26, N'VOA-ChangeCode:11 - ADD - Reconstituted')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (27, N'VOA-ChangeCode:12 - ADD - Ceased to be exempt')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (28, N'VOA-ChangeCode:13 - ADD - Boundary change')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (29, N'VOA-ChangeCode:14 - ADD - Address change and review of assessment')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (30, N'VOA-ChangeCode:17 - ADD - Reconstituted REVAL assessment (new)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (31, N'VOA-ChangeCode:19 - ADD - Other (new)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (32, N'VOA-ChangeCode:20 - UPD - Alteration of a non-deleted assessment')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (33, N'VOA-ChangeCode:21 - UPD - Alteration of effective date only (non-deleted assessment)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (34, N'VOA-ChangeCode:26 - UPD - Amend draft list assessment')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (35, N'VOA-ChangeCode:28 - UPD - Correct new entry REVAL assessment')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (36, N'VOA-ChangeCode:29 - UPD - Other (alteration non-deleted assessment)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (37, N'VOA-ChangeCode:31 - UPD - Alteration of a deleted assessment (effective date only)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (38, N'VOA-ChangeCode:99 - ADD - Record imported as part of a full Baseline rebuild (Nimbus custom code)')
GO
INSERT [dbo].[TitleChangeType] ([ChangeTypeID], [ChangeTypeDescription]) VALUES (39, N'VOA ')
GO
SET IDENTITY_INSERT [dbo].[TitleChangeType] OFF
GO
