﻿USE [master]
GO
/****** Object:  Database [NationalPolygon]    Script Date: 25/06/2021 16:43:25 ******/
CREATE DATABASE [NationalPolygon]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NationalPolygonProcessing', FILENAME = N'E:\Data\NationalPolygon14.mdf' , SIZE = 436981568KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'NationalPolygonProcessing_log', FILENAME = N'F:\Logs\NationalPolygon14_log.ldf' , SIZE = 80839616KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
GO
ALTER DATABASE [NationalPolygon] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [NationalPolygon].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [NationalPolygon] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [NationalPolygon] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [NationalPolygon] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [NationalPolygon] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [NationalPolygon] SET ARITHABORT OFF 
GO
ALTER DATABASE [NationalPolygon] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [NationalPolygon] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [NationalPolygon] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [NationalPolygon] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [NationalPolygon] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [NationalPolygon] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [NationalPolygon] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [NationalPolygon] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [NationalPolygon] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [NationalPolygon] SET  DISABLE_BROKER 
GO
ALTER DATABASE [NationalPolygon] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [NationalPolygon] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [NationalPolygon] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [NationalPolygon] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [NationalPolygon] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [NationalPolygon] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [NationalPolygon] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [NationalPolygon] SET RECOVERY FULL 
GO
ALTER DATABASE [NationalPolygon] SET  MULTI_USER 
GO
ALTER DATABASE [NationalPolygon] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [NationalPolygon] SET DB_CHAINING OFF 
GO
ALTER DATABASE [NationalPolygon] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [NationalPolygon] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [NationalPolygon] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'NationalPolygon', N'ON'
GO
ALTER DATABASE [NationalPolygon] SET QUERY_STORE = OFF
GO
USE [NationalPolygon]
GO
/****** Object:  User [WebUser]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [WebUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [VOAUpdate]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [VOAUpdate] FOR LOGIN [VOAUpdate] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [SimonSeoss]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [SimonSeoss] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [NPSUpdate]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [NPSUpdate] FOR LOGIN [NPSUpdate] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MAC\Shanaka Perera]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [MAC\Shanaka Perera] FOR LOGIN [MAC\Shanaka Perera] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MAC\Nicholas.Bolitho]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [MAC\Nicholas.Bolitho] FOR LOGIN [MAC\Nicholas.Bolitho] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MAC\Joseph.Mhindurwa]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [MAC\Joseph.Mhindurwa] FOR LOGIN [MAC\Joseph.Mhindurwa] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MAC\Iain.Fletcher]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [MAC\Iain.Fletcher] FOR LOGIN [MAC\Iain.Fletcher] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MAC\Christopher.Mountfor]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [MAC\Christopher.Mountfor] FOR LOGIN [MAC\Christopher.Mountfor] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ChrisSeoss]    Script Date: 25/06/2021 16:43:25 ******/
CREATE USER [ChrisSeoss] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [WebUser]
GO
ALTER ROLE [db_datareader] ADD MEMBER [VOAUpdate]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [VOAUpdate]
GO
ALTER ROLE [db_datareader] ADD MEMBER [SimonSeoss]
GO
ALTER ROLE [db_datareader] ADD MEMBER [NPSUpdate]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [NPSUpdate]
GO
ALTER ROLE [db_owner] ADD MEMBER [MAC\Shanaka Perera]
GO
ALTER ROLE [db_owner] ADD MEMBER [MAC\Nicholas.Bolitho]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MAC\Nicholas.Bolitho]
GO
ALTER ROLE [db_owner] ADD MEMBER [MAC\Joseph.Mhindurwa]
GO
ALTER ROLE [db_owner] ADD MEMBER [MAC\Iain.Fletcher]
GO
ALTER ROLE [db_owner] ADD MEMBER [MAC\Christopher.Mountfor]
GO
ALTER ROLE [db_datareader] ADD MEMBER [ChrisSeoss]
GO
/****** Object:  UserDefinedTableType [dbo].[AncientWoodlandPolygonImport]    Script Date: 25/06/2021 16:43:26 ******/
CREATE TYPE [dbo].[AncientWoodlandPolygonImport] AS TABLE(
	[ThemeId] [int] NULL,
	[Name] [varchar](50) NULL,
	[ThemeName] [varchar](50) NULL,
	[Status] [varchar](4) NULL,
	[UpdateStatus] [varchar](15) NULL,
	[GridRef] [varchar](10) NULL,
	[PolygonGeography] [varchar](max) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[AONBPolygonImport]    Script Date: 25/06/2021 16:43:26 ******/
CREATE TYPE [dbo].[AONBPolygonImport] AS TABLE(
	[Name] [varchar](50) NOT NULL,
	[DateDesignated] [date] NOT NULL,
	[Link] [varchar](100) NOT NULL,
	[PolygonGeography] [varchar](max) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[FloodZonePolygonImport]    Script Date: 25/06/2021 16:43:26 ******/
CREATE TYPE [dbo].[FloodZonePolygonImport] AS TABLE(
	[Layer] [varchar](20) NOT NULL,
	[FloodType] [varchar](30) NOT NULL,
	[PolygonGeography] [varchar](max) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[GreenbeltPolygonImport]    Script Date: 25/06/2021 16:43:26 ******/
CREATE TYPE [dbo].[GreenbeltPolygonImport] AS TABLE(
	[PolygonId] [int] NOT NULL,
	[GreenbeltName] [varchar](50) NOT NULL,
	[LocalAuthorityName] [varchar](50) NOT NULL,
	[ONSCode] [varchar](9) NOT NULL,
	[PolygonGeography] [varchar](max) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MatchedUprnPolygonIdImport]    Script Date: 25/06/2021 16:43:26 ******/
CREATE TYPE [dbo].[MatchedUprnPolygonIdImport] AS TABLE(
	[uprn] [bigint] NOT NULL,
	[polygonId] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PolygonImport]    Script Date: 25/06/2021 16:43:26 ******/
CREATE TYPE [dbo].[PolygonImport] AS TABLE(
	[PolygonID] [bigint] NOT NULL,
	[TitleNumber] [varchar](9) NOT NULL,
	[Area] [float] NULL,
	[Polygon] [varchar](max) NOT NULL,
	[SingleEasting] [float] NULL,
	[SingleNorthing] [float] NULL,
	[PolygonGeography] [varchar](max) NOT NULL,
	[LatLng] [varchar](max) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[UnmatchedUprnImport]    Script Date: 25/06/2021 16:43:26 ******/
CREATE TYPE [dbo].[UnmatchedUprnImport] AS TABLE(
	[uprn] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[CreatePolygonFromENMaxMins]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CreatePolygonFromENMaxMins]
(
    @EastingMin FLOAT,
    @EastingMax FLOAT,
    @NorthingMin FLOAT,
    @NorthingMax FLOAT
)
RETURNS GEOGRAPHY
AS
BEGIN

--create four corners
DECLARE @nw GEOGRAPHY = (
                            SELECT dbo.NEtoLL(@EastingMin, @NorthingMax)
                        );
DECLARE @ne GEOGRAPHY = (
                            SELECT dbo.NEtoLL(@EastingMax, @NorthingMax)
                        );
DECLARE @se GEOGRAPHY = (
                            SELECT dbo.NEtoLL(@EastingMax, @NorthingMin)
                        );
DECLARE @sw GEOGRAPHY = (
                            SELECT dbo.NEtoLL(@EastingMin, @NorthingMin)
                        );

						DECLARE @BuildString NVARCHAR(MAX)

						SELECT @BuildString = 'POLYGON(('
						
						SELECT @BuildString = @BuildString + CAST(@nw.Long AS nvarchar(50)) + ' ' + CAST(@nw.Lat AS nvarchar(50))
						SELECT @BuildString = @BuildString + ',' + CAST(@sw.Long AS nvarchar(50)) + ' ' + CAST(@sw.Lat AS nvarchar(50))
						SELECT @BuildString = @BuildString + ',' + CAST(@se.Long AS nvarchar(50)) + ' ' + CAST(@se.Lat AS nvarchar(50))
						SELECT @BuildString = @BuildString + ',' + CAST(@ne.Long AS nvarchar(50)) + ' ' + CAST(@ne.Lat AS nvarchar(50))
						SELECT @BuildString = @BuildString + ',' + CAST(@nw.Long AS nvarchar(50)) + ' ' + CAST(@nw.Lat AS nvarchar(50))

						SELECT @BuildString = @BuildString + '))'

						DECLARE @poly GEOGRAPHY = geography::STPolyFromText(@BuildString, 4326)

						RETURN @poly


--DECLARE @BuildString NVARCHAR(MAX)
--SELECT @BuildString = COALESCE(@BuildString + ',', '') + CAST([Longitude] AS NVARCHAR(50)) + ' ' + CAST([Latitude] AS NVARCHAR(50))
--FROM dbo.LongAndLats
--ORDER BY SortOrder

--SET @BuildString = 'POLYGON((' + @BuildString + '))';  
--DECLARE @PolygonFromPoints geography = geography::STPolyFromText(@BuildString, 4326);
--SELECT @PolygonFromPoints

    --DECLARE @corners TABLE (corner GEOGRAPHY);
    --INSERT @corners ( corner ) VALUES (@nw),(@ne),(@se),(@sw);

    --DECLARE @centroid GEOGRAPHY = (
    --                                  SELECT geography::UnionAggregate(corner).EnvelopeCenter() FROM @corners
    --                              );

    --RETURN @centroid;

END;


GO
/****** Object:  UserDefinedFunction [dbo].[CreatePolygonGeometryFromENMaxMins]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CreatePolygonGeometryFromENMaxMins]
(
    @EastingMin FLOAT,
    @EastingMax FLOAT,
    @NorthingMin FLOAT,
    @NorthingMax FLOAT
)
RETURNS GEOMETRY
AS
BEGIN

--create four corners
DECLARE @nw GEOMETRY = (
                            SELECT dbo.NEtoLLGeometry(@EastingMin, @NorthingMax)
                        );
DECLARE @ne GEOMETRY = (
                            SELECT dbo.NEtoLLGeometry(@EastingMax, @NorthingMax)
                        );
DECLARE @se GEOMETRY = (
                            SELECT dbo.NEtoLLGeometry(@EastingMax, @NorthingMin)
                        );
DECLARE @sw GEOMETRY = (
                            SELECT dbo.NEtoLLGeometry(@EastingMin, @NorthingMin)
                        );

						DECLARE @BuildString NVARCHAR(MAX)

						SELECT @BuildString = 'POLYGON(('
						
						SELECT @BuildString = @BuildString + CAST(@nw.STX AS nvarchar(50)) + ' ' + CAST(@nw.STY AS nvarchar(50))
						SELECT @BuildString = @BuildString + ',' + CAST(@sw.STX AS nvarchar(50)) + ' ' + CAST(@sw.STY AS nvarchar(50))
						SELECT @BuildString = @BuildString + ',' + CAST(@se.STX AS nvarchar(50)) + ' ' + CAST(@se.STY AS nvarchar(50))
						SELECT @BuildString = @BuildString + ',' + CAST(@ne.STX AS nvarchar(50)) + ' ' + CAST(@ne.STY AS nvarchar(50))
						SELECT @BuildString = @BuildString + ',' + CAST(@nw.STX AS nvarchar(50)) + ' ' + CAST(@nw.STY AS nvarchar(50))

						SELECT @BuildString = @BuildString + '))'

						DECLARE @poly GEOMETRY = GEOMETRY::STPolyFromText(@BuildString, 4326)

						RETURN @poly


--DECLARE @BuildString NVARCHAR(MAX)
--SELECT @BuildString = COALESCE(@BuildString + ',', '') + CAST([Longitude] AS NVARCHAR(50)) + ' ' + CAST([Latitude] AS NVARCHAR(50))
--FROM dbo.LongAndLats
--ORDER BY SortOrder

--SET @BuildString = 'POLYGON((' + @BuildString + '))';  
--DECLARE @PolygonFromPoints geography = geography::STPolyFromText(@BuildString, 4326);
--SELECT @PolygonFromPoints

    --DECLARE @corners TABLE (corner GEOGRAPHY);
    --INSERT @corners ( corner ) VALUES (@nw),(@ne),(@se),(@sw);

    --DECLARE @centroid GEOGRAPHY = (
    --                                  SELECT geography::UnionAggregate(corner).EnvelopeCenter() FROM @corners
    --                              );

    --RETURN @centroid;

END;



GO
/****** Object:  UserDefinedFunction [dbo].[DetermineCentroidFromENMaxMins]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[DetermineCentroidFromENMaxMins]
(
    @EastingMin FLOAT,
    @EastingMax FLOAT,
    @NorthingMin FLOAT,
    @NorthingMax FLOAT
)
RETURNS GEOGRAPHY
AS
BEGIN

--create four corners
DECLARE @nw GEOGRAPHY = (
                            SELECT dbo.NEtoLL(@EastingMin, @NorthingMax)
                        );
DECLARE @ne GEOGRAPHY = (
                            SELECT dbo.NEtoLL(@EastingMax, @NorthingMax)
                        );
DECLARE @se GEOGRAPHY = (
                            SELECT dbo.NEtoLL(@EastingMax, @NorthingMin)
                        );
DECLARE @sw GEOGRAPHY = (
                            SELECT dbo.NEtoLL(@EastingMin, @NorthingMin)
                        );

    DECLARE @corners TABLE (corner GEOGRAPHY);
    INSERT @corners ( corner ) VALUES (@nw),(@ne),(@se),(@sw);

    DECLARE @centroid GEOGRAPHY = (
                                      SELECT geography::UnionAggregate(corner).EnvelopeCenter() FROM @corners
                                  );

    RETURN @centroid;

END;

GO
/****** Object:  UserDefinedFunction [dbo].[NEtoLL]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[NEtoLL] (@East INT, @North INT) RETURNS GEOGRAPHY AS
BEGIN

-- Adapted from: https://stackoverflow.com/questions/4107806/sql-function-to-convert-uk-os-coordinates-from-easting-northing-to-longitude-and
-- Changed to return a geography type

--Author: Sandy Motteram
--Date:   06 September 2012

--UDF adapted from javascript at http://www.bdcc.co.uk/LatLngToOSGB.js
--found on page http://mapki.com/wiki/Tools:Snippets

--Instructions:
--Latitude and Longitude are calculated based on BOTH the easting and northing values from the OSGB36
--This UDF takes both easting and northing values in OSGB36 projection and you must specify if a latitude or longitude co-ordinate should be returned.
--IT first converts E/N values to lat and long in OSGB36 projection, then converts those values to lat/lng in WGS84 projection

--Sample values below
--DECLARE @East INT, @North INT, @LatOrLng VARCHAR(3)
--SELECT @East = 529000, @North = 183650 --that combo should be the corner of Camden High St and Delancey St


    DECLARE @Pi              FLOAT
          , @K0              FLOAT
          , @OriginLat       FLOAT
          , @OriginLong      FLOAT
          , @OriginX         FLOAT
          , @OriginY         FLOAT
          , @a               FLOAT
          , @b               FLOAT
          , @e2              FLOAT
          , @ex              FLOAT
          , @n1              FLOAT
          , @n2              FLOAT
          , @n3              FLOAT
          , @OriginNorthings FLOAT
          , @lat             FLOAT
          , @lon             FLOAT
          , @Northing        FLOAT
          , @Easting         FLOAT

    SELECT  @Pi = 3.14159265358979323846
          , @K0 = 0.9996012717 -- grid scale factor on central meridean
          , @OriginLat  = 49.0
          , @OriginLong = -2.0
          , @OriginX =  400000 -- 400 kM
          , @OriginY = -100000 -- 100 kM
          , @a = 6377563.396   -- Airy Spheroid
          , @b = 6356256.910
    /*    , @e2
          , @ex
          , @n1
          , @n2
          , @n3
          , @OriginNorthings*/

    -- compute interim values
    SELECT  @a = @a * @K0
          , @b = @b * @K0

    SET     @n1 = (@a - @b) / (@a + @b)
    SET     @n2 = @n1 * @n1
    SET     @n3 = @n2 * @n1

    SET     @lat = @OriginLat * @Pi / 180.0 -- to radians

    SELECT  @e2 = (@a * @a - @b * @b) / (@a * @a) -- first eccentricity
          , @ex = (@a * @a - @b * @b) / (@b * @b) -- second eccentricity

    SET     @OriginNorthings = @b * @lat + @b * (@n1 * (1.0 + 5.0 * @n1 * (1.0 + @n1) / 4.0) * @lat
          - 3.0 * @n1 * (1.0 + @n1 * (1.0 + 7.0 * @n1 / 8.0)) * SIN(@lat) * COS(@lat)
          + (15.0 * @n1 * (@n1 + @n2) / 8.0) * SIN(2.0 * @lat) * COS(2.0 * @lat)
          - (35.0 * @n3 / 24.0) * SIN(3.0 * @lat) * COS(3.0 * @lat))

    SELECT  @northing = @north - @OriginY
         ,  @easting  = @east  - @OriginX

    DECLARE @nu       FLOAT
          , @phid     FLOAT
          , @phid2    FLOAT
          , @t2       FLOAT
          , @t        FLOAT
          , @q2       FLOAT
          , @c        FLOAT
          , @s        FLOAT
          , @nphid    FLOAT
          , @dnphid   FLOAT
          , @nu2      FLOAT
          , @nudivrho FLOAT
          , @invnurho FLOAT
          , @rho      FLOAT
          , @eta2     FLOAT

    /* Evaluate M term: latitude of the northing on the centre meridian */

    SET     @northing = @northing + @OriginNorthings

    SET     @phid  = @northing / (@b*(1.0 + @n1 + 5.0 * (@n2 + @n3) / 4.0)) - 1.0
    SET     @phid2 = @phid + 1.0

    WHILE (ABS(@phid2 - @phid) > 0.000001)
    BEGIN
        SET @phid = @phid2;
        SET @nphid = @b * @phid + @b * (@n1 * (1.0 + 5.0 * @n1 * (1.0 + @n1) / 4.0) * @phid
                   - 3.0 * @n1 * (1.0 + @n1 * (1.0 + 7.0 * @n1 / 8.0)) * SIN(@phid) * COS(@phid)
                   + (15.0 * @n1 * (@n1 + @n2) / 8.0) * SIN(2.0 * @phid) * COS(2.0 * @phid)
                   - (35.0 * @n3 / 24.0) * SIN(3.0 * @phid) * COS(3.0 * @phid))

        SET @dnphid = @b * ((1.0 + @n1 + 5.0 * (@n2 + @n3) / 4.0) - 3.0 * (@n1 + @n2 + 7.0 * @n3 / 8.0) * COS(2.0 * @phid)
                    + (15.0 * (@n2 + @n3) / 4.0) * COS(4 * @phid) - (35.0 * @n3 / 8.0) * COS(6.0 * @phid))

        SET @phid2 = @phid - (@nphid - @northing) / @dnphid
    END

    SELECT @c = COS(@phid)
         , @s = SIN(@phid)
         , @t = TAN(@phid)
    SELECT @t2 = @t * @t
         , @q2 = @easting * @easting

    SET    @nu2 = (@a * @a) / (1.0 - @e2 * @s * @s)
    SET    @nu = SQRT(@nu2)

    SET    @nudivrho = @a * @a * @c * @c / (@b * @b) - @c * @c + 1.0
    SET    @eta2 = @nudivrho - 1
    SET    @rho = @nu / @nudivrho;

    SET    @invnurho = ((1.0 - @e2 * @s * @s) * (1.0 - @e2 * @s * @s)) / (@a * @a * (1.0 - @e2))

    SET    @lat = @phid - @t * @q2 * @invnurho / 2.0 + (@q2 * @q2 * (@t / (24 * @rho * @nu2 * @nu) * (5 + (3 * @t2) + @eta2 - (9 * @t2 * @eta2))))
    SET    @lon = (@easting / (@c * @nu))
                - (@easting * @q2 * ((@nudivrho + 2.0 * @t2) / (6.0 * @nu2)) / (@c * @nu))
                + (@q2 * @q2 * @easting * (5 + (28 * @t2) + (24 * @t2 * @t2)) / (120 * @nu2 * @nu2 * @nu * @c))


    SELECT @lat = @lat * 180.0 / @Pi
         , @lon = @lon * 180.0 / @Pi + @OriginLong


--Now convert the lat and long from OSGB36 to WGS84

    DECLARE @OGlat  FLOAT
          , @OGlon  FLOAT
          , @height FLOAT

    SELECT  @OGlat  = @lat
          , @OGlon  = @lon
          , @height = 24 --London's mean height above sea level is 24 metres. Adjust for other locations.

    DECLARE @deg2rad  FLOAT
          , @rad2deg  FLOAT
          , @radOGlat FLOAT
          , @radOGlon FLOAT

    SELECT  @deg2rad = @Pi / 180
          , @rad2deg = 180 / @Pi

    --first off convert to radians
    SELECT  @radOGlat = @OGlat * @deg2rad
          , @radOGlon = @OGlon * @deg2rad
    --these are the values for WGS84(GRS80) to OSGB36(Airy) 

    DECLARE @a2       FLOAT
          , @h        FLOAT
          , @xp       FLOAT
          , @yp       FLOAT
          , @zp       FLOAT
          , @xr       FLOAT
          , @yr       FLOAT
          , @zr       FLOAT
          , @sf       FLOAT
          , @e        FLOAT
          , @v        FLOAT
          , @x        FLOAT
          , @y        FLOAT
          , @z        FLOAT
          , @xrot     FLOAT
          , @yrot     FLOAT
          , @zrot     FLOAT
          , @hx       FLOAT
          , @hy       FLOAT
          , @hz       FLOAT
          , @newLon   FLOAT
          , @newLat   FLOAT
          , @p        FLOAT
          , @errvalue FLOAT
          , @lat0     FLOAT

    SELECT  @a2 = 6378137             -- WGS84_AXIS
          , @e2 = 0.00669438037928458 -- WGS84_ECCENTRIC
          , @h  = @height             -- height above datum (from $GPGGA sentence)
          , @a  = 6377563.396         -- OSGB_AXIS
          , @e  = 0.0066705397616     -- OSGB_ECCENTRIC
          , @xp = 446.448
          , @yp = -125.157
          , @zp = 542.06
          , @xr = 0.1502
          , @yr = 0.247
          , @zr = 0.8421
          , @s  = -20.4894

    -- convert to cartesian; lat, lon are in radians
    SET @sf = @s * 0.000001
    SET @v = @a / (sqrt(1 - (@e * (SIN(@radOGlat) * SIN(@radOGlat)))))
    SET @x = (@v + @h) * COS(@radOGlat) * COS(@radOGlon)
    SET @y = (@v + @h) * COS(@radOGlat) * SIN(@radOGlon)
    SET @z = ((1 - @e) * @v + @h) * SIN(@radOGlat)

    -- transform cartesian
    SET @xrot = (@xr / 3600) * @deg2rad
    SET @yrot = (@yr / 3600) * @deg2rad
    SET @zrot = (@zr / 3600) * @deg2rad
    SET @hx = @x + (@x * @sf) - (@y * @zrot) + (@z * @yrot) + @xp
    SET @hy = (@x * @zrot) + @y + (@y * @sf) - (@z * @xrot) + @yp
    SET @hz = (-1 * @x * @yrot) + (@y * @xrot) + @z + (@z * @sf) + @zp

    -- Convert back to lat, lon
    SET @newLon = ATAN(@hy / @hx)
    SET @p = SQRT((@hx * @hx) + (@hy * @hy))
    SET @newLat = ATAN(@hz / (@p * (1 - @e2)))
    SET @v = @a2 / (SQRT(1 - @e2 * (SIN(@newLat) * SIN(@newLat))))
    SET @errvalue = 1.0;
    SET @lat0 = 0
    WHILE (@errvalue > 0.001)
    BEGIN
        SET @lat0 = ATAN((@hz + @e2 * @v * SIN(@newLat)) / @p)
        SET @errvalue = ABS(@lat0 - @newLat)
        SET @newLat = @lat0
    END

    --convert back to degrees
    SET @newLat = @newLat * @rad2deg
    SET @newLon = @newLon * @rad2deg

	RETURN geography::Point(@newLat, @newLon, 4326)

END
GO
/****** Object:  UserDefinedFunction [dbo].[NEtoLLGeometry]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[NEtoLLGeometry] (@East INT, @North INT) RETURNS GEOMETRY AS
BEGIN

-- Adapted from: https://stackoverflow.com/questions/4107806/sql-function-to-convert-uk-os-coordinates-from-easting-northing-to-longitude-and
-- Changed to return a geography type

--Author: Sandy Motteram
--Date:   06 September 2012

--UDF adapted from javascript at http://www.bdcc.co.uk/LatLngToOSGB.js
--found on page http://mapki.com/wiki/Tools:Snippets

--Instructions:
--Latitude and Longitude are calculated based on BOTH the easting and northing values from the OSGB36
--This UDF takes both easting and northing values in OSGB36 projection and you must specify if a latitude or longitude co-ordinate should be returned.
--IT first converts E/N values to lat and long in OSGB36 projection, then converts those values to lat/lng in WGS84 projection

--Sample values below
--DECLARE @East INT, @North INT, @LatOrLng VARCHAR(3)
--SELECT @East = 529000, @North = 183650 --that combo should be the corner of Camden High St and Delancey St


    DECLARE @Pi              FLOAT
          , @K0              FLOAT
          , @OriginLat       FLOAT
          , @OriginLong      FLOAT
          , @OriginX         FLOAT
          , @OriginY         FLOAT
          , @a               FLOAT
          , @b               FLOAT
          , @e2              FLOAT
          , @ex              FLOAT
          , @n1              FLOAT
          , @n2              FLOAT
          , @n3              FLOAT
          , @OriginNorthings FLOAT
          , @lat             FLOAT
          , @lon             FLOAT
          , @Northing        FLOAT
          , @Easting         FLOAT

    SELECT  @Pi = 3.14159265358979323846
          , @K0 = 0.9996012717 -- grid scale factor on central meridean
          , @OriginLat  = 49.0
          , @OriginLong = -2.0
          , @OriginX =  400000 -- 400 kM
          , @OriginY = -100000 -- 100 kM
          , @a = 6377563.396   -- Airy Spheroid
          , @b = 6356256.910
    /*    , @e2
          , @ex
          , @n1
          , @n2
          , @n3
          , @OriginNorthings*/

    -- compute interim values
    SELECT  @a = @a * @K0
          , @b = @b * @K0

    SET     @n1 = (@a - @b) / (@a + @b)
    SET     @n2 = @n1 * @n1
    SET     @n3 = @n2 * @n1

    SET     @lat = @OriginLat * @Pi / 180.0 -- to radians

    SELECT  @e2 = (@a * @a - @b * @b) / (@a * @a) -- first eccentricity
          , @ex = (@a * @a - @b * @b) / (@b * @b) -- second eccentricity

    SET     @OriginNorthings = @b * @lat + @b * (@n1 * (1.0 + 5.0 * @n1 * (1.0 + @n1) / 4.0) * @lat
          - 3.0 * @n1 * (1.0 + @n1 * (1.0 + 7.0 * @n1 / 8.0)) * SIN(@lat) * COS(@lat)
          + (15.0 * @n1 * (@n1 + @n2) / 8.0) * SIN(2.0 * @lat) * COS(2.0 * @lat)
          - (35.0 * @n3 / 24.0) * SIN(3.0 * @lat) * COS(3.0 * @lat))

    SELECT  @northing = @north - @OriginY
         ,  @easting  = @east  - @OriginX

    DECLARE @nu       FLOAT
          , @phid     FLOAT
          , @phid2    FLOAT
          , @t2       FLOAT
          , @t        FLOAT
          , @q2       FLOAT
          , @c        FLOAT
          , @s        FLOAT
          , @nphid    FLOAT
          , @dnphid   FLOAT
          , @nu2      FLOAT
          , @nudivrho FLOAT
          , @invnurho FLOAT
          , @rho      FLOAT
          , @eta2     FLOAT

    /* Evaluate M term: latitude of the northing on the centre meridian */

    SET     @northing = @northing + @OriginNorthings

    SET     @phid  = @northing / (@b*(1.0 + @n1 + 5.0 * (@n2 + @n3) / 4.0)) - 1.0
    SET     @phid2 = @phid + 1.0

    WHILE (ABS(@phid2 - @phid) > 0.000001)
    BEGIN
        SET @phid = @phid2;
        SET @nphid = @b * @phid + @b * (@n1 * (1.0 + 5.0 * @n1 * (1.0 + @n1) / 4.0) * @phid
                   - 3.0 * @n1 * (1.0 + @n1 * (1.0 + 7.0 * @n1 / 8.0)) * SIN(@phid) * COS(@phid)
                   + (15.0 * @n1 * (@n1 + @n2) / 8.0) * SIN(2.0 * @phid) * COS(2.0 * @phid)
                   - (35.0 * @n3 / 24.0) * SIN(3.0 * @phid) * COS(3.0 * @phid))

        SET @dnphid = @b * ((1.0 + @n1 + 5.0 * (@n2 + @n3) / 4.0) - 3.0 * (@n1 + @n2 + 7.0 * @n3 / 8.0) * COS(2.0 * @phid)
                    + (15.0 * (@n2 + @n3) / 4.0) * COS(4 * @phid) - (35.0 * @n3 / 8.0) * COS(6.0 * @phid))

        SET @phid2 = @phid - (@nphid - @northing) / @dnphid
    END

    SELECT @c = COS(@phid)
         , @s = SIN(@phid)
         , @t = TAN(@phid)
    SELECT @t2 = @t * @t
         , @q2 = @easting * @easting

    SET    @nu2 = (@a * @a) / (1.0 - @e2 * @s * @s)
    SET    @nu = SQRT(@nu2)

    SET    @nudivrho = @a * @a * @c * @c / (@b * @b) - @c * @c + 1.0
    SET    @eta2 = @nudivrho - 1
    SET    @rho = @nu / @nudivrho;

    SET    @invnurho = ((1.0 - @e2 * @s * @s) * (1.0 - @e2 * @s * @s)) / (@a * @a * (1.0 - @e2))

    SET    @lat = @phid - @t * @q2 * @invnurho / 2.0 + (@q2 * @q2 * (@t / (24 * @rho * @nu2 * @nu) * (5 + (3 * @t2) + @eta2 - (9 * @t2 * @eta2))))
    SET    @lon = (@easting / (@c * @nu))
                - (@easting * @q2 * ((@nudivrho + 2.0 * @t2) / (6.0 * @nu2)) / (@c * @nu))
                + (@q2 * @q2 * @easting * (5 + (28 * @t2) + (24 * @t2 * @t2)) / (120 * @nu2 * @nu2 * @nu * @c))


    SELECT @lat = @lat * 180.0 / @Pi
         , @lon = @lon * 180.0 / @Pi + @OriginLong


--Now convert the lat and long from OSGB36 to WGS84

    DECLARE @OGlat  FLOAT
          , @OGlon  FLOAT
          , @height FLOAT

    SELECT  @OGlat  = @lat
          , @OGlon  = @lon
          , @height = 24 --London's mean height above sea level is 24 metres. Adjust for other locations.

    DECLARE @deg2rad  FLOAT
          , @rad2deg  FLOAT
          , @radOGlat FLOAT
          , @radOGlon FLOAT

    SELECT  @deg2rad = @Pi / 180
          , @rad2deg = 180 / @Pi

    --first off convert to radians
    SELECT  @radOGlat = @OGlat * @deg2rad
          , @radOGlon = @OGlon * @deg2rad
    --these are the values for WGS84(GRS80) to OSGB36(Airy) 

    DECLARE @a2       FLOAT
          , @h        FLOAT
          , @xp       FLOAT
          , @yp       FLOAT
          , @zp       FLOAT
          , @xr       FLOAT
          , @yr       FLOAT
          , @zr       FLOAT
          , @sf       FLOAT
          , @e        FLOAT
          , @v        FLOAT
          , @x        FLOAT
          , @y        FLOAT
          , @z        FLOAT
          , @xrot     FLOAT
          , @yrot     FLOAT
          , @zrot     FLOAT
          , @hx       FLOAT
          , @hy       FLOAT
          , @hz       FLOAT
          , @newLon   FLOAT
          , @newLat   FLOAT
          , @p        FLOAT
          , @errvalue FLOAT
          , @lat0     FLOAT

    SELECT  @a2 = 6378137             -- WGS84_AXIS
          , @e2 = 0.00669438037928458 -- WGS84_ECCENTRIC
          , @h  = @height             -- height above datum (from $GPGGA sentence)
          , @a  = 6377563.396         -- OSGB_AXIS
          , @e  = 0.0066705397616     -- OSGB_ECCENTRIC
          , @xp = 446.448
          , @yp = -125.157
          , @zp = 542.06
          , @xr = 0.1502
          , @yr = 0.247
          , @zr = 0.8421
          , @s  = -20.4894

    -- convert to cartesian; lat, lon are in radians
    SET @sf = @s * 0.000001
    SET @v = @a / (sqrt(1 - (@e * (SIN(@radOGlat) * SIN(@radOGlat)))))
    SET @x = (@v + @h) * COS(@radOGlat) * COS(@radOGlon)
    SET @y = (@v + @h) * COS(@radOGlat) * SIN(@radOGlon)
    SET @z = ((1 - @e) * @v + @h) * SIN(@radOGlat)

    -- transform cartesian
    SET @xrot = (@xr / 3600) * @deg2rad
    SET @yrot = (@yr / 3600) * @deg2rad
    SET @zrot = (@zr / 3600) * @deg2rad
    SET @hx = @x + (@x * @sf) - (@y * @zrot) + (@z * @yrot) + @xp
    SET @hy = (@x * @zrot) + @y + (@y * @sf) - (@z * @xrot) + @yp
    SET @hz = (-1 * @x * @yrot) + (@y * @xrot) + @z + (@z * @sf) + @zp

    -- Convert back to lat, lon
    SET @newLon = ATAN(@hy / @hx)
    SET @p = SQRT((@hx * @hx) + (@hy * @hy))
    SET @newLat = ATAN(@hz / (@p * (1 - @e2)))
    SET @v = @a2 / (SQRT(1 - @e2 * (SIN(@newLat) * SIN(@newLat))))
    SET @errvalue = 1.0;
    SET @lat0 = 0
    WHILE (@errvalue > 0.001)
    BEGIN
        SET @lat0 = ATAN((@hz + @e2 * @v * SIN(@newLat)) / @p)
        SET @errvalue = ABS(@lat0 - @newLat)
        SET @newLat = @lat0
    END

    --convert back to degrees
    SET @newLat = @newLat * @rad2deg
    SET @newLon = @newLon * @rad2deg

	RETURN GEOMETRY::Point(@newLat, @newLon, 4326)

END

GO
/****** Object:  UserDefinedFunction [dbo].[SplitMultiPolygons]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitMultiPolygons] (@MultiPolygon geography)
RETURNS @Results TABLE (result geography) AS
BEGIN
  DECLARE @i int = 1;
  WHILE @i <= @MultiPolygon.STNumGeometries()  
  BEGIN 
    INSERT INTO @Results VALUES (@MultiPolygon.STGeometryN(@i))  
    SET @i = @i + 1  
  END
  RETURN
END;

GO
/****** Object:  UserDefinedFunction [dbo].[ufnGetRatVal]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufnGetRatVal](@TitleNo VARCHAR(50))  
RETURNS float   
AS   

BEGIN  
    DECLARE @ret float;  
	SELECT @ret = SUM(VL.RateableValue)
	FROM VOA.dbo.VOADataLIST VL
	INNER JOIN AddressBase.dbo.AddressBaseCrossReference CR ON CR.SOURCE IN ('7666VN','7666VC') AND CR.CROSS_REFERENCE = VL.AddressKey
	INNER JOIN NationalPolygon.dbo.TitleNumberUPRN TU ON TU.UPRN = CR.UPRN AND TU.TitleNumber = @TitleNo
    RETURN @ret;  
END; 
GO
/****** Object:  Table [dbo].[TitleNumberUPRN]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleNumberUPRN](
	[pkid] [int] IDENTITY(1,1) NOT NULL,
	[TitleNumber] [nvarchar](24) NULL,
	[UPRN] [bigint] NULL,
	[Source] [smallint] NULL,
	[Source_Str] [nvarchar](10) NULL,
	[ID] [uniqueidentifier] NOT NULL,
	[DeliveryPoint] [bit] NULL,
	[Tenure] [char](1) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumberUPRN]    Script Date: 25/06/2021 16:43:26 ******/
CREATE CLUSTERED INDEX [IX_TitleNumberUPRN] ON [dbo].[TitleNumberUPRN]
(
	[TitleNumber] ASC,
	[UPRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetUPRNTitleNumber]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[GetUPRNTitleNumber]
	(
		@titleNumber AS VARCHAR(9) ,
		@polygon AS GEOGRAPHY
	)
	RETURNS TABLE
AS

RETURN (
		SELECT UPRN, CAST(0 AS BIT) AS [Spatial] FROM TitleNumberUPRN WHERE TitleNumber = @titleNumber
		);
GO
/****** Object:  Table [dbo].[TitleNumberPolygon]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleNumberPolygon](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TitleNumber] [varchar](50) NOT NULL,
	[PolygonID] [bigint] NOT NULL,
	[Reference] [uniqueidentifier] NULL,
 CONSTRAINT [PK_TitleNumberPolygon] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TitleNumberTenure]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleNumberTenure](
	[TitleNumber] [varchar](9) NOT NULL,
	[Tenure] [char](1) NOT NULL,
	[TenureFull]  AS (case [Tenure] when 'F' then 'Freehold' else 'Leasehold' end) PERSISTED NOT NULL,
 CONSTRAINT [PK_TitleNumberTenure] PRIMARY KEY CLUSTERED 
(
	[TitleNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[PolygonTitleNumber]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[PolygonTitleNumber]
WITH SCHEMABINDING
AS

--SELECT NULL AS PolygonID;
    SELECT p.PolygonID ,
           p.TitleNumber ,
           t.TenureFull AS Tenure,
           p.Reference
    FROM   dbo.TitleNumberPolygon p
    INNER JOIN dbo.TitleNumberTenure t ON t.TitleNumber = p.TitleNumber;


GO
/****** Object:  Table [dbo].[Polygon]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Polygon](
	[InspireID] [bigint] NOT NULL,
	[InspireIDPart] [int] NOT NULL,
	[TitleNumber] [varchar](20) NULL,
	[INSERT] [datetime2](7) NULL,
	[UPDATE] [datetime2](7) NULL,
	[REC_STATUS] [char](1) NULL,
	[Freehold] [bit] NULL,
	[Estate] [varchar](2) NULL,
	[Class] [varchar](2) NULL,
	[Status] [varchar](10) NULL,
	[Change] [varchar](1) NULL,
	[Tenure] [char](1) NULL,
	[SingleEasting] [float] NULL,
	[SingleNorthing] [float] NULL,
	[Area] [float] NULL,
	[Long] [float] NULL,
	[Lat] [float] NULL,
	[WKT4326] [nvarchar](max) NULL,
	[PolygonGeography] [geography] NULL,
	[LatLng] [geography] NULL,
	[WKT27700] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[InspireID] ASC,
	[InspireIDPart] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[PolygonByTitleNumber]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[PolygonByTitleNumber]
AS
SELECT        TitleNumber, SingleEasting, SingleNorthing, WKT27700 AS Polygon, Area
FROM            dbo.Polygon
GO
/****** Object:  Table [dbo].[FreeholdToLeasehold]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FreeholdToLeasehold](
	[Freehold] [nvarchar](100) NULL,
	[Leasehold] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FreeholdToLeasehold_orig]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FreeholdToLeasehold_orig](
	[Freehold] [nvarchar](100) NULL,
	[Leasehold] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MultiplePolygonCount]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MultiplePolygonCount](
	[UPRN] [bigint] NOT NULL,
	[PolygonCount] [int] NOT NULL,
 CONSTRAINT [PK_MultiplePolygonCount] PRIMARY KEY CLUSTERED 
(
	[UPRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TitleInfo]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleInfo](
	[TitleNumber] [nvarchar](36) NOT NULL,
	[Estate] [nvarchar](8) NULL,
	[Class] [nvarchar](8) NULL,
	[Status] [nvarchar](200) NULL,
	[RecStatus] [char](1) NULL,
	[Tenure] [char](1) NULL,
	[HasIndustrial] [bit] NULL,
	[TotalArea] [float] NULL,
	[TotalBuildingFootprint] [float] NULL,
	[UPRNCount] [int] NULL,
	[HasLL] [bit] NULL,
	[HasOffices] [bit] NULL,
	[HasRetail] [bit] NULL,
	[HasResi] [bit] NULL,
	[EPC] [char](1) NULL,
	[FirstSaleDate] [datetime2](7) NULL,
	[FirstSalePrice] [decimal](19, 2) NULL,
	[LastSaleDate] [datetime] NULL,
	[LastSalePrice] [float] NULL,
	[Acreage] [float] NULL,
	[BuildingType] [char](1) NULL,
	[Postcode] [nvarchar](48) NULL,
	[DevPerc] [float] NULL,
	[GreenBeltOverlap] [float] NULL,
	[AONBOverlap] [float] NULL,
	[HasListed] [bit] NULL,
	[HasVOARates] [bit] NULL,
	[HasCouncilTax] [bit] NULL,
	[CommercialFootprint] [float] NULL,
	[PricePaidStreet] [nvarchar](800) NULL,
	[Top1Owner] [nvarchar](2000) NULL,
	[OwnershipStreet] [nvarchar](800) NULL,
	[FZ2Overlap] [float] NULL,
	[FZ3Overlap] [float] NULL,
	[VOAPropDescription] [nvarchar](2000) NULL,
	[VOATotalValue] [float] NULL,
	[FreeholdFlag] [int] NOT NULL,
	[WoodlandOverlap] [float] NULL,
	[NumberOfPolys] [int] NULL,
	[CommercialCount] [int] NULL,
	[ResiCount] [int] NULL,
	[Owner1] [nvarchar](100) NULL,
	[Owner2] [nvarchar](100) NULL,
	[Owner3] [nvarchar](100) NULL,
	[Owner4] [nvarchar](100) NULL,
	[Owner1Type] [int] NULL,
	[Owner2Type] [int] NULL,
	[Owner3Type] [int] NULL,
	[Owner4Type] [int] NULL,
	[LastUpdated] [datetime2](7) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TitleInfo_sample]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleInfo_sample](
	[TitleNumber] [nvarchar](36) NOT NULL,
	[Estate] [nvarchar](8) NULL,
	[Class] [nvarchar](8) NULL,
	[Status] [nvarchar](200) NULL,
	[RecStatus] [char](1) NULL,
	[Tenure] [char](1) NULL,
	[HasIndustrial] [bit] NULL,
	[TotalArea] [float] NULL,
	[TotalBuildingFootprint] [float] NULL,
	[UPRNCount] [int] NULL,
	[HasLL] [bit] NULL,
	[HasOffices] [bit] NULL,
	[HasRetail] [bit] NULL,
	[HasResi] [bit] NULL,
	[EPC] [char](1) NULL,
	[LastSaleDate] [datetime] NULL,
	[LastSalePrice] [float] NULL,
	[Acreage] [float] NULL,
	[BuildingType] [char](1) NULL,
	[Postcode] [nvarchar](48) NULL,
	[DevPerc] [float] NULL,
	[GreenBeltOverlap] [float] NULL,
	[AONBOverlap] [float] NULL,
	[HasListed] [bit] NULL,
	[HasVOARates] [bit] NULL,
	[HasCouncilTax] [bit] NULL,
	[CommercialFootprint] [float] NULL,
	[PricePaidStreet] [nvarchar](800) NULL,
	[Top1Owner] [nvarchar](2000) NULL,
	[OwnershipStreet] [nvarchar](800) NULL,
	[FZ2Overlap] [float] NULL,
	[FZ3Overlap] [float] NULL,
	[VOAPropDescription] [nvarchar](2000) NULL,
	[VOATotalValue] [float] NULL,
	[FreeholdFlag] [int] NOT NULL,
	[WoodlandOverlap] [float] NULL,
	[NumberOfPolys] [int] NULL,
	[CommercialCount] [int] NULL,
	[ResiCount] [int] NULL,
	[Owner1] [nvarchar](100) NULL,
	[Owner2] [nvarchar](100) NULL,
	[Owner3] [nvarchar](100) NULL,
	[Owner4] [nvarchar](100) NULL,
	[Owner1Type] [int] NULL,
	[Owner2Type] [int] NULL,
	[Owner3Type] [int] NULL,
	[Owner4Type] [int] NULL,
	[LastUpdated] [datetime2](7) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnmatchedUPRN]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnmatchedUPRN](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UPRN] [bigint] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_UnmatchedUPRN] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnregisteredLand]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnregisteredLand](
	[LOCATION] [geography] NULL,
	[UPRN] [bigint] NOT NULL,
	[SingleEasting] [numeric](8, 2) NULL,
	[SingleNorthing] [numeric](9, 2) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [pk_UnregisteredLandID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UprnPolygonId]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UprnPolygonId](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UPRN] [bigint] NOT NULL,
	[InspireId] [bigint] NOT NULL,
	[Source] [tinyint] NOT NULL,
 CONSTRAINT [PK_UprnPolygonId] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Freehold]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_Freehold] ON [dbo].[FreeholdToLeasehold]
(
	[Freehold] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Leasehold]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_Leasehold] ON [dbo].[FreeholdToLeasehold]
(
	[Leasehold] ASC
)
INCLUDE([Freehold]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Freehold]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_Freehold] ON [dbo].[FreeholdToLeasehold_orig]
(
	[Freehold] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Leasehold]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_Leasehold] ON [dbo].[FreeholdToLeasehold_orig]
(
	[Leasehold] ASC
)
INCLUDE([Freehold]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Area_LatLng]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_Area_LatLng] ON [dbo].[Polygon]
(
	[Area] ASC
)
INCLUDE([LatLng]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_InspireID_TitleNumber_Freehold]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_InspireID_TitleNumber_Freehold] ON [dbo].[Polygon]
(
	[InspireID] ASC,
	[TitleNumber] ASC,
	[Freehold] ASC
)
INCLUDE([SingleEasting],[SingleNorthing],[Area],[WKT27700]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SingleEasting_SingleNorthing]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_SingleEasting_SingleNorthing] ON [dbo].[Polygon]
(
	[SingleEasting] ASC,
	[SingleNorthing] ASC
)
INCLUDE([InspireID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SingleEastSingleNorth]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_SingleEastSingleNorth] ON [dbo].[Polygon]
(
	[SingleEasting] ASC,
	[SingleNorthing] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Tenure]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_Tenure] ON [dbo].[Polygon]
(
	[Tenure] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumber]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumber] ON [dbo].[Polygon]
(
	[TitleNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumber_IncArea]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumber_IncArea] ON [dbo].[Polygon]
(
	[TitleNumber] ASC
)
INCLUDE([Area]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumber_Lat_Lng]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumber_Lat_Lng] ON [dbo].[Polygon]
(
	[TitleNumber] ASC
)
INCLUDE([Lat],[Long]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [SingleEasting_SingleNorthing_TitleNumber]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [SingleEasting_SingleNorthing_TitleNumber] ON [dbo].[Polygon]
(
	[SingleEasting] ASC,
	[SingleNorthing] ASC
)
INCLUDE([TitleNumber]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IDX_TitleNumber_Acreage_VOATotalValue]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IDX_TitleNumber_Acreage_VOATotalValue] ON [dbo].[TitleInfo]
(
	[TitleNumber] ASC
)
INCLUDE([Acreage],[VOATotalValue]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumber_HasIndustrial_HasLL_HasOffices_HasRetail]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumber_HasIndustrial_HasLL_HasOffices_HasRetail] ON [dbo].[TitleInfo]
(
	[TitleNumber] ASC
)
INCLUDE([HasIndustrial],[HasLL],[HasOffices],[HasRetail]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumberPolygon]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumberPolygon] ON [dbo].[TitleNumberPolygon]
(
	[TitleNumber] ASC,
	[PolygonID] ASC
)
INCLUDE([ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TitleNumberPolygon_PolygonID]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumberPolygon_PolygonID] ON [dbo].[TitleNumberPolygon]
(
	[PolygonID] ASC
)
INCLUDE([TitleNumber],[ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumberTenure]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumberTenure] ON [dbo].[TitleNumberTenure]
(
	[Tenure] ASC,
	[TitleNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumberTenure_TitleNumber]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumberTenure_TitleNumber] ON [dbo].[TitleNumberTenure]
(
	[TitleNumber] ASC
)
INCLUDE([TenureFull],[Tenure]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TitleNumberUPRN_UPRN]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumberUPRN_UPRN] ON [dbo].[TitleNumberUPRN]
(
	[UPRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [ix_TN_UPRN_Tenure]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [ix_TN_UPRN_Tenure] ON [dbo].[TitleNumberUPRN]
(
	[TitleNumber] ASC,
	[UPRN] ASC,
	[Tenure] ASC,
	[Source] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UnmatchedUPRN_UPRN]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_UnmatchedUPRN_UPRN] ON [dbo].[UnmatchedUPRN]
(
	[UPRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20200904-165519]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200904-165519] ON [dbo].[UnregisteredLand]
(
	[UPRN] ASC
)
INCLUDE([LOCATION]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_InspireID_UPRN]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_InspireID_UPRN] ON [dbo].[UprnPolygonId]
(
	[UPRN] ASC,
	[InspireId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UprnPolygonID]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_UprnPolygonID] ON [dbo].[UprnPolygonId]
(
	[UPRN] ASC,
	[InspireId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UprnPolygonId_InspireId]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED INDEX [IX_UprnPolygonId_InspireId] ON [dbo].[UprnPolygonId]
(
	[InspireId] ASC
)
INCLUDE([UPRN]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TitleNumberPolygon_CS]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED COLUMNSTORE INDEX [IX_TitleNumberPolygon_CS] ON [dbo].[TitleNumberPolygon]
(
	[ID],
	[TitleNumber],
	[PolygonID]
)WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0) ON [PRIMARY]
GO
/****** Object:  Index [IX_TitleNumberTenure_CS]    Script Date: 25/06/2021 16:43:26 ******/
CREATE NONCLUSTERED COLUMNSTORE INDEX [IX_TitleNumberTenure_CS] ON [dbo].[TitleNumberTenure]
(
	[Tenure],
	[TitleNumber]
)WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TitleNumberPolygon] ADD  CONSTRAINT [DF_TitleNumberPolygon_Reference]  DEFAULT (newid()) FOR [Reference]
GO
ALTER TABLE [dbo].[UnmatchedUPRN] ADD  CONSTRAINT [DF_UnmatchedUPRN_Date]  DEFAULT (getdate()) FOR [Date]
GO
/****** Object:  StoredProcedure [dbo].[CreateUnregisteredLand]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CreateUnregisteredLand] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	TRUNCATE TABLE NationalPolygon.dbo.UnregisteredLand
	INSERT INTO NationalPolygon.dbo.UnregisteredLand([LOCATION],[UPRN],[SingleEasting],[SingleNorthing])
	  SELECT abb.LOCATION,
               abb.UPRN,
               X_COORDINATE AS SingleEasting,
               Y_COORDINATE AS SingleNorthing
        FROM AddressBase.dbo.AddressBaseBLPU abb    
		LEFT JOIN AddressBase.dbo.AddressBaseDeliveryPoint abdp ON abb.UPRN = abdp.UPRN  
        AND abdp.UPRN IS NOT NULL AND abb.END_DATE IS NULL
		LEFT JOIN NationalPolygon.dbo.UprnPolygonId upid ON upid.UPRN = abb.UPRN
		LEFT JOIN NationalPolygon.dbo.TitleNumberUPRN TNU ON TNU.UPRN = abb.UPRN
		WHERE upid.InspireId IS NULL AND TNU.UPRN IS NULL
END


GO
/****** Object:  StoredProcedure [dbo].[DeleteDuplicatePolys]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteDuplicatePolys] 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	WITH CTE AS(
		SELECT [InspireID]
			  ,[TitleNumber]
			  ,[INSERT]
			  ,[UPDATE]
			  ,[REC_STATUS]
			  ,[Freehold]
			  ,[Estate]
			  ,[Class]
			  ,[Status]
			  ,[Change]
			  ,[Tenure]
			  ,[Area]
			  ,ROW_NUMBER() OVER(PARTITION BY [TitleNumber]
			   ORDER BY [UPDATE] DESC,[Area] DESC) AS Rnum

		  FROM [NationalPolygon].[dbo].[Polygon]
		  WHERE Freehold = 1)
  DELETE FROM CTE WHERE Rnum <> 1


END
GO
/****** Object:  StoredProcedure [dbo].[GetPolygonIDsForUPRN]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPolygonIDsForUPRN]
    @uprn AS BIGINT
AS
    DECLARE @g GEOGRAPHY = ( SELECT [LOCATION] FROM AddressBase.dbo.AddressBaseBLPU WHERE UPRN = @uprn );

    SELECT p.InspireID
    FROM   NationalPolygon.dbo.Polygon p
    WHERE  p.PolygonGeography.STContains(@g) = 1
           AND p.PolygonIsValid = 1;

GO
/****** Object:  StoredProcedure [dbo].[GetTitleNumberForCoordinates]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTitleNumberForCoordinates]
    @latitude FLOAT ,
    @longitude FLOAT
AS
    DECLARE @g GEOGRAPHY = ( SELECT geography::Point(@latitude, @longitude, 4326));

    SELECT     TOP 1 p.TitleNumber
    FROM       NationalPolygon.dbo.Polygon p WITH(INDEX(SPX_POLYGONGEOGRAPHY))
    WHERE      p.PolygonGeography.STIntersects(@g) = 1 
               AND p.Tenure = 'F';

-- EXEC [dbo].[GetTitleNumberForCoordinates] 51.506988, -0.197633
GO
/****** Object:  StoredProcedure [dbo].[GetUPRNForCoordinates]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUPRNForCoordinates]
    @latitude FLOAT ,
    @longitude FLOAT
AS
    DECLARE @g GEOGRAPHY = ( SELECT geography::Point(@latitude, @longitude, 4326));

    SELECT TOP 1	tnu.UPRN
    FROM			NationalPolygon.dbo.Polygon p WITH(INDEX(SPX_POLYGONGEOGRAPHY))
	INNER JOIN		NationalPolygon.dbo.TitleNumberUPRN tnu 
	ON				tnu.TitleNumber = p.TitleNumber
    WHERE			p.PolygonGeography.STContains(@g) = 1 
					AND p.Tenure = 'F';

/*
	EXEC [dbo].[GetUPRNForCoordinates] 49.8728846619616, -6.4453420792657

	test query:
	SELECT TOP 1	p.TitleNumber, p.Long ,tnu.UPRN, p.Lat, p.LatLng, PolygonGeography
    FROM			NationalPolygon.dbo.Polygon p 
	INNER JOIN		NationalPolygon.dbo.TitleNumberUPRN tnu 
	ON				tnu.TitleNumber = p.TitleNumber
    WHERE			p.Tenure = 'F';

	test result:
	Geography: 0xE610000001044300000075326D14BDEF4840D79C00B9B0C719C0D06A22F0BDEF4840F73309B5CBC719C0092CD2C4BDEF48408ED525C4D5C719C0FB2224F1BDEF48407116309CE1C719C060E9FB7DBEEF4840693A169FE8C719C09BC33D38BEEF484080D7C7ABFAC719C08B1049F0BEEF4840799C1CFF03C819C0EAA84110BFEF48404EEA159F05C819C02400AA47BFEF48403025F87108C819C0C82BF16DBFEF48403CA9B8B10BC819C01EBE0E73BFEF484010E7360F0DC819C0B5D6AB52BFEF4840657E5FC113C819C05EB089FFBEEF4840450E18FA19C819C0B916B1BDBEEF4840E5367CCD1DC819C0B51B2C49BEEF4840D264805222C819C00E5416C2BDEF4840DC73705D25C819C0A18D34A0BBEF48400704055C32C819C0728728D1BBEF484027DDC51946C819C06CF376F3BBEF4840A2DD5EF74BC819C03BBC87FCBBEF484064E82F5453C819C054A032E2BBEF4840D2B14CA05AC819C02917C2C6BBEF484094BD9A865EC819C0294D0195BBEF4840211A1F6263C819C07C27EA51BBEF4840EF44609B67C819C0818799F9BAEF4840EC09B1AC6AC819C04563D0E0BAEF4840D2D4F42F6BC819C02E6D2867BAEF4840EDA941846CC819C093D9ACE9B9EF4840B27CE20B6CC819C08280B271B9EF4840B0EC252B6AC819C089D91310B9EF4840FE90E34B67C819C0F91D04B7B8EF484071172BFD62C819C007816E10B8EF4840BA96BA4D4DC819C03FA8E68FB7EF48405E31D72B26C819C00DA3F7C9B7EF48403D11343C1EC819C0F9919A46B6EF48401CF05BC10DC819C0A5CE963BB6EF4840B3C630810DC819C0E2ADCA04B6EF4840DBAFA3690BC819C04428FADDB5EF4840D117A09908C819C0DDEE97CAB5EF48403DDF175505C819C0827C1ED9B5EF4840870E79E200C819C020E31913B6EF4840E62A5B16FCC719C026B68222B6EF4840405A1838FBC719C01E761E58B6EF484040289F5DF9C719C0AEABC28FB6EF4840212C5DB3F7C719C0BD9730B3B6EF4840D8D3B248F5C719C08313F2ACB6EF4840943E30B2EEC719C0B2A3E8ACB6EF48407360A81CEEC719C0567AAFA2B6EF4840C36FD5C2EAC719C0323C229CB6EF48409C34667EE7C719C069A3ECABB6EF484059B61F79E4C719C0CDF65BC1B6EF484077C97200E3C719C0AA9311F5B6EF4840082ED3D9E0C719C0D464D433B7EF48402A82E688DFC719C0A83E5B77B7EF484093241D41DFC719C0F49D15C7B9EF4840C2D8BDCED1C719C02DE81556BAEF4840CD6DDD83B6C719C045935C56BAEF484054F827ADB5C719C00ABBF067BAEF4840D8276F33B1C719C02FE12F91BAEF4840A5D2381FADC719C07B75F2D0BAEF4840EB0BC4C3A9C719C0AB7739EABAEF4840E16113DAA8C719C0775B054BBBEF48404840F7DDA6C719C0B6E71BB6BBEF4840CD0ED855A6C719C0271E4020BCEF4840340E1930A7C719C0B9251A82BCEF48408BF1F241A9C719C0CA7DA5D5BCEF48402656F886ACC719C075326D14BDEF4840D79C00B9B0C719C001000000020000000001000000FFFFFFFF0000000003
	LatLng: 0xE6100000010C0D2742AFBAEF484050EA07C107C819C0
	Lat: 49.8728846619616
	Lng: -6.4453420792657
	title: CL222547
	UPRN: 192001838
*/


GO
/****** Object:  StoredProcedure [dbo].[ImportAncientWoodlandPolygon]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ImportAncientWoodlandPolygon]
    @polygons AS AncientWoodlandPolygonImport READONLY
AS
    BEGIN
        INSERT AncientWoodlandPolygon (   ThemeId ,
                                          [Name] ,
                                          ThemeName ,
                                          [Status] ,
                                          UpdateStatus ,
                                          GridRef ,
                                          Polygon
                                      )
               SELECT ThemeId ,
                      [Name] ,
                      ThemeName ,
                      [Status] ,
                      UpdateStatus ,
                      GridRef ,
                      CASE WHEN PolygonGeography LIKE '%MULTI%' THEN geography::STMPolyFromText(PolygonGeography, 4326)
                           WHEN PolygonGeography LIKE '%GEOMETRYCOLL%' THEN
                               geography::STGeomCollFromText(PolygonGeography, 4326)
                           ELSE geography::STPolyFromText(PolygonGeography, 4326)
                      END
               FROM   @polygons;
    END;
GO
/****** Object:  StoredProcedure [dbo].[ImportAONBPolygon]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[ImportAONBPolygon]
    @polygons AS AONBPolygonImport READONLY
AS
    BEGIN
        MERGE AONBPolygon AS t
        USING ( SELECT [Name], DateDesignated, Link, PolygonGeography FROM @polygons ) AS s
        ON s.[Name] = t.[Name]
        WHEN NOT MATCHED THEN
            INSERT (   [Name] ,
                       DateDesignated ,
                       Link ,
                       Polygon
                   )
            VALUES (   s.[Name] ,
                       s.DateDesignated,
                       s.Link,
                       CASE WHEN s.PolygonGeography LIKE '%MULTI%' THEN
                                geography::STMPolyFromText(s.PolygonGeography, 4326)
                            WHEN s.PolygonGeography LIKE '%GEOMETRYCOLL%' THEN
                                geography::STGeomCollFromText(s.PolygonGeography, 4326)
                            ELSE geography::STPolyFromText(s.PolygonGeography, 4326)
                       END
                   );
    END;
GO
/****** Object:  StoredProcedure [dbo].[ImportFloodZonePolygon]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ImportFloodZonePolygon]
    @polygons AS FloodZonePolygonImport READONLY
AS
    BEGIN
        INSERT FloodZone (   Layer ,
                             FloodType ,
                             Polygon
                         )
               SELECT Layer ,
                      FloodType ,
                      CASE WHEN PolygonGeography LIKE '%MULTI%' THEN geography::STMPolyFromText(PolygonGeography, 4326)
                           WHEN PolygonGeography LIKE '%GEOMETRYCOLL%' THEN
                               geography::STGeomCollFromText(PolygonGeography, 4326)
                           ELSE geography::STPolyFromText(PolygonGeography, 4326)
                      END AS Polygon
               FROM   @polygons;
    END;
GO
/****** Object:  StoredProcedure [dbo].[ImportGreenbeltPolygon]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ImportGreenbeltPolygon]
    @polygons AS GreenbeltPolygonImport READONLY
AS
    BEGIN
        MERGE GreenbeltPolygon AS t
        USING (   SELECT PolygonId ,
                         GreenbeltName ,
                         LocalAuthorityName ,
                         ONSCode ,
                         PolygonGeography
                  FROM   @polygons
              ) AS s
        ON s.PolygonId = t.PolygonId
        WHEN NOT MATCHED THEN
            INSERT (   PolygonId ,
                       GreenbeltName ,
                       LocalAuthorityName ,
                       ONSCode ,
                       Polygon
                   )
            VALUES ( s .PolygonId ,
                     s.GreenbeltName,
                     s.LocalAuthorityName,
                     s.ONSCode,
					 CASE 
						WHEN s.PolygonGeography LIKE '%MULTI%' THEN geography::STMPolyFromText(s.PolygonGeography, 4326)
						WHEN s.PolygonGeography LIKE '%GEOMETRYCOLL%' THEN geography::STGeomCollFromText(s.PolygonGeography, 4326)
						ELSE geography::STPolyFromText(s.PolygonGeography, 4326)
					 END
                   );
    END;
GO
/****** Object:  StoredProcedure [dbo].[ImportMatchedUprnPolygonId]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ImportMatchedUprnPolygonId]
    (
        @ImportTable dbo.MatchedUprnPolygonIdImport READONLY
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SET ANSI_NULLS OFF;

        DECLARE @Output TABLE
            (
                mergeAction NVARCHAR(10)
            );

        MERGE UprnPolygonId AS t
        USING ( SELECT * FROM @ImportTable ) AS s
        ON s.uprn = t.UPRN
           AND s.polygonId = t.InspireId
        WHEN NOT MATCHED THEN INSERT (   UPRN ,
                                         InspireId ,
                                         [Source]
                                     )
                              VALUES ( s.uprn, s.polygonId, 3 )
        OUTPUT $action
        INTO @Output;

        SELECT mergeAction, COUNT(*) AS itemCount FROM @Output GROUP BY mergeAction;

        SET ANSI_NULLS ON;

        SET NOCOUNT OFF;
    END;
GO
/****** Object:  StoredProcedure [dbo].[ImportPolygon_deadcode?]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ImportPolygon_deadcode?]
    @polygons AS PolygonImport READONLY
AS
    BEGIN
        SET NOCOUNT ON;

        SET ANSI_NULLS OFF;

        DECLARE @Output TABLE
            (
                mergeAction NVARCHAR(10) ,
                itemID BIGINT
            );

        MERGE Polygon AS t
        USING (   SELECT PolygonID ,
                         Polygon ,
                         SingleEasting ,
                         SingleNorthing ,
                         Area ,
                         PolygonGeography ,
                         LatLng
                  FROM   @polygons ) AS s
        ON s.PolygonID = t.InspireID
        WHEN MATCHED AND t.Polygon <> s.Polygon
                         OR t.SingleEasting <> s.SingleEasting
                         OR t.SingleNorthing <> s.SingleNorthing
                         OR t.Area <> s.Area THEN
            UPDATE SET t.Polygon = s.Polygon ,
                       t.SingleEasting = s.SingleEasting ,
                       t.SingleNorthing = s.SingleNorthing ,
                       t.Area = s.Area ,
                       t.PolygonGeography = CASE WHEN s.PolygonGeography LIKE '%MULTI%' THEN
                                                     geography::STMPolyFromText(s.PolygonGeography, 4326)
                                                 WHEN s.PolygonGeography LIKE '%GEOMETRYCOLL%' THEN
                                                     geography::STGeomCollFromText(s.PolygonGeography, 4326)
                                                 ELSE geography::STPolyFromText(s.PolygonGeography, 4326)
                                            END ,
                       t.LatLng = geography::STPointFromText(s.LatLng, 4326)
        WHEN NOT MATCHED THEN
            INSERT ( InspireID ,
                     Polygon ,
                     SingleEasting ,
                     SingleNorthing ,
                     Area ,
                     PolygonGeography ,
                     LatLng )
            VALUES ( s.PolygonID, s.Polygon, s.SingleEasting, s.SingleNorthing, s.Area ,
                     CASE WHEN s.PolygonGeography LIKE '%MULTI%' THEN
                              geography::STMPolyFromText(s.PolygonGeography, 4326)
                          WHEN s.PolygonGeography LIKE '%GEOMETRYCOLL%' THEN
                              geography::STGeomCollFromText(s.PolygonGeography, 4326)
                          ELSE geography::STPolyFromText(s.PolygonGeography, 4326)
                     END, geography::STPointFromText(s.LatLng, 4326))
        OUTPUT $action ,
               COALESCE(Inserted.InspireID, Deleted.InspireID) AS itemID
        INTO @Output;

        MERGE TitleNumberPolygon AS t
        USING ( SELECT DISTINCT TitleNumber, PolygonID FROM @polygons ) AS s
        ON s.PolygonID = t.PolygonID
        WHEN NOT MATCHED THEN INSERT ( TitleNumber ,
                                       PolygonID ,
                                       Reference )
                              VALUES ( s.TitleNumber, s.PolygonID, NEWID());

        SELECT mergeAction, COUNT(*) AS itemCount FROM @Output GROUP BY mergeAction;

        SET ANSI_NULLS ON;

        SET NOCOUNT OFF;
    END;
GO
/****** Object:  StoredProcedure [dbo].[ImportUnmatchedUprn]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ImportUnmatchedUprn]
    (
        @ImportTable dbo.UnmatchedUprnImport READONLY
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SET ANSI_NULLS OFF;

        DECLARE @Output TABLE
            (
                mergeAction NVARCHAR(10)
            );

        MERGE UnmatchedUPRN AS t
        USING ( SELECT * FROM @ImportTable ) AS s
        ON s.uprn = t.UPRN
        WHEN NOT MATCHED THEN INSERT ( UPRN )
                              VALUES ( s.uprn )
        OUTPUT $action
        INTO @Output;

        SELECT mergeAction, COUNT(*) AS itemCount FROM @Output GROUP BY mergeAction;

        SET ANSI_NULLS ON;

        SET NOCOUNT OFF;
    END;

GO
/****** Object:  StoredProcedure [dbo].[PopulateTitleNumberUPRN]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PopulateTitleNumberUPRN]
AS
    SET NOCOUNT ON;

	DECLARE @uprn BIGINT = 1;
	DECLARE @blpuLocation GEOGRAPHY;
	DECLARE @polygonCount INT;
	DECLARE @recordCount BIGINT = 0;

	DECLARE blpu_cursor CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT blpu.UPRN ,
				blpu.[LOCATION]
		FROM   AddressBase.dbo.AddressBaseBLPU blpu
		LEFT OUTER JOIN TitleNumberUPRN tnu ON tnu.UPRN = blpu.UPRN
		LEFT OUTER JOIN MultiplePolygonCount mpc ON mpc.UPRN = blpu.UPRN
		WHERE  tnu.UPRN IS NULL
				AND mpc.UPRN IS NULL;

	OPEN blpu_cursor;

	FETCH NEXT FROM blpu_cursor
	INTO @uprn ,
			@blpuLocation;

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @polygonCount = COUNT(*)
			FROM   NationalPolygon.dbo.Polygon p
			WHERE  p.PolygonGeography.STContains(@blpuLocation) = 1;

			IF @polygonCount = 1
				BEGIN
					INSERT TitleNumberUPRN (   TitleNumber ,
												UPRN,
												[Source]
											)
							SELECT p.TitleNumber ,
									@uprn AS uprn, 2 as [Source]
							FROM   NationalPolygon.dbo.Polygon p
							WHERE  p.PolygonGeography.STContains(@blpuLocation) = 1;
				END;
			ELSE BEGIN
						INSERT MultiplePolygonCount ( UPRN, PolygonCount ) VALUES ( @uprn, @polygonCount );
				END;

			SET @recordCount = @recordCount + 1;

			IF @recordCount % 1000 = 0 PRINT 'Record: ' + CAST(@recordCount AS VARCHAR);

			FETCH NEXT FROM blpu_cursor
			INTO @uprn ,
					@blpuLocation;
		END;

	CLOSE blpu_cursor;
	DEALLOCATE blpu_cursor;

	SET NOCOUNT OFF;
GO
/****** Object:  StoredProcedure [dbo].[SelectPolygonsInRangeCommercialSalesV2]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[SelectPolygonsInRangeCommercialSalesV2]
    @minSize FLOAT,
    @maxSize FLOAT,
    @EastingMin FLOAT,
    @EastingMax FLOAT,
    @NorthingMin FLOAT,
    @NorthingMax FLOAT,
    @FootprintMin FLOAT,
    @FootprintMax FLOAT,
    @SaleDateStart DATETIME,
    @SaleDateEnd DATETIME,
    @ResidentialPropertyType VARCHAR(15) = NULL,
    @CommericalPropertyType VARCHAR(255) = NULL,
    @LeaseholdDate DATETIME,
    @CompanyReference UNIQUEIDENTIFIER,
    @BoundaryReference UNIQUEIDENTIFIER = NULL,
    @CommericalSalesPropertyType VARCHAR(255),
    @jsonOutput NVARCHAR(MAX) = -1 OUTPUT
AS
BEGIN

    SET NOCOUNT ON;

	DECLARE @SaleDateStartDefault DATETIME = CONVERT(DATETIME, '01/01/1990 00:00:00');

    DECLARE @ResidentialPropertyTypes TABLE (Item VARCHAR(30));
    DECLARE @CommericalPropertyTypes TABLE (Item VARCHAR(30));
    DECLARE @CommercialSalesPropertyTypes TABLE (Item VARCHAR(30));

    INSERT INTO @ResidentialPropertyTypes
    SELECT items
    FROM dbo.Split(@ResidentialPropertyType, ',');

    INSERT INTO @CommericalPropertyTypes
    SELECT items
    FROM dbo.Split(@CommericalPropertyType, ',');

    --INSERT INTO @CommercialSalesPropertyTypes
    --SELECT CASE WHEN items = 'Land' THEN 'L' ELSE items END FROM dbo.Split(@CommericalSalesPropertyType, ',')


		IF OBJECT_ID('#Source1') IS NOT NULL 
    BEGIN 
        DROP TABLE #Source1 
    END
    CREATE TABLE #Source1(
            InspireID bigint
    )

	IF @BoundaryReference IS NULL
		BEGIN
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE SingleEasting BETWEEN @EastingMin AND @EastingMax
				AND SingleNorthing BETWEEN @NorthingMin AND @NorthingMax	
		END
	ELSE
		BEGIN
			DECLARE @poly GEOGRAPHY
			SET @poly = (SELECT PolygonGeography FROM [BoundarySearch.View] (NOLOCK) WHERE Reference = @BoundaryReference)
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE @poly.STContains(poly.PolygonGeography) = 1
		END

    SELECT DISTINCT
        p.InspireID,
        p.WKT27700 AS Polygon,
        p.Area,
        p.SingleEasting,
        p.SingleNorthing,
        p.TitleNumber,
        p.Freehold
    INTO #Source2
    FROM #Source1 AS source
        LEFT JOIN NationalPolygon.dbo.[Polygon] p (NOLOCK)
            ON p.InspireID = source.InspireID
    WHERE p.TitleNumber IS NOT NULL
          AND p.Freehold IS NOT NULL;

    CREATE CLUSTERED INDEX Index_Source2Data
    ON #Source2
    (
        InspireID,
        TitleNumber,
        Freehold
    );

    SET @jsonOutput =
    (
        SELECT *
        FROM
        (
            SELECT DISTINCT
                ResultSet.InspireID,
                ResultSet.Area,
                ResultSet.SingleEasting,
                ResultSet.SingleNorthing,
                ResultSet.TitleNumber,
                ResultSet.Tenure,
                ResultSet.Polygon,
                DateProprietorAdded
            FROM
            (
                SELECT DISTINCT
                    [SourceData].InspireID,
                    [SourceData].TitleNumber,
                    [SourceData].Area,
                    [SourceData].SingleEasting,
                    [SourceData].SingleNorthing,
                    (CASE
                         WHEN [SourceData].Freehold = 1 THEN
                             'Freehold'
                         ELSE
                             'Leasehold'
                     END
                    ) AS Tenure,
                    (CASE
                         WHEN [SourceData].Freehold = 1 THEN
                             [SourceData].[Polygon]
                         ELSE
                             ''
                     END
                    ) AS Polygon,
                    O.Date_Proprietor_Added AS DateProprietorAdded
                FROM #Source2 AS SourceData
                    LEFT JOIN [NationalPolygon].[dbo].[TitleInfo] AS TitleInfo
                        ON TitleInfo.TitleNumber = SourceData.TitleNumber

                    --LEFT JOIN DataImport_BuildingFootprints (NOLOCK) ON DataImport_BuildingFootprints.INSPIREID = SourceData.InspireID

                    -- This voa join appears to only work for freehold URPN numbers.  As it is not returning any leasehold properties so the pins are vanishing from the map
                    --LEFT JOIN NationalPolygon.dbo.UprnPolygonId up ON up.InspireId = SourceData.InspireID
                    --LEFT JOIN [NationalPolygon].[dbo].[TitleNumberUPRN] (NOLOCK) AS TNU ON SourceData.TitleNumber = TNU.TitleNumber
                    --LEFT JOIN AddressBase.[dbo].AddressBaseCrossReference WITH (INDEX(IX_AddressBaseCrossReference_UPRN_SOURCE_CROSS_REFERENCE), NOLOCK) ON AddressBaseCrossReference.UPRN = TNU.UPRN AND AddressBaseCrossReference.SOURCE IN ('7666VN') --Commerical		
                    --LEFT JOIN VOA.dbo.VOADataLIST AS voaList (NOLOCK) ON voaList.AddressKey = AddressBaseCrossReference.CROSS_REFERENCE 
                    --LEFT JOIN [VOA].[dbo].[VOADataSMVType01] (NOLOCK) S ON S.UAM = AddressBaseCrossReference.CROSS_REFERENCE
                    --LEFT JOIN VOA.dbo.VOAPropertyTypes voaTypes (NOLOCK) ON voaTypes.DescriptionCode = voaList.DescriptionCode
                    --LEFT JOIN [dbo].AddressBaseClassification (NOLOCK) ON AddressBaseClassification.UPRN = AddressBaseCrossReference.UPRN

                    --LEFT JOIN [Zenzero.AssuredProperty.Data].[dbo].[PricePaidDataAddress] (NOLOCK) AS ppda ON ppda.UPRN =  TNU.UPRN
                    --LEFT JOIN [Zenzero.AssuredProperty.Data].[dbo].[PricePaidData] (NOLOCK) AS ppd ON ppd.PricePaidDataAddressID = ppda.ID
                    LEFT JOIN [Ownership].[dbo].[Ownership] AS O (NOLOCK)
                        ON O.Title_Number = SourceData.TitleNumber

                --		-- Apply additional filters here (building foot prints, prices, sales dates, leasehold date)   
                WHERE  
					  (
							@SaleDateStart = @SaleDateStartDefault
							OR 
							TitleInfo.LastSaleDate BETWEEN @SaleDateStart AND @SaleDateEnd
                      )
                      AND COALESCE(TitleInfo.TotalBuildingFootprint, 0)
                      BETWEEN @FootprintMin AND @FootprintMax
                      AND (
                              -- Apply residential filters
                              (
                                  TitleInfo.HasResi = 1
                                  AND (ISNULL(TitleInfo.BuildingType, 'X') IN (
                                                                                  SELECT Item FROM @ResidentialPropertyTypes
                                                                              )
                                      )
                              ) -- OR ISNULL(SourceData.PropertyType, 'X') = 'X') -- Do we really want to exclude 'not stated' types, since this drastically reduces the result set

                              -- Apply any commerical filters
                              --OR (PostcodeData.PropertyType IS NULL AND CASE WHEN AddressBaseClassification.CLASSIFICATION_CODE_SINGLE = 'L' THEN 'L' ELSE VOAPropertyTypes.PropertyType END IN (SELECT Item FROM @CommericalPropertyTypes))
                              OR (
									(
                                         TitleInfo.HasResi = 1
                                         AND 'Residential' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
									 OR (
                                         TitleInfo.HasLL = 1
                                         AND 'Licenced & Leisure' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
                                     OR (
                                            TitleInfo.HasIndustrial = 1
                                            AND 'Industrial' IN (
                                                                    SELECT Item FROM @CommericalPropertyTypes
                                                                )
                                        )
                                     OR (
                                            TitleInfo.HasOffices = 1
                                            AND 'Offices' IN (
                                                                 SELECT Item FROM @CommericalPropertyTypes
                                                             )
                                        )
                                     OR (
                                            TitleInfo.HasRetail = 1
                                            AND 'Retail' IN (
                                                                SELECT Item FROM @CommericalPropertyTypes
                                                            )
                                        )
                                     OR (
                                            TitleInfo.UPRNCount = 0
                                            AND 'Land' IN (
                                                              SELECT Item FROM @CommericalPropertyTypes
                                                          )
                                        )
                                     -- TODO: Remove this quick fix when TitleInfo is fixed
                                     -- Title number ON149096 doesn't pull through as the TitleInfo table doesn't show as a residential polygon
                                     OR (
                                        (
                                            SELECT COUNT(*) FROM @CommericalPropertyTypes
                                        ) = 6
                                        )
                                 )
                                 AND O.Price_Paid IS NOT NULL
								 AND (
										@SaleDateStart = @SaleDateStartDefault
										OR 
										O.Date_Proprietor_Added BETWEEN @SaleDateStart AND @SaleDateEnd
								 )
                          )
            ) AS ResultSet
            WHERE (
                      (
                          ResultSet.Tenure = 'Freehold'
                          OR ResultSet.Tenure IS NULL
                      )
                      OR (
                             (ResultSet.Tenure = 'Leasehold')
                             AND (
                                     @LeaseholdDate > '01/01/1753'
                                     AND ResultSet.DateProprietorAdded IS NOT NULL
                                     AND ResultSet.DateProprietorAdded >= @LeaseholdDate
                                 )
                             OR (@LeaseholdDate = '01/01/1753')
                         )
                  )
                  AND ResultSet.TitleNumber IS NOT NULL
        ) AS Source

        --ORDER BY TitleNumber DESC 

        --DROP TABLE IF EXISTS #Source1
        --DROP TABLE IF EXISTS #Source2

        FOR JSON AUTO
    );

    SET NOCOUNT OFF;

/*

 --EXEC SelectPolygonsInRangeCommercialSalesV2 0, 2147483647, 529687, 530187, 180071, 180571, 0, 2147483647, '01/01/1995 00:00:00', '01/12/2018 00:00:00', 'F,T,S,D,O,X', 'Industrial,Licenced & Leisure,Offices,Retail,Land,Other Property Types', '01/01/1970 00:00:00', '01/01/1970 00:00:00', '62982b78-de9a-4c6c-b5ad-7eadab13a7fd', null, 'Industrial,Licenced & Leisure,Offices,Retail,Land,Other Property Types'
 
 */
END
GO
/****** Object:  StoredProcedure [dbo].[SelectPolygonsInRangeCustomWithEPCCommercialSalesV2]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[SelectPolygonsInRangeCustomWithEPCCommercialSalesV2]
    @minSize FLOAT,
    @maxSize FLOAT,
    @EastingMin FLOAT,
    @EastingMax FLOAT,
    @NorthingMin FLOAT,
    @NorthingMax FLOAT,
    @FootprintMin FLOAT,
    @FootprintMax FLOAT,
    @SaleDateStart DATETIME,
    @SaleDateEnd DATETIME,
    @ResidentialPropertyType VARCHAR(15) = NULL,
    @CommericalPropertyType VARCHAR(255) = NULL,
    @LeaseholdDate DATETIME,
    @EPCType VARCHAR(25),
    @CompanyReference UNIQUEIDENTIFIER,
    @BoundaryReference UNIQUEIDENTIFIER = NULL,
    @CommericalSalesPropertyType VARCHAR(255),
    @jsonOutput NVARCHAR(MAX) = -1 OUTPUT
AS
BEGIN

    SET NOCOUNT ON;

	DECLARE @SaleDateStartDefault DATETIME = CONVERT(DATETIME, '01/01/1990 00:00:00');

    DECLARE @ResidentialPropertyTypes TABLE (Item VARCHAR(30));
    DECLARE @CommericalPropertyTypes TABLE (Item VARCHAR(30));
    --DECLARE @CommercialSalesPropertyTypes TABLE (Item VARCHAR(30))

    DECLARE @EPCTypes TABLE (Item VARCHAR(25));

    INSERT INTO @ResidentialPropertyTypes
    SELECT items
    FROM dbo.Split(@ResidentialPropertyType, ',');

    INSERT INTO @CommericalPropertyTypes
    SELECT items
    FROM dbo.Split(@CommericalPropertyType, ',');


    --INSERT INTO @CommercialSalesPropertyTypes
    --SELECT CASE WHEN items = 'Land' THEN 'L' ELSE items END FROM dbo.Split(@CommericalSalesPropertyType, ',')

    INSERT INTO @EPCTypes
    SELECT items
    FROM dbo.Split(@EPCType, ',');

		IF OBJECT_ID('#Source1') IS NOT NULL 
    BEGIN 
        DROP TABLE #Source1 
    END
    CREATE TABLE #Source1(
            InspireID bigint
    )

	IF @BoundaryReference IS NULL
		BEGIN
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE SingleEasting BETWEEN @EastingMin AND @EastingMax
				AND SingleNorthing BETWEEN @NorthingMin AND @NorthingMax	
		END
	ELSE
		BEGIN
			DECLARE @poly GEOGRAPHY
			SET @poly = (SELECT PolygonGeography FROM [BoundarySearch.View] (NOLOCK) WHERE Reference = @BoundaryReference)
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE @poly.STContains(poly.PolygonGeography) = 1
		END

    SELECT DISTINCT
        p.InspireID,
        p.WKT27700 AS Polygon,
        p.Area,
        p.SingleEasting,
        p.SingleNorthing,
        p.TitleNumber,
        p.Freehold
    INTO #Source2
    FROM #Source1 AS source
        LEFT JOIN NationalPolygon.dbo.[Polygon] p (NOLOCK)
            ON p.InspireID = source.InspireID
    WHERE p.TitleNumber IS NOT NULL
          AND p.Freehold IS NOT NULL;

    CREATE CLUSTERED INDEX Index_Source2Data
    ON #Source2
    (
        InspireID,
        TitleNumber,
        Freehold
    );

    SET @jsonOutput =
    (
        SELECT *
        FROM
        (
            SELECT DISTINCT
                ResultSet.InspireID,
                ResultSet.Area,
                ResultSet.SingleEasting,
                ResultSet.SingleNorthing,
                ResultSet.TitleNumber,
                ResultSet.Tenure,
                ResultSet.Polygon,
                DateProprietorAdded
            FROM
            (
                SELECT DISTINCT
                    [SourceData].InspireID,
                    [SourceData].TitleNumber,
                    [SourceData].Area,
                    [SourceData].SingleEasting,
                    [SourceData].SingleNorthing,
                    (CASE
                         WHEN [SourceData].Freehold = 1 THEN
                             'Freehold'
                         ELSE
                             'Leasehold'
                     END
                    ) AS Tenure,
                    (CASE
                         WHEN [SourceData].Freehold = 1 THEN
                             [SourceData].[Polygon]
                         ELSE
                             ''
                     END
                    ) AS Polygon,
                    O.Date_Proprietor_Added AS DateProprietorAdded
                FROM #Source2 AS SourceData
                    LEFT JOIN [NationalPolygon].[dbo].[TitleInfo] AS TitleInfo
                        ON TitleInfo.TitleNumber = SourceData.TitleNumber

                    --LEFT JOIN DataImport_BuildingFootprints (NOLOCK)
                    --    ON DataImport_BuildingFootprints.INSPIREID = SourceData.InspireID

                    -- This voa join appears to only work for freehold URPN numbers.  As it is not returning any leasehold properties so the pins are vanishing from the map
                    --            LEFT JOIN [NationalPolygon].[dbo].[TitleNumberUPRN] AS TNU (NOLOCK) ON SourceData.TitleNumber = TNU.TitleNumber
                    --            INNER JOIN
                    --            (
                    --                SELECT E.UPRN,
                    --                       E.CURRENT_ENERGY_RATING
                    --                FROM [EPC].[dbo].[DomesticNonDomesticEPC.View] AS E (NOLOCK)
                    --                    INNER JOIN
                    --                    (
                    --                        SELECT UPRN,
                    --                               MAX(INSPECTION_DATE) AS [INSPECTION_DATE]
                    --                        FROM [EPC].[dbo].[DomesticNonDomesticEPC.View] (NOLOCK)
                    --                        GROUP BY UPRN
                    --                    ) AS t
                    --                        ON E.UPRN = t.UPRN
                    --                           AND E.INSPECTION_DATE = t.INSPECTION_DATE
                    --            ) AS EPCD
                    --                ON EPCD.UPRN = TNU.UPRN
                    --                   AND EPCD.CURRENT_ENERGY_RATING IN (
                    --                                                         SELECT Item FROM @EPCTypes
                    --                                                     )
                    --            LEFT JOIN AddressBase.[dbo].AddressBaseCrossReference (NOLOCK)-- WITH (INDEX(IX_AddressBaseCrossReference_UPRN_SOURCE_CROSS_REFERENCE), NOLOCK)
                    --                ON AddressBaseCrossReference.UPRN = TNU.UPRN
                    --                   AND AddressBaseCrossReference.SOURCE IN ( '7666VN' ) --Commerical		
                    --            LEFT JOIN VOA.dbo.VOADataLIST AS voaList (NOLOCK)
                    --                ON voaList.AddressKey = AddressBaseCrossReference.CROSS_REFERENCE
                    --            LEFT JOIN VOA.dbo.VOAPropertyTypes voaTypes (NOLOCK)
                    --                ON voaTypes.DescriptionCode = voaList.DescriptionCode
                    --LEFT JOIN [VOA].[dbo].[VOADataSMVType01] S (NOLOCK)
                    --	ON S.UAM = AddressBaseCrossReference.CROSS_REFERENCE
                    --            LEFT JOIN [dbo].AddressBaseClassification (NOLOCK)
                    --                ON AddressBaseClassification.UPRN = AddressBaseCrossReference.UPRN
                    --            LEFT JOIN [Zenzero.AssuredProperty.Data].[dbo].[PricePaidDataAddress] (NOLOCK) AS ppda
                    --                ON ppda.UPRN = TNU.UPRN
                    --            LEFT JOIN [Zenzero.AssuredProperty.Data].[dbo].[PricePaidData] (NOLOCK) AS ppd
                    --                ON ppd.PricePaidDataAddressID = ppda.ID
                    LEFT JOIN [Ownership].dbo.[Ownership] O (NOLOCK)
                        ON O.Title_Number = SourceData.TitleNumber

                -- Apply additional filters here (building foot prints, prices, sales dates, leasehold date)   
                WHERE TitleInfo.EPC IN (
                                           SELECT Item FROM @EPCTypes
                                       )
					  AND 
					  (
							@SaleDateStart = @SaleDateStartDefault
							OR 
							TitleInfo.LastSaleDate BETWEEN @SaleDateStart AND @SaleDateEnd
                      )
                      AND COALESCE(TitleInfo.TotalBuildingFootprint, 0)
                      BETWEEN @FootprintMin AND @FootprintMax
                      AND (
                              -- Apply residential filters
                              (
                                  TitleInfo.HasResi = 1
                                  AND (ISNULL(TitleInfo.BuildingType, 'X') IN (
                                                                                  SELECT Item FROM @ResidentialPropertyTypes
                                                                              )
                                      )
                              ) -- Do we really want to exclude 'not stated' types, since this drastically reduces the result set

                              -- Apply any commerical filters
                              --OR (PostcodeData.PropertyType IS NULL AND CASE WHEN AddressBaseClassification.CLASSIFICATION_CODE_SINGLE = 'L' THEN 'L' ELSE VOAPropertyTypes.PropertyType END IN (SELECT Item FROM @CommericalPropertyTypes))
                              OR (
									(
                                         TitleInfo.HasResi = 1
                                         AND 'Residential' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
									 OR (
                                         TitleInfo.HasLL = 1
                                         AND 'Licenced & Leisure' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
                                     OR (
                                            TitleInfo.HasIndustrial = 1
                                            AND 'Industrial' IN (
                                                                    SELECT Item FROM @CommericalPropertyTypes
                                                                )
                                        )
                                     OR (
                                            TitleInfo.HasOffices = 1
                                            AND 'Offices' IN (
                                                                 SELECT Item FROM @CommericalPropertyTypes
                                                             )
                                        )
                                     OR (
                                            TitleInfo.HasRetail = 1
                                            AND 'Retail' IN (
                                                                SELECT Item FROM @CommericalPropertyTypes
                                                            )
                                        )
                                     OR (
                                            TitleInfo.UPRNCount = 0
                                            AND 'Land' IN (
                                                              SELECT Item FROM @CommericalPropertyTypes
                                                          )
                                        )
                                     -- TODO: Remove this quick fix when TitleInfo is fixed
                                     -- Title number ON149096 doesn't pull through as the TitleInfo table doesn't show as a residential polygon
                                     OR (
                                        (
                                            SELECT COUNT(*) FROM @CommericalPropertyTypes
                                        ) = 6
                                        )
                                 )
                          )
            ) AS ResultSet
            WHERE (
                      (
                          ResultSet.Tenure = 'Freehold'
                          OR ResultSet.Tenure IS NULL
                      )
                      OR (
                             (ResultSet.Tenure = 'Leasehold')
                             AND (
                                     @LeaseholdDate > '01/01/1753'
                                     AND ResultSet.DateProprietorAdded IS NOT NULL
                                     AND ResultSet.DateProprietorAdded >= @LeaseholdDate
                                 )
                             OR (@LeaseholdDate = '01/01/1753')
                         )
                  )
                  AND ResultSet.TitleNumber IS NOT NULL
        ) AS Source
        --ORDER BY TitleNumber DESC

        --DROP TABLE IF EXISTS #Source1
        --DROP TABLE IF EXISTS #Source2

        FOR JSON AUTO
    );

    SET NOCOUNT OFF;

/*

 EXEC SelectPolygonsInRangeCustomWithEPCCommercialSalesV2 
	0, 
	2147483647, 
	529687, 
	530187, 
	180071, 
	180571, 
	0, 
	2147483647, 
	'01/12/2015 00:00:00', 
	'01/12/2018 00:00:00', 
	'F,T,S,D,O,X', 
	'Industrial,Retail,Land,Other Property Types', 
	'01/01/1970 00:00:00', 
	'A,B,C,D', 
	'62982B78-DE9A-4C6C-B5AD-7EADAB13A7FD', 
	null,
	'Industrial,Licenced & Leisure,Offices,Retail,Land,Other Property Types'

 */

END
GO
/****** Object:  StoredProcedure [dbo].[SelectPolygonsInRangeCustomWithEPCV2]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[SelectPolygonsInRangeCustomWithEPCV2]
    @minSize FLOAT,
    @maxSize FLOAT,
    @EastingMin FLOAT,
    @EastingMax FLOAT,
    @NorthingMin FLOAT,
    @NorthingMax FLOAT,
    @FootprintMin FLOAT,
    @FootprintMax FLOAT,
    @SaleDateStart DATETIME,
    @SaleDateEnd DATETIME,
    @ResidentialPropertyType VARCHAR(15) = NULL,
    @CommericalPropertyType VARCHAR(255) = NULL,
    @LeaseholdDate DATETIME,
    @EPCType VARCHAR(25),
    @CompanyReference UNIQUEIDENTIFIER,
    @BoundaryReference UNIQUEIDENTIFIER = NULL,
    @jsonOutput NVARCHAR(MAX) = -1 OUTPUT
AS
BEGIN

    SET NOCOUNT ON;

	DECLARE @SaleDateStartDefault DATETIME = CONVERT(DATETIME, '01/01/1990 00:00:00');

    DECLARE @ResidentialPropertyTypes TABLE (Item VARCHAR(30));
    DECLARE @CommericalPropertyTypes TABLE (Item VARCHAR(30));

    DECLARE @EPCTypes TABLE (Item VARCHAR(25));

    INSERT INTO @ResidentialPropertyTypes
    SELECT items
    FROM dbo.Split(@ResidentialPropertyType, ',');

    INSERT INTO @CommericalPropertyTypes
    SELECT items
    FROM dbo.Split(@CommericalPropertyType, ',');

    INSERT INTO @EPCTypes
    SELECT items
    FROM dbo.Split(@EPCType, ',');

	IF OBJECT_ID('#Source1') IS NOT NULL 
    BEGIN 
        DROP TABLE #Source1 
    END
    CREATE TABLE #Source1(
            InspireID bigint
    )

	IF @BoundaryReference IS NULL
		BEGIN
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE SingleEasting BETWEEN @EastingMin AND @EastingMax
				AND SingleNorthing BETWEEN @NorthingMin AND @NorthingMax
		END
	ELSE
		BEGIN
			DECLARE @poly GEOGRAPHY
			SET @poly = (SELECT PolygonGeography FROM [BoundarySearch.View] (NOLOCK) WHERE Reference = @BoundaryReference)
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE @poly.STContains(poly.PolygonGeography) = 1
		END

    SELECT DISTINCT
        p.InspireID,
        p.WKT27700 AS Polygon,
        p.Area,
        p.SingleEasting,
        p.SingleNorthing,
        p.TitleNumber,
        p.Freehold
    INTO #Source2
    FROM #Source1 AS source
        LEFT JOIN NationalPolygon.dbo.[Polygon] p (NOLOCK)
            ON p.InspireID = source.InspireID
    WHERE p.TitleNumber IS NOT NULL
          AND p.Freehold IS NOT NULL;

    CREATE CLUSTERED INDEX Index_Source2Data
    ON #Source2
    (
        InspireID,
        TitleNumber,
        Freehold
    );

    SET @jsonOutput =
    (
        SELECT *
        FROM
        (
            SELECT DISTINCT
                ResultSet.InspireID,
                ResultSet.Area,
                ResultSet.SingleEasting,
                ResultSet.SingleNorthing,
                ResultSet.TitleNumber,
                ResultSet.Tenure,
                ResultSet.Polygon
            FROM
            (
                SELECT DISTINCT
                    [SourceData].InspireID,
                    [SourceData].Freehold,
                    [SourceData].Area,
                    [SourceData].SingleEasting,
                    [SourceData].SingleNorthing,
                    [SourceData].TitleNumber,
                    (CASE
                         WHEN Freehold = 1 THEN
                             'Freehold'
                         ELSE
                             'Leasehold'
                     END
                    ) AS Tenure,
                    (CASE
                         WHEN Freehold = 1 THEN
                             [Polygon]
                         ELSE
                             ''
                     END
                    ) AS Polygon
                FROM #Source2 AS SourceData
                    LEFT JOIN [NationalPolygon].[dbo].[TitleInfo] AS TitleInfo
                        ON TitleInfo.TitleNumber = SourceData.TitleNumber

                -- Apply additional filters here (building foot prints, prices, sales dates, leasehold date)   
                WHERE TitleInfo.EPC IN (
                                           SELECT Item FROM @EPCTypes
                                       )
                      AND (
							@SaleDateStart = @SaleDateStartDefault
							OR 
							TitleInfo.LastSaleDate BETWEEN @SaleDateStart AND @SaleDateEnd
                      )
                      AND COALESCE(TitleInfo.TotalBuildingFootprint, 0)
                      BETWEEN @FootprintMin AND @FootprintMax
                      AND (
                              -- Apply residential filters
                              (
                                  TitleInfo.HasResi = 1
                                  AND (ISNULL(TitleInfo.BuildingType, 'X') IN (
                                                                                  SELECT Item FROM @ResidentialPropertyTypes
                                                                              )
                                      )
                              )
                              -- Apply any commerical filters
                              --OR (PostcodeData.PropertyType IS NULL AND CASE WHEN AddressBaseClassification.CLASSIFICATION_CODE_SINGLE = 'L' THEN 'L' ELSE VOAPropertyTypes.PropertyType END IN (SELECT Item FROM @CommericalPropertyTypes))
                              OR (
									  -- Apply any commerical filters     
									  (
                                         TitleInfo.HasResi = 1
                                         AND 'Residential' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
									  OR
                                     (
                                         TitleInfo.HasLL = 1
                                         AND 'Licenced & Leisure' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
                                     OR (
                                            TitleInfo.HasIndustrial = 1
                                            AND 'Industrial' IN (
                                                                    SELECT Item FROM @CommericalPropertyTypes
                                                                )
                                        )
                                     OR (
                                            TitleInfo.HasOffices = 1
                                            AND 'Offices' IN (
                                                                 SELECT Item FROM @CommericalPropertyTypes
                                                             )
                                        )
                                     OR (
                                            TitleInfo.HasRetail = 1
                                            AND 'Retail' IN (
                                                                SELECT Item FROM @CommericalPropertyTypes
                                                            )
                                        )
                                     OR (
                                            TitleInfo.UPRNCount = 0
                                            AND 'Land' IN (
                                                              SELECT Item FROM @CommericalPropertyTypes
                                                          )
                                        )
                                     -- TODO: Remove this quick fix when TitleInfo is fixed
                                     -- Title number ON149096 doesn't pull through as the TitleInfo table doesn't show as a residential polygon
                                     OR (
                                        (
                                            SELECT COUNT(*) FROM @CommericalPropertyTypes
                                        ) = 6
                                        )
                                 )
                          )
            ) AS ResultSet
                LEFT JOIN [Ownership] (NOLOCK)
                    ON [Ownership].TitleNo = ResultSet.TitleNumber
            WHERE (
                      (
                          COALESCE(ResultSet.Tenure, Ownership.Tenure) = 'Freehold'
                          OR COALESCE(ResultSet.Tenure, Ownership.Tenure) IS NULL
                      )
                      OR (
                             (COALESCE(ResultSet.Tenure, Ownership.Tenure) = 'Leasehold')
                             AND (
                                     @LeaseholdDate > '01/01/1753'
                                     AND [Ownership].DateProprietorAdded IS NOT NULL
                                     AND [Ownership].DateProprietorAdded >= @LeaseholdDate
                                 )
                             OR (@LeaseholdDate = '01/01/1753')
                         )
                  )
                  AND ResultSet.TitleNumber IS NOT NULL
        ) AS Source
        --ORDER BY TitleNumber DESC

        --DROP TABLE IF EXISTS #Source1
        --DROP TABLE IF EXISTS #Source2

        FOR JSON AUTO
    );

    SET NOCOUNT OFF;

/*

 EXEC SelectPolygonsInRangeCustomWithEPCV2 
	 0, 
	 2147483647, 
	 529031.779556601, 
	 531031.779556601, 
	 179374.709153827, 
	 181374.709153827, 
	 0, 
	 2147483647, 
	 '01/12/2015 00:00:00', 
	 '01/12/2018 00:00:00', 
	 'F,T,S,D,O,X', 
	 'Industrial,Retail,Land,Other Property Types', 
	 '', 
	 'A,B,C,D', 
	 '62982B78-DE9A-4C6C-B5AD-7EADAB13A7FD', 
	 null

 */

END
GO
/****** Object:  StoredProcedure [dbo].[SelectPolygonsInRangeIndustrialPDRightsV2]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
CREATE   PROCEDURE [dbo].[SelectPolygonsInRangeIndustrialPDRightsV2]

	@EastingMin FLOAT,
	@EastingMax FLOAT,
	@NorthingMin FLOAT,
	@NorthingMax FLOAT,
	@CompanyReference UNIQUEIDENTIFIER,
	@BoundaryReference UNIQUEIDENTIFIER = NULL,
	@SaleDateStart DATETIME,
	@jsonOutput NVARCHAR(MAX) = -1 OUTPUT 

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @SaleDateStartDefault DATETIME = CONVERT(DATETIME, '01/01/1990 00:00:00');

	IF OBJECT_ID('#Source1') IS NOT NULL 
    BEGIN 
        DROP TABLE #Source1 
    END
    CREATE TABLE #Source1(
            InspireID bigint
    )

	IF @BoundaryReference IS NULL
		BEGIN
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE SingleEasting BETWEEN @EastingMin AND @EastingMax
				AND SingleNorthing BETWEEN @NorthingMin AND @NorthingMax	
		END
	ELSE
		BEGIN
			DECLARE @poly GEOGRAPHY
			SET @poly = (SELECT PolygonGeography FROM [BoundarySearch.View] (NOLOCK) WHERE Reference = @BoundaryReference)
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE @poly.STContains(poly.PolygonGeography) = 1
		END

	--DECLARE @poly GEOGRAPHY

	--IF @BoundaryReference IS NULL
	--	SET @poly = (SELECT NationalPolygon.dbo.CreatePolygonFromENMaxMins(@EastingMin, @EastingMax, @NorthingMin, @NorthingMax))
	--ELSE
	--	SET @poly = (SELECT PolygonGeography FROM [BoundarySearch.View] (NOLOCK) WHERE Reference = @BoundaryReference)
	 
	--SELECT 
	--	poly.InspireID
	--INTO #Source1
	--FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
	--WHERE SingleEasting BETWEEN @EastingMin AND @EastingMax
	--AND SingleNorthing BETWEEN @NorthingMin AND @NorthingMax
			
	SELECT DISTINCT p.InspireID, p.WKT27700 AS Polygon, p.Area, p.SingleEasting, p.SingleNorthing, p.TitleNumber, p.Freehold
	INTO #Source2
	FROM #Source1 AS source
	LEFT JOIN NationalPolygon.dbo.[Polygon] p (NOLOCK) ON p.InspireID = source.InspireID
	WHERE p.TitleNumber IS NOT NULL
    AND p.Freehold IS NOT NULL
	  
	CREATE CLUSTERED INDEX Index_Source2Data ON #Source2 (InspireID, TitleNumber, Freehold)

	SET @jsonOutput =
	(SELECT *
	FROM
	(
		SELECT DISTINCT

			ResultSet.InspireID,  
			ResultSet.Area,
			ResultSet.SingleEasting,
			ResultSet.SingleNorthing, 
			ResultSet.TitleNumber,
			(CASE WHEN ResultSet.Freehold = 1 THEN 'Freehold' ELSE 'Leasehold' END) AS Tenure,
			(CASE WHEN ResultSet.Freehold = 1 THEN ResultSet.[Polygon] ELSE '' END) AS Polygon,
			'false' AS HasPlanningData,
			'false' AS HasAvailabilityData

		FROM
		(
			SELECT DISTINCT
				[SourceData].InspireID, 
				[SourceData].Area,
			[SourceData].SingleEasting,
			[SourceData].SingleNorthing, 
				[SourceData].Freehold,
				[SourceData].TitleNumber,
				[SourceData].Polygon
			
			FROM #Source2 AS SourceData

			LEFT JOIN [NationalPolygon].[dbo].[TitleInfo] AS TitleInfo ON TitleInfo.TitleNumber = SourceData.TitleNumber

			--LEFT JOIN DataImport_BuildingFootprints (NOLOCK) ON DataImport_BuildingFootprints.INSPIREID = SourceData.InspireID

			--LEFT JOIN [NationalPolygon].[dbo].[TitleNumberUPRN] AS TNU (NOLOCK) ON SourceData.TitleNumber = TNU.TitleNumber
			--LEFT JOIN AddressBase.[dbo].AddressBaseCrossReference WITH (INDEX(IX_AddressBaseCrossReference_UPRN_SOURCE_CROSS_REFERENCE), NOLOCK) ON AddressBaseCrossReference.UPRN = TNU.UPRN AND AddressBaseCrossReference.SOURCE IN ('7666VN') --Commerical		
			--LEFT JOIN VOA.dbo.VOADataLIST AS voaList (NOLOCK) ON voaList.AddressKey = AddressBaseCrossReference.CROSS_REFERENCE 
			--LEFT JOIN VOA.dbo.VOAPropertyTypes voaTypes (NOLOCK) ON voaTypes.DescriptionCode = voaList.DescriptionCode
			--LEFT JOIN [VOA].[dbo].[VOADataSMVType01] S (NOLOCK) on S.UAM = AddressBaseCrossReference.CROSS_REFERENCE
			INNER JOIN [Ownership].[dbo].[Ownership] AS O (NOLOCK) ON O.Title_Number = SourceData.TitleNumber
			
			WHERE TitleInfo.HasIndustrial = 1
			AND COALESCE(TitleInfo.TotalBuildingFootprint, 0) >= 500
			AND (
				@SaleDateStart = @SaleDateStartDefault
				OR 
				O.Date_Proprietor_Added >= @SaleDateStart
            )

		) AS ResultSet
	
	) AS Source

	--ORDER BY TitleNumber DESC 

	--DROP TABLE IF EXISTS #Source1
	--DROP TABLE IF EXISTS #Source2

	FOR JSON AUTO)

	SET NOCOUNT OFF

END

/*

EXEC [dbo].[SelectPolygonsInRangeIndustrialPDRightsV2] 529687, 530187, 180071, 180571, '62982b78-de9a-4c6c-b5ad-7eadab13a7fd', null, '11/01/2010 00:00:00'

*/
GO
/****** Object:  StoredProcedure [dbo].[SelectPolygonsInRangeOfficePDRightsV2]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
CREATE   PROCEDURE [dbo].[SelectPolygonsInRangeOfficePDRightsV2]

	@EastingMin FLOAT,
	@EastingMax FLOAT,
	@NorthingMin FLOAT,
	@NorthingMax FLOAT,
	@CompanyReference UNIQUEIDENTIFIER,
	@BoundaryReference UNIQUEIDENTIFIER = NULL,
	@SaleDateStart DATETIME,
	@jsonOutput NVARCHAR(MAX) = -1 OUTPUT 

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SaleDateStartDefault DATETIME = CONVERT(DATETIME, '01/01/1990 00:00:00');

	IF OBJECT_ID('#Source1') IS NOT NULL 
    BEGIN 
        DROP TABLE #Source1 
    END
    CREATE TABLE #Source1(
            InspireID bigint
    )

	IF @BoundaryReference IS NULL
		BEGIN
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE SingleEasting BETWEEN @EastingMin AND @EastingMax
				AND SingleNorthing BETWEEN @NorthingMin AND @NorthingMax	
		END
	ELSE
		BEGIN
			DECLARE @poly GEOGRAPHY
			SET @poly = (SELECT PolygonGeography FROM [BoundarySearch.View] (NOLOCK) WHERE Reference = @BoundaryReference)
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE @poly.STContains(poly.PolygonGeography) = 1
		END

	--DECLARE @poly GEOGRAPHY

	--IF @BoundaryReference IS NULL
	--	SET @poly = (SELECT NationalPolygon.dbo.CreatePolygonFromENMaxMins(@EastingMin, @EastingMax, @NorthingMin, @NorthingMax))
	--ELSE
	--	SET @poly = (SELECT PolygonGeography FROM [BoundarySearch.View] WHERE Reference = @BoundaryReference)
	 
	--SELECT 
	--	poly.InspireID
	--INTO #Source1
	--FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
	--WHERE SingleEasting BETWEEN @EastingMin AND @EastingMax
	--AND SingleNorthing BETWEEN @NorthingMin AND @NorthingMax
			
	SELECT DISTINCT p.InspireID, p.WKT27700 AS Polygon, p.Area, p.SingleEasting, p.SingleNorthing, p.TitleNumber, p.Freehold
	INTO #Source2
	FROM #Source1 AS source
	LEFT JOIN NationalPolygon.dbo.[Polygon] p (NOLOCK) ON p.InspireID = source.InspireID
	WHERE p.TitleNumber IS NOT NULL
    AND p.Freehold IS NOT NULL
	  
	CREATE CLUSTERED INDEX Index_Source2Data ON #Source2 (InspireID, TitleNumber, Freehold)

	SET @jsonOutput =
	(SELECT *
	FROM
	(
		SELECT DISTINCT

			ResultSet.InspireID,  
			ResultSet.Area,
			ResultSet.SingleEasting,
			ResultSet.SingleNorthing, 
			ResultSet.TitleNumber,
			(CASE WHEN ResultSet.Freehold = 1 THEN 'Freehold' ELSE 'Leasehold' END) AS Tenure,
			(CASE WHEN ResultSet.Freehold = 1 THEN ResultSet.[Polygon] ELSE '' END) AS Polygon,
			'false' AS HasPlanningData,
			'false' AS HasAvailabilityData

		FROM
		(
			SELECT DISTINCT
				[SourceData].InspireID, 
				[SourceData].Area,
			[SourceData].SingleEasting,
			[SourceData].SingleNorthing, 
				[SourceData].Freehold,
				[SourceData].TitleNumber,
				[SourceData].Polygon
			
			FROM #Source2 AS SourceData

			LEFT JOIN [NationalPolygon].[dbo].[TitleInfo] AS TitleInfo ON TitleInfo.TitleNumber = SourceData.TitleNumber

			--LEFT JOIN [NationalPolygon].[dbo].[TitleNumberUPRN] AS TNU ON SourceData.TitleNumber = TNU.TitleNumber
			--LEFT JOIN AddressBase.[dbo].AddressBaseCrossReference WITH (INDEX(IX_AddressBaseCrossReference_UPRN_SOURCE_CROSS_REFERENCE), NOLOCK) ON AddressBaseCrossReference.UPRN = TNU.UPRN AND AddressBaseCrossReference.SOURCE IN ('7666VN') --Commerical		
			--LEFT JOIN VOA.dbo.VOADataLIST AS voaList (NOLOCK) ON voaList.AddressKey = AddressBaseCrossReference.CROSS_REFERENCE 
			--LEFT JOIN VOA.dbo.VOAPropertyTypes voaTypes (NOLOCK) ON voaTypes.DescriptionCode = voaList.DescriptionCode
			--LEFT JOIN [VOA].[dbo].[VOADataSMVType01] S on S.UAM = AddressBaseCrossReference.CROSS_REFERENCE
			INNER JOIN [Ownership].[dbo].[Ownership] AS O ON O.Title_Number = SourceData.TitleNumber
			
			WHERE TitleInfo.HasOffices = 1
			AND (
				@SaleDateStart = @SaleDateStartDefault
				OR 
				O.Date_Proprietor_Added >= @SaleDateStart
			)

		) AS ResultSet
	
	) AS Source

	--ORDER BY TitleNumber DESC 

	--DROP TABLE IF EXISTS #Source1
	--DROP TABLE IF EXISTS #Source2

	FOR JSON AUTO)

	SET NOCOUNT OFF

END

/*

EXEC [dbo].[SelectPolygonsInRangeOfficePDRightsV2] 529031.779556601, 531031.779556601, 179374.709153827, 181374.709153827, '62982b78-de9a-4c6c-b5ad-7eadab13a7fd', null, '11/01/2010 00:00:00'

*/
GO
/****** Object:  StoredProcedure [dbo].[SelectPolygonsInRangePropertiesSoldV2]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[SelectPolygonsInRangePropertiesSoldV2]
    @minSize FLOAT,
    @maxSize FLOAT,
    @EastingMin FLOAT,
    @EastingMax FLOAT,
    @NorthingMin FLOAT,
    @NorthingMax FLOAT,
    @FootprintMin FLOAT,
    @FootprintMax FLOAT,
    @SaleDateStart DATETIME,
    @SaleDateEnd DATETIME,
    @ResidentialPropertyType VARCHAR(15) = NULL,
    @CommericalPropertyType VARCHAR(255) = NULL,
    @LeaseholdDate DATETIME,
    @CompanyReference UNIQUEIDENTIFIER,
    @BoundaryReference UNIQUEIDENTIFIER = NULL,
	@SoldMin INT,
	@SoldMax INT,
    @jsonOutput NVARCHAR(MAX) = -1 OUTPUT
AS
BEGIN


    SET NOCOUNT ON;

    DECLARE @SaleDateStartDefault DATETIME = CONVERT(DATETIME, '01/01/1990 00:00:00');

    DECLARE @ResidentialPropertyTypes TABLE (Item VARCHAR(30));
    DECLARE @CommericalPropertyTypes TABLE (Item VARCHAR(30));

    INSERT INTO @ResidentialPropertyTypes
    SELECT items
    FROM dbo.Split(@ResidentialPropertyType, ',');

    INSERT INTO @CommericalPropertyTypes
    SELECT items
    FROM dbo.Split(@CommericalPropertyType, ',');

	IF OBJECT_ID('#Source1') IS NOT NULL 
    BEGIN 
        DROP TABLE #Source1 
    END
    CREATE TABLE #Source1(
            InspireID bigint
    )

	IF @BoundaryReference IS NULL
		BEGIN
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE SingleEasting BETWEEN @EastingMin AND @EastingMax
				AND SingleNorthing BETWEEN @NorthingMin AND @NorthingMax	
				AND (poly.Area BETWEEN @minSize AND @maxSize)
		END
	ELSE
		BEGIN
			DECLARE @poly GEOGRAPHY
			SET @poly = (SELECT PolygonGeography FROM [BoundarySearch.View] (NOLOCK) WHERE Reference = @BoundaryReference)
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE @poly.STContains(poly.PolygonGeography) = 1
				AND (poly.Area
					BETWEEN @minSize AND @maxSize
				)
		END

	SELECT DISTINCT p.InspireID, p.WKT27700 AS Polygon, p.Area, p.SingleEasting, p.SingleNorthing, p.TitleNumber, p.Freehold
	INTO #Source2
	FROM #Source1 AS source
	LEFT JOIN NationalPolygon.dbo.[Polygon] p (NOLOCK) ON p.InspireID = source.InspireID
	WHERE p.TitleNumber IS NOT NULL
    AND p.Freehold IS NOT NULL
	
    IF(@SoldMin != 0 OR @SoldMax != 1000)
    BEGIN
        DELETE #Source2
        WHERE [dbo].[OwnerOfTitleNumberInSaleMode](TitleNumber, @SoldMin, @SoldMax) = 0
    END



	CREATE CLUSTERED INDEX Index_Source2Data ON #Source2 (InspireID, TitleNumber, Freehold)

    SET @jsonOutput =
    (
        SELECT *
        FROM
        (
            SELECT DISTINCT
                InspireID,
                Area,
                SingleEasting,
                SingleNorthing,
                ResultSet.TitleNumber,
                ResultSet.Tenure,
				Polygon
            FROM
            (
                SELECT DISTINCT
                    [SourceData].InspireID,
					[SourceData].Area,
					[SourceData].SingleEasting,
					[SourceData].SingleNorthing,
                    (CASE WHEN [SourceData].Freehold = 1 THEN 'Freehold' ELSE 'Leasehold' END) AS Tenure,
					(CASE WHEN [SourceData].Freehold = 1 THEN [SourceData].[Polygon] ELSE '' END) AS Polygon,
                    [SourceData].TitleNumber
                FROM #Source2 AS SourceData
                    LEFT JOIN [NationalPolygon].[dbo].[TitleInfo] AS TitleInfo
                        ON TitleInfo.TitleNumber = SourceData.TitleNumber

                -- Apply additional filters here (building foot prints, prices, sales dates, leasehold date)   
                WHERE  
				     (
							@SaleDateStart = @SaleDateStartDefault
							OR 
							TitleInfo.LastSaleDate BETWEEN @SaleDateStart AND @SaleDateEnd
                      )
                      AND COALESCE(TitleInfo.TotalBuildingFootprint, 0)
                      BETWEEN @FootprintMin AND @FootprintMax
                      AND (
                              -- Apply residential filters
                              (
                                  TitleInfo.HasResi = 1
                                  AND (ISNULL(TitleInfo.BuildingType, 'X') IN (
                                                                                  SELECT Item FROM @ResidentialPropertyTypes
                                                                              )
                                      )
                              )
                              -- Apply any commerical filters                             
                              OR (
									(
                                         TitleInfo.HasResi = 1
                                         AND 'Residential' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
									 OR (
                                         TitleInfo.HasLL = 1
                                         AND 'Licenced & Leisure' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
                                     OR (
                                            TitleInfo.HasIndustrial = 1
                                            AND 'Industrial' IN (
                                                                    SELECT Item FROM @CommericalPropertyTypes
                                                                )
                                        )
                                     OR (
                                            TitleInfo.HasOffices = 1
                                            AND 'Offices' IN (
                                                                 SELECT Item FROM @CommericalPropertyTypes
                                                             )
                                        )
                                     OR (
                                            TitleInfo.HasRetail = 1
                                            AND 'Retail' IN (
                                                                SELECT Item FROM @CommericalPropertyTypes
                                                            )
                                        )
                                     OR (
                                            TitleInfo.UPRNCount = 0
                                            AND 'Land' IN (
                                                              SELECT Item FROM @CommericalPropertyTypes
                                                          )
                                        )
                                     -- TODO: Remove this quick fix when TitleInfo is fixed
                                     -- Title number ON149096 doesn't pull through as the TitleInfo table doesn't show as a residential polygon
                                     OR (
											(SELECT COUNT(*) FROM @CommericalPropertyTypes) = 6
                                        )
                                 )
                          )
            ) AS ResultSet
                
                LEFT JOIN [Ownership] (NOLOCK)
                    ON [Ownership].TitleNo = ResultSet.TitleNumber
            WHERE (
                      (
                          COALESCE(ResultSet.Tenure, Ownership.Tenure) = 'Freehold'
                          OR COALESCE(ResultSet.Tenure, Ownership.Tenure) IS NULL
                      )
                      OR (
                             (COALESCE(ResultSet.Tenure, Ownership.Tenure) = 'Leasehold')
                             AND (
                                     @LeaseholdDate > '01/01/1753'
                                     AND [Ownership].DateProprietorAdded IS NOT NULL
                                     AND [Ownership].DateProprietorAdded >= @LeaseholdDate
                                 )
                             OR (@LeaseholdDate = '01/01/1753')
                         )
                  )
                  AND ResultSet.TitleNumber IS NOT NULL
        ) AS Source

        --ORDER BY TitleNumber DESC 

        --DROP TABLE IF EXISTS #Source1
        --DROP TABLE IF EXISTS #Source2

        FOR JSON AUTO
    );

    SET NOCOUNT OFF;

/*

 EXEC SelectPolygonsInRangePropertiesSoldV2 
	6070.1711788272432, 
	2147483647, 
	529031.779556601, 
	531031.779556601, 
	179374.709153827, 
	181374.709153827, 
	0, 
	2147483647, 
	'01/01/1990 00:00:00', 
	'01/12/2019 00:00:00', 
	'F,T,S,D,X', 
	'Industrial,Licenced & Leisure,Offices,Retail,Land,Other Property Types', 
	'01/01/1970 00:00:00', 
	'01/01/1970 00:00:00', 
	'62982b78-de9a-4c6c-b5ad-7eadab13a7fd', 
	null,null,null,0,0

 SELECT * FROM [NationalPolygon].[dbo].[TitleInfo] WHERE TitleNumber = 'WR38849'

 */

END
GO
/****** Object:  StoredProcedure [dbo].[SelectPolygonsInRangeV2]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[SelectPolygonsInRangeV2]
    @minSize FLOAT,
    @maxSize FLOAT,
    @EastingMin FLOAT,
    @EastingMax FLOAT,
    @NorthingMin FLOAT,
    @NorthingMax FLOAT,
    @FootprintMin FLOAT,
    @FootprintMax FLOAT,
    @SaleDateStart DATETIME,
    @SaleDateEnd DATETIME,
    @ResidentialPropertyType VARCHAR(15) = NULL,
    @CommericalPropertyType VARCHAR(255) = NULL,
    @LeaseholdDate DATETIME,
    @CompanyReference UNIQUEIDENTIFIER,
    @BoundaryReference UNIQUEIDENTIFIER = NULL,
    @jsonOutput NVARCHAR(MAX) = -1 OUTPUT
AS
BEGIN


    SET NOCOUNT ON;

    DECLARE @SaleDateStartDefault DATETIME = CONVERT(DATETIME, '01/01/1990 00:00:00');

    DECLARE @ResidentialPropertyTypes TABLE (Item VARCHAR(30));
    DECLARE @CommericalPropertyTypes TABLE (Item VARCHAR(30));

    INSERT INTO @ResidentialPropertyTypes
    SELECT items
    FROM dbo.Split(@ResidentialPropertyType, ',');

    INSERT INTO @CommericalPropertyTypes
    SELECT items
    FROM dbo.Split(@CommericalPropertyType, ',');

	IF OBJECT_ID('#Source1') IS NOT NULL 
    BEGIN 
        DROP TABLE #Source1 
    END
    CREATE TABLE #Source1(
            InspireID bigint
    )

	IF @BoundaryReference IS NULL
		BEGIN
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE SingleEasting BETWEEN @EastingMin AND @EastingMax
				AND SingleNorthing BETWEEN @NorthingMin AND @NorthingMax	
				AND (poly.Area
					BETWEEN @minSize AND @maxSize
				)
		END
	ELSE
		BEGIN
			DECLARE @poly GEOGRAPHY
			SET @poly = (SELECT PolygonGeography FROM [BoundarySearch.View] (NOLOCK) WHERE Reference = @BoundaryReference)
			INSERT INTO #Source1 
				SELECT
				poly.InspireID
				FROM NationalPolygon.dbo.[Polygon] AS poly (NOLOCK)
				WHERE @poly.STContains(poly.PolygonGeography) = 1
				AND (poly.Area
					BETWEEN @minSize AND @maxSize
				)
		END

    SELECT DISTINCT
        p.InspireID,
        p.WKT27700 AS Polygon,
        p.Area,
        p.SingleEasting,
        p.SingleNorthing,
        p.TitleNumber,
        p.Freehold
    INTO #Source2
    FROM #Source1 AS source
        LEFT JOIN NationalPolygon.dbo.[Polygon] p (NOLOCK)
            ON p.InspireID = source.InspireID
    WHERE p.TitleNumber IS NOT NULL
          AND p.Freehold IS NOT NULL;

    CREATE CLUSTERED INDEX Index_Source2Data
    ON #Source2
    (
        InspireID,
        TitleNumber,
        Freehold
    );

    SET @jsonOutput =
    (
        SELECT *
        FROM
        (
            SELECT DISTINCT
                InspireID,
                Area,
                SingleEasting,
                SingleNorthing,
                ResultSet.TitleNumber,
                ResultSet.Tenure,
                Polygon
            FROM
            (
                SELECT DISTINCT
                    [SourceData].InspireID,
                    [SourceData].Area,
                    [SourceData].SingleEasting,
                    [SourceData].SingleNorthing,
                    (CASE
                         WHEN [SourceData].Freehold = 1 THEN
                             'Freehold'
                         ELSE
                             'Leasehold'
                     END
                    ) AS Tenure,
                    (CASE
                         WHEN [SourceData].Freehold = 1 THEN
                             [SourceData].[Polygon]
                         ELSE
                             ''
                     END
                    ) AS Polygon,
                    [SourceData].TitleNumber
                FROM #Source2 AS SourceData
                    LEFT JOIN [NationalPolygon].[dbo].[TitleInfo] AS TitleInfo
                        ON TitleInfo.TitleNumber = SourceData.TitleNumber

                -- Apply additional filters here (building foot prints, prices, sales dates, leasehold date)   
                WHERE  
					  (
							@SaleDateStart = @SaleDateStartDefault
							OR 
							TitleInfo.LastSaleDate BETWEEN @SaleDateStart AND @SaleDateEnd
                       )
                      AND COALESCE(TitleInfo.TotalBuildingFootprint, 0)
                      BETWEEN @FootprintMin AND @FootprintMax
                      AND (
                            -- Apply residential filters
                              (
                                  TitleInfo.HasResi = 1
                                  AND (ISNULL(TitleInfo.BuildingType, 'X') IN (
                                                                                  SELECT Item FROM @ResidentialPropertyTypes
                                                                              )
                                      )
                              )
                              -- Apply any commerical filters                             
                              OR (
									(
                                         TitleInfo.HasResi = 1
                                         AND 'Residential' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
                                     OR
                                     (
                                         TitleInfo.HasLL = 1
                                         AND 'Licenced & Leisure' IN (
                                                                         SELECT Item FROM @CommericalPropertyTypes
                                                                     )
                                     )
                                     OR (
                                            TitleInfo.HasIndustrial = 1
                                            AND 'Industrial' IN (
                                                                    SELECT Item FROM @CommericalPropertyTypes
                                                                )
                                        )
                                     OR (
                                            TitleInfo.HasOffices = 1
                                            AND 'Offices' IN (
                                                                 SELECT Item FROM @CommericalPropertyTypes
                                                             )
                                        )
                                     OR (
                                            TitleInfo.HasRetail = 1
                                            AND 'Retail' IN (
                                                                SELECT Item FROM @CommericalPropertyTypes
                                                            )
                                        )
                                     OR (
                                            TitleInfo.UPRNCount = 0
                                            AND 'Land' IN (
                                                              SELECT Item FROM @CommericalPropertyTypes
                                                          )
                                        )
                                     -- TODO: Remove this quick fix when TitleInfo is fixed
                                     -- Title number ON149096 doesn't pull through as the TitleInfo table doesn't show as a residential polygon
                                     OR (
                                        (
                                            SELECT COUNT(*) FROM @CommericalPropertyTypes
                                        ) = 6
                                        )
                                 )
                          )
            ) AS ResultSet
                LEFT JOIN [Ownership] (NOLOCK)
                    ON [Ownership].TitleNo = ResultSet.TitleNumber
            WHERE (
                      (
                          COALESCE(ResultSet.Tenure, Ownership.Tenure) = 'Freehold'
                          OR COALESCE(ResultSet.Tenure, Ownership.Tenure) IS NULL
                      )
                      OR (
                             (COALESCE(ResultSet.Tenure, Ownership.Tenure) = 'Leasehold')
                             AND (
                                     @LeaseholdDate > '01/01/1753'
                                     AND [Ownership].DateProprietorAdded IS NOT NULL
                                     AND [Ownership].DateProprietorAdded >= @LeaseholdDate
                                 )
                             OR (@LeaseholdDate = '01/01/1753')
                         )
                  )
                  AND ResultSet.TitleNumber IS NOT NULL
        ) AS Source

        --ORDER BY TitleNumber DESC 

        --DROP TABLE IF EXISTS #Source1
        --DROP TABLE IF EXISTS #Source2

        FOR JSON AUTO
    );

    SET NOCOUNT OFF;

/*

 EXEC SelectPolygonsInRangeV2 
	6070.1711788272432, 
	2147483647, 
	529031.779556601, 
	531031.779556601, 
	179374.709153827, 
	181374.709153827, 
	0, 
	2147483647, 
	'01/01/1995 00:00:00', 
	'01/12/2019 00:00:00', 
	'F,T,S,D,X', 
	'Industrial,Licenced & Leisure,Offices,Retail,Land,Other Property Types', 
	'01/01/1970 00:00:00', 
	'62982b78-de9a-4c6c-b5ad-7eadab13a7fd', 
	null,0,0

 */
END

PRINT 'DONE'
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_BuildingType]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 16th June 2021
 Description:	Updates the [BuildingType] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		OWN = Ownership

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields updated.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_BuildingType]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'OWN' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'OWN'
		BEGIN
			/* !!!!!!!!!! NEEDS REFINING TO SELECT OWNERSHIP UDATED TITLENUMBERS !!!!!!!!!!!!!*/
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		BuildingType = DataScience.dbo.ufnGetResiBuildingType(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_CommercialCount]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [CommercialCount] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the stored procedure what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		VOA = VOAUpdate (VOA)

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_CommercialCount]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'VOA' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'VOA'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM VOAUpdate.dbo.AffectedTitleNumbersFromLastUpdate;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END


	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		CommercialCount = DataScience.dbo.ufnGetCommercialCount(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_CommercialFootprint]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [CommercialFootprint] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the stored procedure what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		VOA = VOAUpdate (VOA)

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_CommercialFootprint]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'VOA' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'VOA'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM VOAUpdate.dbo.AffectedTitleNumbersFromLastUpdate;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END


	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		CommercialFootprint = DataScience.dbo.ufnGetVOAFootprints(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_DevPerc]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [DevPerc] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update

 OUTPUT PARAMETERS:
	@FieldsProcessed  - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = fail

 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_DevPerc]
	@IsCOU BIT,
	@FieldsProcessed  INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));
	IF @IsCOU = '1'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
	END
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*Perform the update. */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		DevPerc = TotalArea/TotalBuildingFootprint,
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed  = @@ROWCOUNT;

	RETURN 0;	
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_EPC]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [EPC] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = fail
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_EPC]
	@IsCOU BIT,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));
	IF @IsCOU = '1'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;	
	END
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM TitleInfo;
	END

	/*Perform the update. */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		EPC = DataScience.dbo.ufnGetLowestEPC(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_FirstSaleDate]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 25th June 2021
 Description:	Updates the [FirstSaleDate] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		OWN = Ownership

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_FirstSaleDate]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'OWN' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'OWN'
		BEGIN
			/* !!!!!!!!!! NEEDS REFINING TO SELECT OWNERSHIP UDATED TITLENUMBERS !!!!!!!!!!!!!*/
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		FirstSaleDate = DataScience.dbo.ufnGetFirstSaleDate(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_FirstSalePrice]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 24th June 2021
 Description:	Updates the [FirstSalePrice] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		OWN = Ownership

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields updated.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_FirstSalePrice]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'OWN' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'OWN'
		BEGIN
			/* !!!!!!!!!! NEEDS REFINING TO SELECT OWNERSHIP UDATED TITLENUMBERS !!!!!!!!!!!!!*/
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		FirstSalePrice = DataScience.dbo.ufnGetFirstSalePrice(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_HasCouncilTax]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [HasCouncilTax] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update

 OUTPUT PARAMETERS:
	@FlaggedHasCouncilTax - a count of all titles flagged as having Council Tax.
	@FieldsProcessed  - a count of all fields processed.

 RETURN VALUES:
	0 = success
	1 = fail

 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_HasCouncilTax]
	@IsCOU BIT,
	@FlaggedHasCouncilTax INT = 0 OUTPUT,
	@FieldsProcessed  INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));
	IF @IsCOU = '1'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
	END
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END;

	UPDATE NationalPolygon.dbo.TitleInfo
	SET HasCouncilTax = DataScience.dbo.ufnHasCouncilTax(TitleNumber)
	WHERE TitleNumber IN
	(SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed  = @@ROWCOUNT;

	SET @FlaggedHasCouncilTax = (SELECT COUNT(TitleNumber) 
										FROM NationalPolygon.dbo.TitleInfo
										WHERE TitleNumber IN
										(SELECT TitleNumber FROM @TitlesToUpdate)
										AND HasCouncilTax = '1');


	RETURN 0;	
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_HasIndustrial]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [HasIndustrial] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		VOA = VOAUpdate (VOA)

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields updated.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_HasIndustrial]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'VOA' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'VOA'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM VOAUpdate.dbo.AffectedTitleNumbersFromLastUpdate;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END


	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		HasIndustrial = DataScience.dbo.ufnHasIndustrial(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_HasLL]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [HasLL] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		VOA = VOAUpdate (VOA)

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_HasLL]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'VOA' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'VOA'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM VOAUpdate.dbo.AffectedTitleNumbersFromLastUpdate;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END


	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		HasLL = DataScience.dbo.ufnHasLL(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_HasOffices]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [HasOffices] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		VOA = VOAUpdate (VOA)

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_HasOffices]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'VOA' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'VOA'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM VOAUpdate.dbo.AffectedTitleNumbersFromLastUpdate;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END


	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		HasOffices = DataScience.dbo.ufnHasOffice(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_HasResi]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [HasResi] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields updated.

 RETURN VALUES:
	0 = success
	1 = fail

 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_HasResi]
	@IsCOU BIT,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));
	IF @IsCOU = '1'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
	END
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*Perform the update. */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		HasResi = DataScience.dbo.ufnHasResi(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;
		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_HasRetail]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 16th June 2021
 Description:	Updates the [HasRetail] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		VOA = VOAUpdate (VOA)

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_HasRetail]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'VOA' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'VOA'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM VOAUpdate.dbo.AffectedTitleNumbersFromLastUpdate;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END


	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		HasRetail = DataScience.dbo.ufnHasRetail(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_HasVOARates]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [HasVOARates] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update

 OUTPUT PARAMETERS:
	@FlaggedHasVOARates - as the name suggests a count of all titles flagged as having VOARates.
	@FieldsProcessed  = number of fields processed.

 RETURN VALUES:
	0 = success
	1 = fail

 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_HasVOARates]
	@IsCOU BIT,
	@FlaggedHasVOARates INT = 0 OUTPUT,
	@FieldsProcessed  INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));
	IF @IsCOU = '1'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
	END
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END;

	UPDATE NationalPolygon.dbo.TitleInfo
	SET HasVOARates = DataScience.dbo.ufnHasVOARates(TitleNumber),
	LastUpdated = GETDATE()
	WHERE TitleNumber IN
	(SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed  = @@ROWCOUNT;

	SET @FlaggedHasVOARates = (SELECT COUNT(TitleNumber) 
										FROM NationalPolygon.dbo.TitleInfo
										WHERE TitleNumber IN
										(SELECT TitleNumber FROM @TitlesToUpdate)
										AND HasVOARates = '1');

	RETURN 0;	
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_LastSaleDate]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 16th June 2021
 Description:	Updates the [LastSaleDate] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		OWN = Ownership

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_LastSaleDate]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'OWN' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'OWN'
		BEGIN
			/* !!!!!!!!!! NEEDS REFINING TO SELECT OWNERSHIP UDATED TITLENUMBERS !!!!!!!!!!!!!*/
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		LastSaleDate = DataScience.dbo.ufnGetLastSaleDate(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_LastSalePrice]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 16th June 2021
 Description:	Updates the [LastSalePrice] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		OWN = Ownership

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields updated.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_LastSalePrice]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'OWN' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'OWN'
		BEGIN
			/* !!!!!!!!!! NEEDS REFINING TO SELECT OWNERSHIP UDATED TITLENUMBERS !!!!!!!!!!!!!*/
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		LastSalePrice = DataScience.dbo.ufnGetLastSalePrice(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_NumberOfPolys]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [NumberOfPolys] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update, the SP will update all Titles affected by the last NPS Update RUN.
						The list of titles changed during the last NPSUpdate run are in the following table:
						[NPSUpdate].[dbo].[temp_AllChangedTitles]
		0 = Full update, all titles in [NationalPolygon].[dbo].[TitleInfo] will be updated.

 OUTPUT PARAMETERS:
	@FieldsProcessed  - returns the number of fields updated.

 RETURN VALUES:
	0 = success
	1 = fail

 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_NumberOfPolys]
	@IsCOU BIT,
	@FieldsProcessed  INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));
	IF @IsCOU = '1'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
	END
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END;

	/*Perform the update. */
	WITH PolyCount
	AS
	(
		SELECT TitleNumber, COUNT(TitleNumber) as cnt
		FROM NationalPolygon.dbo.Polygon
		WHERE TitleNumber IN
		(SELECT TitleNumber FROM @TitlesToUpdate)
		GROUP BY TitleNumber
	)
	UPDATE ti
	SET NumberOfPolys = pol.cnt
	FROM NationalPolygon.dbo.TitleInfo ti
	INNER JOIN PolyCount pol ON pol.TitleNumber = ti.TitleNumber
	WHERE ti.TitleNumber IN
	(SELECT TitleNumber FROM NPSUpdate.dbo.analysis_AllChangedTitles);
	SET @FieldsProcessed  = @@ROWCOUNT;

	RETURN 0;
		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_Postcode]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 17th June 2021
 Description:	Updates the [Postcode] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		COD = Codepoint

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_Postcode]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'COD' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'COD'
		BEGIN
			/* !!!!!!!!!! NEEDS REFINING TO SELECT OWNERSHIP UDATED TITLENUMBERS !!!!!!!!!!!!!*/
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		Postcode = DataScience.dbo.ufnGetTitlePostcode(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END




GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_PricePaidStreet]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 17th June 2021
 Description:	Updates the [PricePaidStreet] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		OWN = Ownership

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_PricePaidStreet]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR ( @UpdateContext <> 'OWN' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'OWN'
		BEGIN
			/* !!!!!!!!!! NEEDS REFINING TO SELECT OWNERSHIP UDATED TITLENUMBERS !!!!!!!!!!!!!*/
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		PricePaidStreet = DataScience.dbo.ufnGetPPStreet(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END




GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_ResiCount]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [ResiCount] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = fail
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_ResiCount]
	@IsCOU BIT,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));
	IF @IsCOU = '1'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;	
	END
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM TitleInfo;
	END

	/*Perform the update. */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
	ResiCount = DataScience.dbo.ufnGetResiCount(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_Top1Owner]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 16th June 2021
 Description:	Updates the [Top1Owner] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		OWN = Ownership

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_Top1Owner]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'OWN' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'OWN'
		BEGIN
			/* !!!!!!!!!! NEEDS REFINING TO SELECT OWNERSHIP UDATED TITLENUMBERS !!!!!!!!!!!!!*/
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		Top1Owner = DataScience.dbo.ufnGetTop1Owner(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_TotalBuildingFootprint]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 21th June 2021
 Description:	Updates the [TotalBuildingFootprint] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the SP what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		BUI = Building database.

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_TotalBuildingFootprint]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext <> 'COD' AND @UpdateContext <> 'NPS'
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a Building DB update. */
		IF @UpdateContext = 'BUI'
		BEGIN
			/* !!!!!!!!!! NEEDS REFINING TO SELECT BUILDING UDATED TITLENUMBERS !!!!!!!!!!!!!*/
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END

	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		TotalBuildingFootprint = DataScience.dbo.ufnGetBFoot(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END




GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_UPRNCount]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [UPRNCount] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update

 OUTPUT PARAMETERS:
	@FieldsProcessed  = number of rows processed

 RETURN VALUES:
	0 = success
	1 = fail

 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_UPRNCount]
	@IsCOU BIT,
	@FieldsProcessed  INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));
	IF @IsCOU = '1'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
	END
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END;

	/* Populate [UPRNCount] (count of all UPRNs in [TitleNumberUPRN] with a DeliveryPoint address) */
	WITH UPRNCounter
	AS
	(	SELECT [TitleNumber], COUNT([UPRN]) as uprnCount
		FROM [NationalPolygon].[dbo].[TitleNumberUPRN] tnu
		WHERE tnu.[DeliveryPoint] = 1
		AND TitleNumber IN
		(SELECT TitleNumber FROM @TitlesToUpdate)
		GROUP BY tnu.[TitleNumber]
	)
	UPDATE ti
	SET [UPRNCount] = uc.uprnCount
	FROM [NationalPolygon].[dbo].[TitleInfo] ti
	INNER JOIN UPRNCounter uc ON uc.TitleNumber = ti.TitleNumber;
	SET @FieldsProcessed  = @@ROWCOUNT;

	UPDATE [NationalPolygon].[dbo].[TitleInfo] SET [UPRNCount] = 0 WHERE [UPRNCount] IS NULL

	RETURN 0;	
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_VOAPropDescription]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 15th June 2021
 Description:	Updates the [VOAPropDescription] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the stored procedure what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		VOA = VOAUpdate (VOA)

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_VOAPropDescription]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'VOA' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'VOA'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM VOAUpdate.dbo.AffectedTitleNumbersFromLastUpdate;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END


	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		VOAPropDescription = DataScience.dbo.ufnGetVOAUses(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[spTIUpdate_VOATotalValue]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		Ben Blackham
 Create date: 17th June 2021
 Description:	Updates the [VOATotalValue] field of [NationalPolygon].[dbo].[TitleInfo].

 INPUT PARAMETERS:
	@IsCOU BIT 
		1 = COU update
		0 = Full update
	@UpdateContext - Informs the stored procedure what update system has called it.
	                 This allows it to descide where to source the COU affected titlenumbers from.
		NPS = NPSUpdate (National Polygon)
		VOA = VOAUpdate (VOA)

 OUTPUT PARAMETERS:
	@FieldsProcessed - returns the number of fields processed.

 RETURN VALUES:
	0 = success
	1 = The FULL/COU selection flag was not set.
	2 = An invalid update context was given.
 ============================================= */
CREATE PROCEDURE [dbo].[spTIUpdate_VOATotalValue]
	@IsCOU BIT,
	@UpdateContext CHAR(3) = NULL,
	@FieldsProcessed INT = 0 OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF @IsCOU IS NULL
	BEGIN
		RETURN 1; /* No update type has been selected. */
	END

    --Gather titles to be updated.
	DECLARE @TitlesToUpdate TABLE (TitleNumber VARCHAR(36));

	/*----------------- COU Update ---------------------------*/
	IF @IsCOU = '1'
	BEGIN
		IF @UpdateContext IS NULL OR (@UpdateContext <> 'VOA' AND @UpdateContext <> 'NPS')
		BEGIN 
			RETURN 2; /* An invalid update context was given. */
		END

		/* Select the TitleNumbers to be updated based on a VOA update. */
		IF @UpdateContext = 'VOA'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM VOAUpdate.dbo.AffectedTitleNumbersFromLastUpdate;
		END
	    /* Select the TitleNumbers to be updated based on a NPS update. */
		IF @UpdateContext = 'NPS'
		BEGIN
			INSERT INTO @TitlesToUpdate
			SELECT DISTINCT TitleNumber FROM NPSUpdate.dbo.temp_AllChangedTitles;
		END
	END

	/*----------------- Full UPDATE ---------------------------*/
	IF @IsCOU = '0'
	BEGIN
		INSERT INTO @TitlesToUpdate
		SELECT DISTINCT TitleNumber FROM NationalPolygon.dbo.TitleInfo;
	END


	/*---------------- Perform the update ---------------------- */
	UPDATE NationalPolygon.dbo.TitleInfo 
	SET 
		VOATotalValue = DataScience.dbo.ufnGetRatVal(TitleNumber),
		LastUpdated = GETDATE()
	WHERE TitleNumber IN (SELECT TitleNumber FROM @TitlesToUpdate);
	SET @FieldsProcessed = @@ROWCOUNT;

	RETURN 0;		
END
GO
/****** Object:  StoredProcedure [dbo].[UnmatchedUprnImport]    Script Date: 25/06/2021 16:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UnmatchedUprnImport]
    (
        @ImportTable dbo.UnmatchedUprnImport READONLY
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SET ANSI_NULLS OFF;

        DECLARE @Output TABLE
            (
                mergeAction NVARCHAR(10)
            );

        MERGE UnmatchedUPRN AS t
        USING ( SELECT * FROM @ImportTable ) AS s
        ON s.uprn = t.UPRN
        WHEN NOT MATCHED THEN INSERT ( UPRN )
                              VALUES ( s.uprn )
        OUTPUT $action
        INTO @Output;

        SELECT mergeAction, COUNT(*) AS itemCount FROM @Output GROUP BY mergeAction;

        SET ANSI_NULLS ON;

        SET NOCOUNT OFF;
    END;
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Polygon"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 311
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'PolygonByTitleNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'PolygonByTitleNumber'
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [SpatialIndexLatLng]    Script Date: 25/06/2021 16:43:26 ******/
CREATE SPATIAL INDEX [SpatialIndexLatLng] ON [dbo].[Polygon]
(
	[LatLng]
)USING  GEOGRAPHY_AUTO_GRID 
WITH (
CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [SPX_POLYGONGEOGRAPHY]    Script Date: 25/06/2021 16:43:26 ******/
CREATE SPATIAL INDEX [SPX_POLYGONGEOGRAPHY] ON [dbo].[Polygon]
(
	[PolygonGeography]
)USING  GEOGRAPHY_AUTO_GRID 
WITH (
CELLS_PER_OBJECT = 12, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [IX_UNREGISTEREDLAND_LOCATION]    Script Date: 25/06/2021 16:43:26 ******/
CREATE SPATIAL INDEX [IX_UNREGISTEREDLAND_LOCATION] ON [dbo].[UnregisteredLand]
(
	[LOCATION]
)USING  GEOGRAPHY_GRID 
WITH (GRIDS =(LEVEL_1 = HIGH,LEVEL_2 = HIGH,LEVEL_3 = HIGH,LEVEL_4 = HIGH), 
CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [NationalPolygon] SET  READ_WRITE 
GO
