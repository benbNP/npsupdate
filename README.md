# Introduction 
This repo contains the .NET code that controls the update of the Nimbus National Polygon data set from the HM National Polygon Service monbthly updates.
For more information, see the documentation here:
https://assuredpropertygroup1.sharepoint.com/:w:/r/sites/EngineeringTeam/_layouts/15/Doc.aspx?sourcedoc=%7BD0700800-ECB2-4F0E-BA7E-9ED4174446D8%7D&file=NPSUpdate_Documentation.docx&action=default&mobileredirect=true
