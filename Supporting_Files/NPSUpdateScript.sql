USE [master]
GO
/****** Object:  Database [NPSUpdate]    Script Date: 18/03/2021 12:07:09 ******/
CREATE DATABASE [NPSUpdate]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NPSUpdate', FILENAME = N'E:\Data\NPSUpdate_Primary.mdf' , SIZE = 328455232KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'NPSUpdate_log', FILENAME = N'E:\Data\NPSUpdate_Primary.ldf' , SIZE = 177158016KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [NPSUpdate] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [NPSUpdate].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [NPSUpdate] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [NPSUpdate] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [NPSUpdate] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [NPSUpdate] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [NPSUpdate] SET ARITHABORT OFF 
GO
ALTER DATABASE [NPSUpdate] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [NPSUpdate] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [NPSUpdate] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [NPSUpdate] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [NPSUpdate] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [NPSUpdate] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [NPSUpdate] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [NPSUpdate] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [NPSUpdate] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [NPSUpdate] SET  DISABLE_BROKER 
GO
ALTER DATABASE [NPSUpdate] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [NPSUpdate] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [NPSUpdate] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [NPSUpdate] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [NPSUpdate] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [NPSUpdate] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [NPSUpdate] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [NPSUpdate] SET RECOVERY FULL 
GO
ALTER DATABASE [NPSUpdate] SET  MULTI_USER 
GO
ALTER DATABASE [NPSUpdate] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [NPSUpdate] SET DB_CHAINING OFF 
GO
ALTER DATABASE [NPSUpdate] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [NPSUpdate] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [NPSUpdate] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [NPSUpdate] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'NPSUpdate', N'ON'
GO
ALTER DATABASE [NPSUpdate] SET QUERY_STORE = OFF
GO
USE [NPSUpdate]
GO
/****** Object:  User [NPSUpdate]    Script Date: 18/03/2021 12:07:09 ******/
CREATE USER [NPSUpdate] FOR LOGIN [NPSUpdate] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [NPSImporter]    Script Date: 18/03/2021 12:07:09 ******/
CREATE USER [NPSImporter] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [NPSUpdate]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [NPSUpdate]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [NPSUpdate]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [NPSUpdate]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [NPSUpdate]
GO
ALTER ROLE [db_datareader] ADD MEMBER [NPSUpdate]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [NPSUpdate]
GO
ALTER ROLE [db_owner] ADD MEMBER [NPSImporter]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [NPSImporter]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [NPSImporter]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [NPSImporter]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [NPSImporter]
GO
ALTER ROLE [db_datareader] ADD MEMBER [NPSImporter]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [NPSImporter]
GO
/****** Object:  UserDefinedTableType [dbo].[PolygonTableType]    Script Date: 18/03/2021 12:07:09 ******/
CREATE TYPE [dbo].[PolygonTableType] AS TABLE(
	[InspireID] [bigint] NOT NULL,
	[InspireIDPart] [int] NULL DEFAULT ((1)),
	[TitleNumber] [varchar](20) NULL,
	[INSERT] [datetime2](7) NULL,
	[UPDATE] [datetime2](7) NULL,
	[REC_STATUS] [char](1) NULL,
	[SingleEasting] [float] NULL,
	[SingleNorthing] [float] NULL,
	[Area] [float] NULL,
	[Long] [float] NULL,
	[Lat] [float] NULL,
	[WKT4326] [nvarchar](max) NULL,
	[PolygonGeography] [nvarchar](max) NULL,
	[LatLng] [nvarchar](max) NULL,
	[WKT27700] [nvarchar](max) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TenureTableType]    Script Date: 18/03/2021 12:07:10 ******/
CREATE TYPE [dbo].[TenureTableType] AS TABLE(
	[TITLE_NO] [varchar](20) NOT NULL,
	[ESTATE_INTRST_CODE] [varchar](2) NULL,
	[CLASS_TITLE_CODE] [varchar](2) NULL,
	[PEND_NT_CODE] [varchar](8) NULL,
	[REC_STATUS] [char](1) NULL
)
GO
/****** Object:  Table [dbo].[analysis_AllChangedTitles]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[analysis_AllChangedTitles](
	[TitleNumber] [varchar](20) NOT NULL,
	[timestamp] [datetime2](7) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[analysis_CorrectedInvalidPolygons]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[analysis_CorrectedInvalidPolygons](
	[InspireID] [bigint] NULL,
	[invalid_geo] [varchar](max) NULL,
	[valid_geo] [varchar](max) NULL,
	[invalid_centroid] [varchar](max) NULL,
	[envelope_centre] [varchar](max) NULL,
	[invalid_area] [float] NULL,
	[valid_area] [float] NULL,
	[area_diff] [float] NULL,
	[percentage_diff] [float] NULL,
	[timestamp] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[analysis_ExplodedPolygons]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[analysis_ExplodedPolygons](
	[isOriginal] [bit] NULL,
	[InspireID] [bigint] NOT NULL,
	[InspireIDPart] [int] NULL,
	[TitleNumber] [varchar](20) NULL,
	[INSERT] [datetime] NULL,
	[UPDATE] [datetime] NULL,
	[REC_STATUS] [char](1) NULL,
	[SingleEasting] [float] NULL,
	[SingleNorthing] [float] NULL,
	[Area] [float] NULL,
	[Long] [float] NULL,
	[Lat] [float] NULL,
	[WKT4326] [varchar](max) NULL,
	[PolygonGeography] [varchar](max) NULL,
	[LatLng] [varchar](max) NULL,
	[WKT27700] [varchar](max) NULL,
	[timestamp] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[analysis_IncomingInspireIDs]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[analysis_IncomingInspireIDs](
	[InspireID] [int] NULL,
	[timestamp] [datetime2](7) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[analysis_NewPolygonData_BadPolygons]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[analysis_NewPolygonData_BadPolygons](
	[id] [int] NULL,
	[InspireID] [bigint] NOT NULL,
	[InspireIDPart] [int] NULL,
	[TitleNumber] [varchar](20) NULL,
	[INSERT] [datetime2](7) NULL,
	[UPDATE] [datetime2](7) NULL,
	[REC_STATUS] [char](1) NULL,
	[SingleEasting] [float] NULL,
	[SingleNorthing] [float] NULL,
	[Area] [float] NULL,
	[Long] [float] NULL,
	[Lat] [float] NULL,
	[WKT4326] [nvarchar](max) NULL,
	[PolygonGeography] [nvarchar](max) NULL,
	[LatLng] [nvarchar](max) NULL,
	[WKT27700] [nvarchar](max) NULL,
	[timestamp] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailRecipients]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailRecipients](
	[RecipientID] [int] IDENTITY(1,1) NOT NULL,
	[RecipientName] [nvarchar](50) NULL,
	[RecipientEmail] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FreeholdToLeasehold]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FreeholdToLeasehold](
	[Freehold] [nvarchar](100) NULL,
	[Leasehold] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewPolygonData]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewPolygonData](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[InspireID] [bigint] NOT NULL,
	[InspireIDPart] [int] NULL,
	[TitleNumber] [varchar](20) NULL,
	[INSERT] [datetime2](7) NULL,
	[UPDATE] [datetime2](7) NULL,
	[REC_STATUS] [char](1) NULL,
	[SingleEasting] [float] NULL,
	[SingleNorthing] [float] NULL,
	[Area] [float] NULL,
	[Long] [float] NULL,
	[Lat] [float] NULL,
	[WKT4326] [nvarchar](max) NULL,
	[PolygonGeography] [nvarchar](max) NULL,
	[LatLng] [nvarchar](max) NULL,
	[WKT27700] [nvarchar](max) NULL,
 CONSTRAINT [PK_NewPolygonData] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewTenureData]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewTenureData](
	[TITLE_NO] [varchar](20) NULL,
	[ESTATE_INTRST_CODE] [varchar](10) NULL,
	[CLASS_TITLE_CODE] [varchar](10) NULL,
	[PEND_NT_CODE] [varchar](10) NULL,
	[REC_STATUS] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewUPRNData]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewUPRNData](
	[TitleNumber] [varchar](20) NULL,
	[UPRN] [bigint] NULL,
	[Status] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Polygon]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Polygon](
	[InspireID] [bigint] NOT NULL,
	[InspireIDPart] [int] NOT NULL,
	[TitleNumber] [varchar](20) NULL,
	[INSERT] [datetime2](7) NULL,
	[UPDATE] [datetime2](7) NULL,
	[REC_STATUS] [char](1) NULL,
	[Freehold] [bit] NULL,
	[Estate] [varchar](2) NULL,
	[Class] [varchar](2) NULL,
	[Status] [varchar](50) NULL,
	[Change] [varchar](1) NULL,
	[Tenure] [char](1) NULL,
	[SingleEasting] [float] NULL,
	[SingleNorthing] [float] NULL,
	[Area] [float] NULL,
	[Long] [float] NULL,
	[Lat] [float] NULL,
	[WKT4326] [varchar](max) NULL,
	[PolygonGeography] [geography] NULL,
	[LatLng] [geography] NULL,
	[WKT27700] [varchar](max) NULL,
 CONSTRAINT [PK__Polygon__FE3DC31F6D24AB84] PRIMARY KEY CLUSTERED 
(
	[InspireID] ASC,
	[InspireIDPart] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StatsForLatestRun]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StatsForLatestRun](
	[StatID] [int] NOT NULL,
	[StatDescription] [nvarchar](250) NULL,
	[Value] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StatsHistorical]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StatsHistorical](
	[StartTime] [datetime2](7) NULL,
	[UpdateType] [char](4) NULL,
	[StatID] [int] NULL,
	[StatDescription] [nvarchar](250) NULL,
	[Value] [int] NULL,
	[EndTime] [datetime2](7) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[temp_CorrectedInvalidPolygons]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_CorrectedInvalidPolygons](
	[InspireID] [bigint] NULL,
	[invalid_geo] [varchar](max) NULL,
	[valid_geo] [varchar](max) NULL,
	[invalid_centroid] [varchar](max) NULL,
	[envelope_centre] [varchar](max) NULL,
	[invalid_area] [float] NULL,
	[valid_area] [float] NULL,
	[area_diff] [float] NULL,
	[percentage_diff] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TitleChangeType]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleChangeType](
	[ChangeTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ChangeTypeDescription] [varchar](30) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TitleHistory]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleHistory](
	[ChangeID] [int] IDENTITY(1,1) NOT NULL,
	[ChangeDate] [datetime2](7) NOT NULL,
	[TitleNumber] [varchar](20) NOT NULL,
	[LRRecStatus] [char](1) NULL,
	[NimbusChangeCode] [char](1) NULL,
	[TitleChangeType] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TitleInfo]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleInfo](
	[TitleNumber] [nvarchar](18) NOT NULL,
	[Estate] [nvarchar](4) NULL,
	[Class] [nvarchar](4) NULL,
	[Status] [nvarchar](100) NULL,
	[RecStatus] [char](1) NULL,
	[Tenure] [char](1) NULL,
	[HasIndustrial] [bit] NULL,
	[TotalArea] [float] NULL,
	[TotalBuildingFootprint] [float] NULL,
	[UPRNCount] [int] NULL,
	[HasLL] [bit] NULL,
	[HasOffices] [bit] NULL,
	[HasRetail] [bit] NULL,
	[HasResi] [bit] NULL,
	[EPC] [char](1) NULL,
	[LastSaleDate] [datetime] NULL,
	[LastSalePrice] [float] NULL,
	[Acreage] [float] NULL,
	[BuildingType] [char](1) NULL,
	[Postcode] [nvarchar](24) NULL,
	[DevPerc] [float] NULL,
	[GreenBeltOverlap] [float] NULL,
	[AONBOverlap] [float] NULL,
	[HasListed] [bit] NULL,
	[HasVOARates] [bit] NULL,
	[HasCouncilTax] [bit] NULL,
	[CommercialFootprint] [float] NULL,
	[PricePaidStreet] [nvarchar](400) NULL,
	[Top1Owner] [nvarchar](1000) NULL,
	[OwnershipStreet] [nvarchar](400) NULL,
	[FZ2Overlap] [float] NULL,
	[FZ3Overlap] [float] NULL,
	[VOAPropDescription] [nvarchar](1000) NULL,
	[VOATotalValue] [float] NULL,
	[FreeholdFlag] [int] NOT NULL,
	[WoodlandOverlap] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[TitleNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TitleNumberUPRN]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleNumberUPRN](
	[TitleNumber] [nvarchar](24) NULL,
	[UPRN] [bigint] NULL,
	[Source] [smallint] NULL,
	[Source_Str] [nvarchar](10) NULL,
	[ID] [uniqueidentifier] NOT NULL,
	[DeliveryPoint] [bit] NULL,
	[Tenure] [char](1) NULL,
 CONSTRAINT [PK_ID] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumberUPRN]    Script Date: 18/03/2021 12:07:10 ******/
CREATE CLUSTERED INDEX [IX_TitleNumberUPRN] ON [dbo].[TitleNumberUPRN]
(
	[TitleNumber] ASC,
	[UPRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TitleNumberUPRN_backup]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TitleNumberUPRN_backup](
	[TitleNumber] [nvarchar](24) NULL,
	[UPRN] [bigint] NULL,
	[Source] [smallint] NULL,
	[Source_Str] [nvarchar](10) NULL,
	[ID] [uniqueidentifier] NOT NULL,
	[DeliveryPoint] [bit] NULL,
	[Tenure] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_TitleNumberUPRN]    Script Date: 18/03/2021 12:07:10 ******/
CREATE CLUSTERED INDEX [IX_TitleNumberUPRN] ON [dbo].[TitleNumberUPRN_backup]
(
	[TitleNumber] ASC,
	[UPRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UpdateResults]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UpdateResults](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[timeof] [datetime] NULL,
	[description] [varchar](max) NULL,
 CONSTRAINT [PK__UpdateRe__3214EC270D2EFA10] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [idx_npd]    Script Date: 18/03/2021 12:07:10 ******/
CREATE NONCLUSTERED INDEX [idx_npd] ON [dbo].[NewPolygonData]
(
	[InspireID] ASC,
	[TitleNumber] ASC,
	[REC_STATUS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [idx_ntd]    Script Date: 18/03/2021 12:07:10 ******/
CREATE NONCLUSTERED INDEX [idx_ntd] ON [dbo].[NewTenureData]
(
	[TITLE_NO] ASC,
	[REC_STATUS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_PolyTenure]    Script Date: 18/03/2021 12:07:10 ******/
CREATE NONCLUSTERED INDEX [IX_PolyTenure] ON [dbo].[Polygon]
(
	[Tenure] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_TitleNumberUPRN_UPRN]    Script Date: 18/03/2021 12:07:10 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumberUPRN_UPRN] ON [dbo].[TitleNumberUPRN]
(
	[UPRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_TitleNumberUPRN_UPRN]    Script Date: 18/03/2021 12:07:10 ******/
CREATE NONCLUSTERED INDEX [IX_TitleNumberUPRN_UPRN] ON [dbo].[TitleNumberUPRN_backup]
(
	[UPRN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20210210-160334]    Script Date: 18/03/2021 12:07:10 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20210210-160334] ON [dbo].[UpdateResults]
(
	[timeof] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SpatiallyPopulateTitleNumberUPRN]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpatiallyPopulateTitleNumberUPRN] 
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  

	--INSERT FREEHOLDS--
	WITH ExistingTitleNumberUPRNFreeholds ([UPRN])
	AS (SELECT [UPRN] FROM [TitleNumberUPRN] WHERE [Tenure] = 'F')
	---------------------------
	INSERT INTO TitleNumberUPRN
	SELECT 
		p.[TitleNumber],
		blpu.[UPRN],
		3,
		'Spatial',
		NEWID(),
		CASE 
			WHEN dp.[UPRN] IS NULL THEN 0
			ELSE 1
		END,
		'F'
	FROM 
		[AddressBase].[dbo].[AddressBaseBLPU] blpu 
		INNER JOIN [Polygon] p ON (p.[PolygonGeography].STIntersects(blpu.[LOCATION]) = 1) 
		LEFT JOIN [AddressBase].[dbo].[AddressBaseDeliveryPoint] dp ON dp.[UPRN] = blpu.[UPRN]
	WHERE blpu.[UPRN] NOT IN (SELECT [UPRN] FROM ExistingTitleNumberUPRNFreeholds);


	--INSERT LEASEHOLDS--
	WITH ExistingTitleNumberUPRNLeasholds ([UPRN])
	AS (SELECT [UPRN] FROM [TitleNumberUPRN] WHERE [Tenure] = 'L')
	---------------------------
	INSERT INTO TitleNumberUPRN
	SELECT 
		p.[TitleNumber],
		blpu.[UPRN],
		3,
		'Spatial',
		NEWID(),
		CASE 
			WHEN dp.[UPRN] IS NULL THEN 0
			ELSE 1
		END,
		'L'
	FROM 
		[AddressBase].[dbo].[AddressBaseBLPU] blpu 
		INNER JOIN [Polygon] p ON (p.[PolygonGeography].STIntersects(blpu.[LOCATION]) = 1) 
		LEFT JOIN [AddressBase].[dbo].[AddressBaseDeliveryPoint] dp ON dp.[UPRN] = blpu.[UPRN]
	WHERE blpu.[UPRN] NOT IN (SELECT [UPRN] FROM ExistingTitleNumberUPRNLeasholds);

END
GO
/****** Object:  StoredProcedure [dbo].[spNPSUpdate]    Script Date: 18/03/2021 12:07:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spNPSUpdate]
	@NewPolygonData AS PolygonTableType readonly,
	@ProcessAction AS CHAR(1),
	@IsFullBuild AS BIT = 0,
	@FullFileLocation AS VARCHAR(250) = '',
	@COUFileLocation AS VARCHAR(250) = '',
	@TestFileLocation AS VARCHAR(250) = '',
	@TestMode AS BIT = 0,
	@StartTime AS DATETIME2 = '1900-01-01 00:00:00',
	@TruncateAnalysis AS BIT = 0,
	@Status VARCHAR(MAX) OUTPUT
	
AS
BEGIN
SET NOCOUNT ON

--###################################################################################################################
--################################### Receive a block of shapefile data #############################################
IF @ProcessAction = 'B'	
BEGIN

	BEGIN TRY
		INSERT INTO [NewPolygonData]
		SELECT * FROM @NewPolygonData	
		SET @Status = 'success'
	END TRY
	BEGIN CATCH
		SET @Status = (SELECT ERROR_MESSAGE())

		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR: ' + ERROR_MESSAGE ())
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR NUMBER: ' + CONVERT(VARCHAR(MAX), ERROR_NUMBER()))
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR PROCEDURE: ' + ERROR_PROCEDURE())
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR LINE NUMBER: ' + CONVERT(VARCHAR(MAX), ERROR_LINE()))

	END CATCH
END

--######################################################################################################################
--############################## Start of new update (COU or Full rebuild) #############################################
IF @ProcessAction = 'N' 
BEGIN
	BEGIN TRY
		--Main stat collection variables
		DECLARE @Stat1_TitlesChanged INT = 0
		DECLARE @Stat2_TitlesAdded INT = 0
		DECLARE @Stat3_TitlesDeleted INT = 0
		DECLARE @Stat4_UPRNDeleted INT = 0
		DECLARE @Stat5_UPRNAdded INT = 0
		DECLARE @Stat6_UPRNDP INT = 0
		DECLARE @FileLocation VARCHAR(250) = ''

		TRUNCATE TABLE [NewPolygonData]
		TRUNCATE TABLE [NewTenureData]
		TRUNCATE TABLE [NewUPRNData];

		IF @TruncateAnalysis = 1
		BEGIN
			TRUNCATE TABLE analysis_AllChangedTitles
			TRUNCATE TABLE analysis_CorrectedInvalidPolygons
			TRUNCATE TABLE analysis_ExplodedPolygons
			TRUNCATE TABLE analysis_IncomingInspireIDs
			TRUNCATE TABLE analysis_NewPolygonData_BadPolygons
		END

		IF @IsFullBuild = 1
		BEGIN

			INSERT INTO UpdateResults  ([timeof], [description])
			VALUES (GETDATE(), 'FULL BUILD: Dropping and recreating [Polygon], [TitleInfo] and [FreeholdToLeasehold].');
			--Reset [polygon]
			DROP TABLE IF EXISTS  [Polygon]
			CREATE TABLE [dbo].[Polygon](
				[InspireID] [bigint] NOT NULL,
				[InspireIDPart] [int] NOT NULL,
				[TitleNumber] [varchar](20) NULL,
				[INSERT] [datetime2](7) NULL,
				[UPDATE] [datetime2](7) NULL,
				[REC_STATUS] [char](1) NULL,
				[Freehold] [bit] NULL,
				[Estate] [varchar](2) NULL,
				[Class] [varchar](2) NULL,
				[Status] [varchar](50) NULL,
				[Change] [varchar](1) NULL,
				[Tenure] [char](1) NULL,
				[SingleEasting] [float] NULL,
				[SingleNorthing] [float] NULL,
				[Area] [float] NULL,
				[Long] [float] NULL,
				[Lat] [float] NULL,
				[WKT4326] [varchar](max) NULL,
				[PolygonGeography] [geography] NULL,
				[LatLng] [geography] NULL,
				[WKT27700] [varchar](max) NULL,
				CONSTRAINT [PK__Polygon__FE3DC31F6D24AB84] PRIMARY KEY CLUSTERED 
			(
				[InspireID], [InspireIDPart]  ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
			) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
			CREATE INDEX ix_titlenum ON [Polygon] ([TitleNumber])

			--Reset TitleInfo
			DROP TABLE IF EXISTS [TitleInfo]
			CREATE TABLE [dbo].[TitleInfo](
				[TitleNumber] [nvarchar](18) NOT NULL PRIMARY KEY,
				[Estate] [nvarchar](4) NULL,
				[Class] [nvarchar](4) NULL,
				[Status] [nvarchar](100) NULL,
				[RecStatus] [char](1) NULL,
				[Tenure] [char](1) NULL,
				[HasIndustrial] [bit] NULL,
				[TotalArea] [float] NULL,
				[TotalBuildingFootprint] [float] NULL,
				[UPRNCount] [int] NULL,
				[HasLL] [bit] NULL,
				[HasOffices] [bit] NULL,
				[HasRetail] [bit] NULL,
				[HasResi] [bit] NULL,
				[EPC] [char](1) NULL,
				[LastSaleDate] [datetime] NULL,
				[LastSalePrice] [float] NULL,
				[Acreage] [float] NULL,
				[BuildingType] [char](1) NULL,
				[Postcode] [nvarchar](24) NULL,
				[DevPerc] [float] NULL,
				[GreenBeltOverlap] [float] NULL,
				[AONBOverlap] [float] NULL,
				[HasListed] [bit] NULL,
				[HasVOARates] [bit] NULL,
				[HasCouncilTax] [bit] NULL,
				[CommercialFootprint] [float] NULL,
				[PricePaidStreet] [nvarchar](400) NULL,
				[Top1Owner] [nvarchar](1000) NULL,
				[OwnershipStreet] [nvarchar](400) NULL,
				[FZ2Overlap] [float] NULL,
				[FZ3Overlap] [float] NULL,
				[VOAPropDescription] [nvarchar](1000) NULL,
				[VOATotalValue] [float] NULL,
				[FreeholdFlag] [int] NOT NULL,
				[WoodlandOverlap] [float] NULL
			) ON [PRIMARY]

			--Reset FreeholdToLeasehold
			DROP TABLE IF EXISTS [FreeholdToLeasehold]
			CREATE TABLE [dbo].[FreeholdToLeasehold](
				[Freehold] [nvarchar](100) NULL,
				[Leasehold] [nvarchar](100) NULL
			) ON [PRIMARY]

			--[TitleNumberUPRN] -- Has matches from other sources 
								-- All LR matches are stripped out and replaced regardless of if
								--   this is a full rebuild or COU.



			--Pull in the full file tenure data
			IF @TestMode = 1
			BEGIN
				SET @FileLocation = @TestFileLocation + '\TEST_FULL'
			END
			ELSE
			BEGIN
				SET @FileLocation = @FullFileLocation
			END


			DECLARE @FullTenureInsert VARCHAR(max) = 'BULK INSERT [NewTenureData] FROM ''' 
														+ @FileLocation + '\NewTenureData.csv''' +
														' WITH ( FORMAT=''CSV'', ROWTERMINATOR = ''0x0A'')'

			EXEC (@FullTenureInsert)


		END
		ELSE
		BEGIN

			IF @TestMode = 1
			BEGIN
				SET @FileLocation = @TestFileLocation  + '\TEST_COU'
			END
			ELSE
			BEGIN
				SET @FileLocation = @COUFileLocation
			END
			--Not a full build so pull in the COU tenure data
			DECLARE @COUTenureInsert VARCHAR(max) = 'BULK INSERT [NewTenureData] FROM ''' 
														+ @FileLocation + '\NewTenureData.csv''' +
														' WITH ( FORMAT=''CSV'', ROWTERMINATOR = ''0x0A'')'

			EXEC (@COUTenureInsert) 

		END

		--Reset the stats table.
		UPDATE [StatsForLatestRun] SET [Value] = 0

		--======================== Update TitleInfo table from new NPS Tenure data ===================================
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Starting NPS Tenure update of TitleInfo.');

		INSERT INTO [analysis_AllChangedTitles]
		SELECT [TITLE_NO], GETDATE() FROM [NewTenureData] 
		
		--Setup stats reporting table
		DECLARE @TitleInfo_NewTenure_Update_Stats TABLE (
											[StatsAction] [varchar](10) NULL,
											[insertOrUpdate_title_no] [varchar](20) NULL,
											[delete_title_no] [varchar](20) NULL)

		INSERT INTO @TitleInfo_NewTenure_Update_Stats ([StatsAction], [insertOrUpdate_title_no], [delete_title_no])
		SELECT [StatsAction], insertOrUpdate_title_no, delete_title_no
		FROM
		(
		MERGE [TitleInfo] t USING [NewTenureData] nt
		ON t.[TitleNumber] = nt.[TITLE_NO]
		--Note, match 'C' (change) and 'A' (add) records here as teh Land Registry could
		--  send throug records with as add when we already have them.
		WHEN MATCHED AND nt.[REC_STATUS] = 'C' OR nt.[REC_STATUS] = 'A'
			THEN UPDATE SET
			t.[Estate] = nt.[ESTATE_INTRST_CODE],
			t.[Class] = nt.[CLASS_TITLE_CODE],
			t.[Status] = [PEND_NT_CODE],
			t.[RecStatus] = nt.[REC_STATUS],
			t.[Tenure] = RIGHT(nt.[CLASS_TITLE_CODE],1),
			t.[FreeholdFlag] = 	
					CASE 
						WHEN RIGHT(nt.[CLASS_TITLE_CODE],1) = 'F' THEN 1
						ELSE 0 
					END
		WHEN MATCHED AND nt.[REC_STATUS] = 'D' THEN
			DELETE
		WHEN NOT MATCHED BY TARGET AND (nt.[REC_STATUS] = 'A' or  nt.[REC_STATUS] = 'C') THEN -- Add any 'C' marked records not already in the data.
			INSERT ([TitleNumber], 
					[Estate], 
					[Class], 
					[Status], 
					[RecStatus], 
					[Tenure],
					[FreeholdFlag])
			VALUES ( [nt].[TITLE_NO],
						[nt].[ESTATE_INTRST_CODE], 
						[nt].[CLASS_TITLE_CODE], 
						[PEND_NT_CODE], 
						[nt].[REC_STATUS],
						RIGHT(nt.[CLASS_TITLE_CODE],1),
						CASE 
						WHEN RIGHT(nt.[CLASS_TITLE_CODE],1) = 'F' THEN 1
						ELSE 0 
					END)
		OUTPUT $action as [StatsAction], 
				INSERTED.[Titlenumber] AS insertOrUpdate_title_no,  
				DELETED.[TitleNumber] AS delete_title_no) MergeOutput

		--Update the titleinfo history table
		INSERT INTO [TitleHistory] ([ChangeDate],[TitleNumber], [LRRecStatus], [NimbusChangeCode], [TitleChangeType])
		SELECT
			GetDate(), 
			insertOrUpdate_title_no,
			nt.[REC_STATUS],
			'A', 
			1
		FROM @TitleInfo_NewTenure_Update_Stats st
		INNER JOIN [NewTenureData] nt ON nt.[TITLE_NO] = st.insertOrUpdate_title_no
		WHERE [StatsAction] = 'INSERT'


		INSERT INTO [TitleHistory] ([ChangeDate],[TitleNumber], [LRRecStatus], [NimbusChangeCode], [TitleChangeType])
		SELECT
			GetDate(), 
			delete_title_no,
			nt.[REC_STATUS],
			'D',  
			2
		FROM @TitleInfo_NewTenure_Update_Stats st
		INNER JOIN [NewTenureData] nt ON nt.[TITLE_NO] = st.delete_title_no
		WHERE [StatsAction] = 'DELETE'


		INSERT INTO [TitleHistory] ([ChangeDate],[TitleNumber], [LRRecStatus], [NimbusChangeCode], [TitleChangeType])
		SELECT
			GetDate(), 
			insertOrUpdate_title_no,
			nt.[REC_STATUS],
			'C',  
			3
		FROM @TitleInfo_NewTenure_Update_Stats st
		INNER JOIN [NewTenureData] nt ON nt.[TITLE_NO] = st.insertOrUpdate_title_no
		WHERE [StatsAction] = 'UPDATE'


		--Write processing stats
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),
					CONVERT(varchar(10), (SELECT COUNT(*) FROM @TitleInfo_NewTenure_Update_Stats WHERE [StatsAction] = 'DELETE'))
				+ ' [TitleInfo] info deletions')
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),
				CONVERT(varchar(10), (SELECT COUNT(*) FROM @TitleInfo_NewTenure_Update_Stats WHERE [StatsAction] = 'UPDATE'))
				+ ' [TitleInfo] changes')
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),
				CONVERT(varchar(10), (SELECT COUNT(*) FROM @TitleInfo_NewTenure_Update_Stats WHERE [StatsAction] = 'INSERT'))
				+ ' [TitleInfo] additions')

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Finished updating [TitleInfo] from [NewTenureData].');

		SET @Stat3_TitlesDeleted = (SELECT COUNT(*) FROM @TitleInfo_NewTenure_Update_Stats WHERE [StatsAction] = 'DELETE')
		SET @Stat1_TitlesChanged = (SELECT COUNT(*) FROM @TitleInfo_NewTenure_Update_Stats WHERE [StatsAction] = 'UPDATE')
		SET @Stat2_TitlesAdded = (SELECT COUNT(*) FROM @TitleInfo_NewTenure_Update_Stats WHERE [StatsAction] = 'INSERT')


		--============================== Update [TitleNumberUPRN] =================================
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Getting new TitleNumberUPRN data.') 
		
		--The UPRN file currently is only available in the full file,


		--Pull in the full file tenure data
		IF @TestMode = 1
		BEGIN
			SET @FileLocation = @TestFileLocation + '\TEST_FULL'
		END
		ELSE
		BEGIN
			SET @FileLocation = @FullFileLocation
		END
		DECLARE @FullUPRNInsert VARCHAR(max) = 'BULK INSERT [NewUPRNData] FROM '''
													+ @FileLocation + '\NewUPRNData.csv''' +
													' WITH ( FORMAT=''CSV'', ROWTERMINATOR = ''0x0A'')'
			
		EXEC (@FullUPRNInsert)

	
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Updating [TitleNumberUPRN].')

		DELETE FROM [TitleNumberUPRN]  WHERE [Source] = 1
		DECLARE @tiUPRNRemoved INT = @@ROWCOUNT

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), CONVERT(varchar(10), @tiUPRNRemoved) + ' existing TitleNumber/UPRN records deleted.')

		SET @Stat4_UPRNDeleted = @tiUPRNRemoved

		INSERT INTO [TitleNumberUPRN]  ([TitleNumber], [UPRN], [Source], [Source_Str], [ID])
		SELECT 
			nu.[TitleNumber], 
			nu.[UPRN], 
			1, 
			'LR',
			NEWID()
		FROM  NewUPRNData nu
		DECLARE @tiUPRNAdded INT = @@ROWCOUNT

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), CONVERT(varchar(10), @tiUPRNAdded) + ' TitleNumber/UPRN records added from new NPS data.')

		SET @Stat5_UPRNAdded = @tiUPRNAdded

		UPDATE [TitleNumberUPRN]
		SET [DeliveryPoint] = 1
		WHERE [UPRN] IN (SELECT [UPRN] FROM AddressBase.dbo.AddressBaseDeliveryPoint)
		DECLARE @tiUPRNDP INT = @@ROWCOUNT

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), CONVERT(varchar(10), @tiUPRNDP) + ' TitleNumber/UPRN records have a delivery point.')

		SET @Stat6_UPRNDP = @tiUPRNDP

		--=================================== UPRN Delivery Point count ===========================================
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'UPRN Counting.')

		-- Count of all UPRNs in [TitleNumberUPRN] with a DeliveryPoint address 
		UPDATE [TitleInfo]
		SET [UPRNCount] = UPRNCounter.[uprnCount]
		FROM [TitleInfo] ti
		INNER JOIN 
		(	SELECT [TitleNumber], COUNT([UPRN]) as uprnCount
			FROM [TitleNumberUPRN] tnu
			WHERE tnu.[DeliveryPoint] = 1
			GROUP BY tnu.[TitleNumber]
		) UPRNCounter on ti.[TitleNumber] = UPRNCounter.[TitleNumber]

		UPDATE [TitleInfo] SET [UPRNCount] = 0 WHERE [UPRNCount] IS NULL

		--================================ Update lowest energy ratings ========================================
		--INSERT INTO UpdateResults  ([timeof], [description])
		--VALUES (GETDATE(), 'Updating lowest energy ratings.')
		--UPDATE [TitleInfo] SET [EPC] = [DataScience].[dbo].[ufnGetLowestEPC]([TitleNumber])
		--INSERT INTO UpdateResults  ([timeof], [description])
		--VALUES (GETDATE(), 'Updating of lowest energy ratings complete.')

		SET @Status = 'success' 
		--Update the stats table.
		UPDATE [StatsForLatestRun] SET [Value] = @Stat1_TitlesChanged WHERE StatID = 1 
		UPDATE [StatsForLatestRun] SET [Value] = @Stat2_TitlesAdded WHERE StatID = 2 
		UPDATE [StatsForLatestRun] SET [Value] = @Stat3_TitlesDeleted WHERE StatID = 3 
		UPDATE [StatsForLatestRun] SET [Value] = @Stat4_UPRNDeleted WHERE StatID = 4 
		UPDATE [StatsForLatestRun] SET [Value] = @Stat5_UPRNAdded WHERE StatID = 5 
		UPDATE [StatsForLatestRun] SET [Value] = @Stat6_UPRNDP WHERE StatID = 6 

	END TRY
	BEGIN CATCH

		SET @Status = 'ERROR - ' + ERROR_MESSAGE();

		--Gather any error stats available.
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR NUMBER: ' + CONVERT(VARCHAR(MAX), ERROR_NUMBER()))
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR PROCEDURE: ' + ERROR_PROCEDURE())
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR LINE NUMBER: ' + CONVERT(VARCHAR(MAX), ERROR_LINE()))
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR MESSAGE: ' + ERROR_MESSAGE())

	END CATCH
END

--######################################################################################################################
--########################################## Shapefile process #########################################################
IF @ProcessAction = 'S'
BEGIN
	BEGIN TRY 
		--Main stat collection variables
		DECLARE @Stat7_PolySkipAge INT = 0
		DECLARE @Stat8_InvlaidPolys INT = 0
		DECLARE @Stat9_MultiPolys INT = 0
		DECLARE @Stat10_NewPolyFronExp INT = 0
		DECLARE @Stat11_PolysChanged INT = 0
		DECLARE @Stat12_PolysAdded INT = 0
		DECLARE @Stat13_PolysDeleted INT = 0
		DECLARE @Stat14_PolysTitlesAreaReCalc INT = 0
		DECLARE @Stat15_PolysBadGeog INT = 0
		DECLARE @Stat17_DupesInSF INT = 0
		DECLARE @Stat18_PolygonsIn INT = 0

		INSERT INTO UpdateResults  ([timeof], [description])	VALUES (GETDATE(), 'Shapefile SQL processing starting.')
		
		SET @Stat18_PolygonsIn = (SELECT COUNT(*) FROM [NewPolygonData])
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),
				CONVERT(varchar(10), @Stat18_PolygonsIn) + ' polygons incoming.')

		--Capture a list of all incoming InspireIDs (for analysis and tracability).
		BEGIN TRY
			INSERT INTO [analysis_IncomingInspireIDs] ([InspireID], [timestamp])
			SELECT [InspireID], GETDATE() FROM [NewPolygonData] 
		END TRY
		BEGIN CATCH
			INSERT INTO UpdateResults  ([timeof], [description])
			VALUES (GETDATE(), 'Error inserting inspire IDs into [anlysis_IncomingInspireIDs]: ' +  ERROR_MESSAGE())
		END CATCH

		--Remove duplicate InspireIDs keeping the one with the latest update date.
		DECLARE @Dupes TABLE ([dupeid] INT)
		INSERT INTO @Dupes
		SELECT [id]
		FROM (
				SELECT [id],
				ROW_NUMBER() OVER 
				(PARTITION BY [InspireID] ORDER BY [UPDATE] DESC) rn
				FROM [NewPolygonData]
			) dupeTable
		WHERE dupeTable.rn <> 1

		DELETE FROM np
		FROM [NewPolygonData] np
		INNER JOIN @Dupes d on d.[dupeid] = np.[id]

		SET @Stat17_DupesInSF = (SELECT COUNT(*) FROM @Dupes)

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), '#### SQL NPS update starting ####');

		--====================== Update the Polygon database from the new Land Registry Polygon data ======================

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Updating [Polygon] from new polygon data.')

		INSERT INTO [analysis_AllChangedTitles]
		SELECT [TitleNumber], GETDATE() FROM [NewPolygonData];
				
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Identifying polygons not in the current Nimbus data but are in the LR data as changes (these will be added).');
		
		--Identify any incoming polygons marked for change that are actually missing from the current Polygon data and are therefore really additions.
		UPDATE [NewPolygonData] 
		SET [REC_STATUS] = 'M' --Missing, 'M' LR say 'C', but we don't have
		WHERE [REC_STATUS] = 'C' 
		AND
		[InspireID] NOT IN (SELECT [InspireID] from [Polygon])

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Identifying polygons that are already in the current Nimbus data but are LR additions (these will be updated).');
		--Identify any incoming polygons marked for change that are actually already present in the current Polygon data and are therefore really updates.
		UPDATE [NewPolygonData] 
		SET [REC_STATUS] = 'P' --P, LR day 'A' add, but we already have
		WHERE [REC_STATUS] = 'A' 
		AND
		[InspireID] IN (SELECT [InspireID] from [Polygon])

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Delete any incoming polygon change records that have an older Land Registry UPDATE time stamp than already present in the database')

		--Delete any incoming updates that have an older Land Registry UPDATE time stamp than already present in the database.
		--Prevents overwriting newer polygons with old data.
		DELETE n FROM [NewPolygonData] n
		INNER JOIN [Polygon] p on p.[InspireID] = n.[InspireID]
		WHERE (p.[UPDATE] > n.[UPDATE]) AND (n.[REC_STATUS] = 'C' OR n.[REC_STATUS] = 'P')

		DECLARE @polysSkippedOnAge INT = @@ROWCOUNT

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),
				CONVERT(varchar(10), @polysSkippedOnAge) + ' polygon update(s) skipped because the data was older than current Nimbus data.')

		SET @Stat7_PolySkipAge = @polysSkippedOnAge

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Building a list of titles that will have their area re-calculated.');
		--Update the table variable used to hold a list of all titles that have had polygon changes and will require an area re-calculation.
		--Get the sum total areas from the full Polygon table for all titles for which a polygon has changed. 
		DECLARE @NewTotalTitleAreas TABLE ([TitleNumber] [varchar](20) NULL, [totalArea] [float] NULL)
		INSERT INTO @NewTotalTitleAreas ([TitleNumber])
		SELECT [TitleNumber] FROM [NewPolygonData] 
		WHERE [TitleNumber] NOT IN (SELECT [TitleNumber] FROM @NewTotalTitleAreas)
		
		--Store the number of InspireID polygons that are to be updated (for stats).
		DECLARE @updatedPolygons VARCHAR(10) =  CONVERT(varchar(10), (SELECT COUNT(*) FROM [NewPolygonData] WHERE [REC_STATUS] = 'C' OR [REC_STATUS] = 'P'))

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Removing polygons marked as updates, these will be processed separately.');
		--Remove any polygons in the existing Polygon table that are updates.
		--This includes 'C' marked polys and 'P' marked polys (poly is flagged as new addition 'A' be we already have) 
		--Changed polygons will be fully removed before being re-inserted.
		--This is to ensure all existing multi-polygon parts (that can be generated by previous polygon MakeValid operations) are fully replaced.
		DELETE FROM [Polygon] WHERE [InspireID] IN
			(SELECT DISTINCT([InspireID]) FROM [NewPolygonData] WHERE [REC_STATUS] = 'C' OR [REC_STATUS] = 'P')

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Beginning invalid polygon detection.');
		TRUNCATE TABLE [temp_CorrectedInvalidPolygons]
		-- Correct any incoming invalid Polygons 
		--Get all polygons (aside from deletions) that SQL thinks are invalid.
		BEGIN TRY
			INSERT INTO [temp_CorrectedInvalidPolygons] 
			(
				[InspireID], 
				[invalid_geo],
				[invalid_centroid],
				[invalid_area]
			)
			SELECT 
				[InspireID], 
				[PolygonGeography],
				[LatLng],
				[Area]
			FROM  [NewPolygonData]
			WHERE geography::STGeomFromText([PolygonGeography], 4326).STIsValid() = 0
			AND [REC_STATUS] <> 'D'

		END TRY
		BEGIN CATCH
			--If the geography WKT looks nothing like a polygon or is so messed up SQL can't work with it then
			--  an error will be thrown. If these are detected - remove them.
			--This cursor process is in a catch block to avoid the expensive cursor operation if it is not needed.
			DECLARE @valid bit
			DECLARE @inspireid varchar(20)
			DECLARE @polygon varchar(max)
			DECLARE bad_poly_check CURSOR FOR
			SELECT [InspireID],[PolygonGeography] FROM [NewPolygonData]	WHERE [REC_STATUS] <> 'D'	
			OPEN bad_poly_check 
			FETCH NEXT FROM bad_poly_check INTO @inspireid, @polygon
			WHILE @@FETCH_STATUS = 0  
			BEGIN 
				BEGIN TRY 
					SET @valid = geography::STGeomFromText(@polygon, 4326).STIsValid()
				END TRY
				BEGIN CATCH
					INSERT INTO [analysis_NewPolygonData_BadPolygons] 
					SELECT  *, GETDATE() FROM [NewPolygonData] WHERE [InspireID] = @inspireid

					DELETE FROM [NewPolygonData] WHERE [InspireID] = @inspireid
					INSERT INTO UpdateResults  ([timeof], [description])
					VALUES (GETDATE(), 'InspireID - ' + CONVERT(varchar(10), @inspireid) + ' has unreadable geography and has been DELETED')

					SET @Stat15_PolysBadGeog = @Stat15_PolysBadGeog + 1

				END CATCH
				FETCH NEXT FROM  bad_poly_check INTO @inspireid, @polygon
			END
			CLOSE bad_poly_check  
			DEALLOCATE bad_poly_check

			INSERT INTO UpdateResults  ([timeof], [description])
			VALUES (GETDATE(), 'Retrying polygon validity checking following bad polygon removal.');
			--Now the bad geography has been removed retry the geography validation check.
			INSERT INTO [temp_CorrectedInvalidPolygons] 
			(
				[InspireID], 
				[invalid_geo],
				[invalid_centroid],
				[invalid_area]
			)
			SELECT 
				[InspireID], 
				[PolygonGeography],
				[LatLng],
				[Area]
			FROM  [NewPolygonData]
			WHERE geography::STGeomFromText([PolygonGeography], 4326).STIsValid() = 0
			AND [REC_STATUS] <> 'D'
		END	CATCH	

		--Make valid the polygons SQL thinks are invalid
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Making invalid polygons valid using SQL MakeValid().');
		UPDATE [temp_CorrectedInvalidPolygons]
		SET [valid_geo] = geography::STGeomFromText([invalid_geo],4326).MakeValid().ToString()

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),
				CONVERT(varchar(10), (SELECT COUNT(*) FROM [temp_CorrectedInvalidPolygons]))
				+ ' invalid polygons corrected by SQL.')

		SET @Stat8_InvlaidPolys = (SELECT COUNT(*) FROM [temp_CorrectedInvalidPolygons])

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Recalculating the centre points of the polygons that were changed during validation.');
		--Recalculate area and centre point of newly valid polys
		UPDATE [temp_CorrectedInvalidPolygons]
		SET
				[valid_area] = geography::STGeomFromText([valid_geo], 4326).STArea(),
				[envelope_centre] = geography::STGeomFromText([valid_geo], 4326).EnvelopeCenter().ToString()
			
		UPDATE [temp_CorrectedInvalidPolygons]
		SET [area_diff] = [valid_area] - [invalid_area]
		UPDATE [temp_CorrectedInvalidPolygons]
		SET [percentage_diff] = [area_diff] / ([invalid_area]/100)

		--Update the new polygon data with the corrected polygon information.
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Updating polygons in [NewPolygonData] that have been made valid.');
		UPDATE [NewPolygonData] 
		SET
			[WKT4326] = cor.[valid_geo],
			[PolygonGeography] = cor.[valid_geo],
			[LatLng] = cor.[envelope_centre],
			[Lat] = geography::STGeomFromText(cor.[envelope_centre], 4326).Lat,
			[Long] = geography::STGeomFromText(cor.[envelope_centre], 4326).Long,
			[Area] = cor.[valid_area]
		FROM [NewPolygonData] np
		INNER JOIN [temp_CorrectedInvalidPolygons] cor
		ON np.[InspireID] = cor.[InspireID]


		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Beginning multi-polygon checking.');
		--Check for multi-polygons in the incoming polygon data (including any multiple polygons caused by making valid).
		DECLARE @inspire VARCHAR(20)
		DECLARE @poly VARCHAR(MAX) 
		DECLARE @TitleNumber VARCHAR(20)
		DECLARE @INSERT DATETIME2
		DECLARE @UPDATE DATETIME2
		DECLARE @REC_STATUS CHAR(1)
		DECLARE @SingleEasting FLOAT
		DECLARE @SingleNorthing FLOAT
		DECLARE @Area float
		DECLARE @Long float
		DECLARE @Lat float
		DECLARE @WKT4326 VARCHAR(MAX)
		DECLARE @LatLng VARCHAR(MAX)
		DECLARE @WKT27700 VARCHAR(MAX)
		DECLARE @poly_geo GEOGRAPHY
		DECLARE @idx INT = 0 
		DECLARE @geoType VARCHAR(200)
		DECLARE @multipolyCount INT = 0
		DECLARE @totalNewPolygons INT = 0
		DECLARE @NewMultiPolyChildsAdded INT = 0

		--Loop all polygons in [NewPolygonData]
		DECLARE multipoly_check_cursor CURSOR FOR
			SELECT [InspireID],[PolygonGeography],[TitleNumber],[INSERT],[UPDATE],[REC_STATUS],[SingleEasting],[SingleNorthing],
					[Area], [Long], [Lat], [WKT4326], [LatLng], [WKT27700]
			FROM [NewPolygonData]
			WHERE [REC_STATUS] <> 'D'
	
		OPEN multipoly_check_cursor  
		FETCH NEXT FROM multipoly_check_cursor 
			INTO @inspire, @poly, @TitleNumber, @INSERT, @UPDATE, @REC_STATUS, @SingleEasting, @SingleNorthing,
					@Area, @Long, @Lat, @WKT4326, @Latlng, @WKT27700	
		WHILE @@FETCH_STATUS = 0  
		BEGIN 
			SET @geoType = geography::STGeomFromText(@poly,4326).STGeometryType()
			IF @geoType = 'MultiPolygon' 
			BEGIN
				-- Insert parent to analysis table.
				INSERT INTO [analysis_ExplodedPolygons] (
					[timestamp],
					[isOriginal],
					[InspireID],
					[InspireIDPart],
					[TitleNumber],
					[INSERT],
					[UPDATE],
					[REC_STATUS],
					[SingleEasting],
					[SingleNorthing],
					[Area],
					[Long],
					[Lat],
					[WKT4326],
					[PolygonGeography],
					[LatLng],
					[WKT27700])
				VALUES ( 
						GETDATE(),
						1,
						@inspire,
						0,
						@TitleNumber, 
						@INSERT, 
						@UPDATE,
						@REC_STATUS, 
						@SingleEasting, 
						@SingleNorthing, 
						@Area,
						@Long,
						@Lat,
						@WKT4326,
						@poly,
						@LatLng,
						@WKT27700)
				
				-- Split the multi polygon
				SET @multipolyCount = @multipolyCount + 1		
				SET @poly_geo = geography::STMPolyFromText(@poly,4326)
				WHILE @idx < @poly_geo.STNumGeometries()  
				BEGIN 	

					SET @idx = @idx + 1  
					--Add the new polygons back to the incoming polygon data
					INSERT INTO [NewPolygonData] (
							[InspireID],
							[InspireIDPart],
							[TitleNumber],
							[INSERT],
							[UPDATE],
							[REC_STATUS],
							[SingleEasting],
							[SingleNorthing],
							[Area],
							[Long],
							[Lat],
							[WKT4326],
							[PolygonGeography],
							[LatLng],
							[WKT27700])
					VALUES (
							@inspire,
							@idx,
							@TitleNumber,
							@INSERT,
							@UPDATE,
							@REC_STATUS,
							@SingleEasting, --Note OSGB data may no be inaccurate
							@SingleNorthing, --Note OSGB data may no be inaccurate
							@poly_geo.STGeometryN(@idx).STArea(), 
							@poly_geo.STGeometryN(@idx).EnvelopeCenter().Long,
							@poly_geo.STGeometryN(@idx).EnvelopeCenter().Lat, 
							@poly_geo.STGeometryN(@idx).ToString(),
							@poly_geo.STGeometryN(@idx).ToString(),
							@poly_geo.STGeometryN(@idx).EnvelopeCenter().ToString(),
							@WKT27700 --Note OSGB data may no be inaccurate
							)			

					--Insert exploded poly(s) into anlysis table
					INSERT INTO [analysis_ExplodedPolygons] (
						[timestamp],
						[isOriginal],
						[InspireID],
						[InspireIDPart],
						[TitleNumber],
						[INSERT],
						[UPDATE],
						[REC_STATUS],
						[SingleEasting],
						[SingleNorthing],
						[Area],
						[Long],
						[Lat],
						[WKT4326],
						[PolygonGeography],
						[LatLng],
						[WKT27700])
					VALUES (
						GETDATE(),
						0,
						@inspire,
						@idx,
						@TitleNumber, 
						@INSERT,    
						@UPDATE,
						@REC_STATUS, 
						@SingleEasting, 
						@SingleNorthing, 
						@poly_geo.STGeometryN(@idx).STArea(), 
						@poly_geo.STGeometryN(@idx).EnvelopeCenter().Long, 
						@poly_geo.STGeometryN(@idx).EnvelopeCenter().Lat, 
						@poly_geo.STGeometryN(@idx).ToString(),
						@poly_geo.STGeometryN(@idx).ToString(),
						@poly_geo.STGeometryN(@idx).EnvelopeCenter().ToString(),
						@WKT27700)
						
						SET @NewMultiPolyChildsAdded = @NewMultiPolyChildsAdded + 1
				END
				SET @idx = 0

				DELETE FROM [NewPolygonData] WHERE [InspireID] = @inspire AND [InspireIDPart] = 0
			END
			IF @geoType <> 'MultiPolygon' AND  @geoType <> 'Polygon' 
			BEGIN
				-- Identify any non-polygons and remove.
				INSERT INTO UpdateResults  ([timeof], [description])
					VALUES (GETDATE(), 'InspireID - ' + CONVERT(varchar(10), @inspire) + ' is not valid it is a: ' + @geoType + '. SKIPPED. Written to the [analysis_NewPolygonData_BadPolygons] table instead.')

				INSERT INTO [analysis_NewPolygonData_BadPolygons] 
				SELECT *, GETDATE() FROM [NewPolygonData] WHERE [InspireID] = @inspire

				DELETE FROM [NewPolygonData] WHERE [InspireID] = @inspire
			END


			FETCH NEXT FROM  multipoly_check_cursor INTO @inspire, @poly, @TitleNumber, @INSERT, @UPDATE, @REC_STATUS, @SingleEasting, @SingleNorthing,
														@Area, @Long, @Lat, @WKT4326, @Latlng, @WKT27700
		END 
		CLOSE multipoly_check_cursor  
		DEALLOCATE multipoly_check_cursor


		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),	CONVERT(varchar(10), @multipolyCount)
				+ ' multi polygons detected by SQL.')

		SET @Stat9_MultiPolys = @multipolyCount

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),	CONVERT(varchar(10), @NewMultiPolyChildsAdded)
				+ ' new polygons created from exploded multi-polygons.')

		SET @Stat10_NewPolyFronExp = @NewMultiPolyChildsAdded

		--Re-insert the changed (REC_STATUS = 'C' and 'P') polygons that were previously deleted thereby completing the update.
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Re-inserting "C" and "P" changed polygons.')

		INSERT INTO [Polygon]
			([InspireID]
			,[InspireIDPart]
			,[TitleNumber]
			,[INSERT]
			,[UPDATE]
			,[REC_STATUS]
			,[Freehold]
			,[Estate]
			,[Class]
			,[Status]
			,[Tenure]
			,[SingleEasting]
			,[SingleNorthing]
			,[Area]
			,[Long]
			,[Lat]
			,[WKT4326]
			,[PolygonGeography]
			,[LatLng]
			,[WKT27700])
		SELECT 
			np.[InspireID]
			,np.[InspireIDPart]
			,np.[TitleNumber]
			,np.[INSERT]
			,np.[UPDATE]
			,np.[REC_STATUS]
			,ti.[Freeholdflag]
			,ti.[Estate]
			,ti.[Class]
			,ti.[Status]
			,ti.[Tenure]
			,[SingleEasting]
			,np.[SingleNorthing]
			,np.[Area]
			,np.[Long]
			,np.[Lat]
			,np.[WKT4326]
			,np.[PolygonGeography]
			,np.[LatLng]
			,np.[WKT27700]
		FROM [NewPolygonData] np
		LEFT JOIN [TitleInfo] ti ON ti.[TitleNumber] = np.[TitleNumber] 
		WHERE np.[REC_STATUS] = 'C' OR np.[REC_STATUS] = 'P'

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Updating [TitleHistory] for polygons that have changed.')

		--Update the title history for the titles of any polygons that are to be changed (recording of additions and deletes is done after the MERGE)
		INSERT INTO [TitleHistory]  ([ChangeDate], [TitleNumber], [LRRecStatus], [NimbusChangeCode], [TitleChangeType] )
		SELECT DISTINCT
			GetDate(),
			np.[TitleNumber], 
			np.[Rec_Status], 
			'C', 
			6
		FROM [NewPolygonData] np WHERE [REC_STATUS] = 'C'

		INSERT INTO [TitleHistory] ([ChangeDate], [TitleNumber], [LRRecStatus], [NimbusChangeCode], [TitleChangeType] )
		SELECT DISTINCT
			GetDate(),
			p.[TitleNumber], 
			'A', 
			'C', 
			6
		FROM [NewPolygonData] p	WHERE [REC_STATUS] = 'P'
	

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'MERGEing new polygon data.')

		--Carry out a MERGE statement to handle the polygon additions and deletes.
  		DECLARE @Polygon_NewPolygon_Update_Stats TABLE (
												[StatsAction] [varchar](10) NULL,
												[insertOrUpdate_title_no] [varchar](20) NULL,
												[delete_title_no] [varchar](20) NULL,
												[insertOrUpdate_inspireID] [bigint] NULL INDEX ixiou,
												[delete_inspireID] [bigint] NULL)

		INSERT INTO @Polygon_NewPolygon_Update_Stats ([StatsAction], [insertOrUpdate_title_no], [delete_title_no], [insertOrUpdate_inspireID], [delete_inspireID])
		SELECT [StatsAction], insOrUpd_tn, deleted_tn, insORUpd_inspireID, deleted_inspireID
		FROM
		(
		MERGE [Polygon] p USING [NewPolygonData] np
		ON p.[InspireID] = np.[InspireID]
		WHEN MATCHED AND np.[REC_STATUS] = 'D'
			THEN DELETE	
		--Handle normal additions and the 'M' records which are records LR marked as 'C' changes but that we do not have.		
		WHEN NOT MATCHED BY TARGET AND (np.[REC_STATUS] = 'A' OR np.[REC_STATUS] = 'M')
			THEN
				INSERT ([InspireID], [InspireIDPart],[TitleNumber], [INSERT], [UPDATE], [Rec_Status], 
						[SingleEasting], [SingleNorthing], [Area], [Long], [Lat], [WKT4326], 
						[PolygonGeography], [LatLng], [WKT27700])		
				VALUES (	np.[InspireID],
							np.[InspireIDPart],
							np.[TitleNumber],
							np.[INSERT],
							np.[UPDATE],
							np.[Rec_Status] ,
							np.[SingleEasting],
							np.[SingleNorthing],
							np.[Area],
							np.[Long],
							np.[Lat],
							np.[WKT4326],
							geography::STGeomFromText (np.[PolygonGeography], 4326),
							geography::STGeomFromText (np.LatLng, 4326),
							np.[WKT27700])
		OUTPUT $action as [StatsAction], 
				INSERTED.[titleNumber] as insOrUpd_tn, 
				DELETED.[titlenumber] as deleted_tn,
				INSERTED.[InspireID] as insOrUpd_inspireID, 
				DELETED.[InspireID] as deleted_inspireID) MergeOutput;

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Updating newly inserted records with tenure data.')

		--Update the newly inserted records with their Tenure information
		UPDATE p
		SET [Freehold] = t.[FreeholdFlag],
			[Estate] = t.[Estate],
			[Class] = t.[Class],
			[Status] = t.[Status],
			[Tenure] = t.[Tenure]	
		FROM [Polygon] p
		INNER JOIN [TitleInfo] t on t.[TitleNumber] = p.[TitleNumber]
		INNER JOIN @Polygon_NewPolygon_Update_Stats us ON us.[insertOrUpdate_inspireID] = p.[InspireID] 

		--Change back the flagged 'M' missing and 'P' present flag
		UPDATE [Polygon] SET [REC_STATUS] = 'A' WHERE [REC_STATUS] = 'P'
		UPDATE [Polygon] SET [REC_STATUS] = 'C' WHERE [REC_STATUS] = 'M'

			
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'Updating [TitleInfo] for deletions and insertions.')

		--Update the titleinfo history table to reflect polygon changes
		INSERT INTO [TitleHistory] ([ChangeDate], [TitleNumber], [LRRecStatus], [NimbusChangeCode], [TitleChangeType])
		SELECT DISTINCT -- Distinct guards against multi poly changes being listed once for every poly part
			GetDate(),
			insertOrUpdate_title_no,
			CASE 
				WHEN np.[REC_STATUS] = 'M' THEN 'C'
				ELSE  np.[REC_STATUS]
			END,
			'A',
			4
		FROM @Polygon_NewPolygon_Update_Stats st
		INNER JOIN [NewPolygonData] np on np.[InspireID] = st.[insertOrUpdate_inspireID] 
		WHERE [StatsAction] = 'INSERT'


		INSERT INTO [TitleHistory] ([ChangeDate], [TitleNumber], [LRRecStatus], [NimbusChangeCode], [TitleChangeType])
		SELECT DISTINCT -- Distinct guards against multi poly changes being listed once for every poly part
			GetDate(),
			delete_title_no,
			np.[REC_STATUS],
			'D',
			5
		FROM @Polygon_NewPolygon_Update_Stats st
		INNER JOIN [NewPolygonData] np on np.[InspireID] = st.[delete_inspireID] 
		WHERE [StatsAction] = 'DELETE' AND [delete_title_no] IS NOT NULL -- This excludes recording deletions of polys that were already not present in our data

		--Write processing stats
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),
					CONVERT(varchar(10), (SELECT COUNT(*) FROM @Polygon_NewPolygon_Update_Stats  WHERE [StatsAction] = 'DELETE'))
				+ ' polygons deleted from [Polygon]')
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),@updatedPolygons + ' polygons changed in [Polygon]')
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(),
				CONVERT(varchar(10), (SELECT COUNT(*) FROM @Polygon_NewPolygon_Update_Stats  WHERE [StatsAction] = 'INSERT'))
				+ ' polygons added to [Polygon]')


		SET @Stat11_PolysChanged = @updatedPolygons
		SET @Stat12_PolysAdded = (SELECT COUNT(*) FROM @Polygon_NewPolygon_Update_Stats  WHERE [StatsAction] = 'INSERT')
		SET @Stat13_PolysDeleted = (SELECT COUNT(*) FROM @Polygon_NewPolygon_Update_Stats  WHERE [StatsAction] = 'DELETE')

		--Store corrected invalid polygons for inspection.
		INSERT INTO [analysis_CorrectedInvalidPolygons]
		SELECT *, GETDATE()  FROM [temp_CorrectedInvalidPolygons]

		--Clear new polygon table ready for next shapefile
		TRUNCATE TABLE [NewPolygonData]

	--======================= Update the total area fields in the TitleInfo table ============================
		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), '>> Calculating polygon areas for any Titles that have had polygon changes.')

		--Calculate the new combined polygon areas for all polygons that have seen change.
		DECLARE @PolygonAreas TABLE (TitleNumber NVARCHAR(20), Area FLOAT)
		INSERT INTO @PolygonAreas
		SELECT [TitleNumber], SUM([Area]) 
		FROM [Polygon] 
		WHERE [TitleNumber] IN (SELECT [TitleNumber] FROM @NewTotalTitleAreas)
		GROUP BY [TitleNumber]

		UPDATE ntta
		SET ntta.[totalArea] = pa.Area
		FROM @NewTotalTitleAreas ntta
		INNER JOIN 	@PolygonAreas pa ON pa.[TitleNumber] = ntta.[TitleNumber]

		DECLARE @areaReCal INT = (SELECT count([TitleNumber]) FROM @NewTotalTitleAreas)

		--Update TitleInfo with the new total areas
		UPDATE [TitleInfo]
		SET [TotalArea] = ntta.[totalArea],
			[Acreage] = ntta.[totalArea] * 0.00024711
		FROM [TitleInfo] ti
		INNER JOIN @NewTotalTitleAreas ntta
		ON ti.[TitleNumber] = ntta.[TitleNumber]
		

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), CONVERT(varchar(10), @areaReCal) + ' titles had their areas recalculated.')

		SET @Stat14_PolysTitlesAreaReCalc = @areaReCal

		INSERT INTO UpdateResults  ([timeof], [description])
		VALUES (GETDATE(), 'SHAPEFILE PROCESSED.')
 
		--Update stats, since this section of the stored procedure runs once for every shapefile the operation is equals plus.
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat7_PolySkipAge WHERE StatID = 7
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat8_InvlaidPolys WHERE StatID = 8 
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat9_MultiPolys WHERE StatID = 9
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat10_NewPolyFronExp WHERE StatID = 10 
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat11_PolysChanged WHERE StatID = 11 
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat12_PolysAdded WHERE StatID = 12 
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat13_PolysDeleted WHERE StatID = 13
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat14_PolysTitlesAreaReCalc WHERE StatID = 14
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat15_PolysBadGeog WHERE StatID = 15
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat17_DupesInSF WHERE StatID = 17
		UPDATE [StatsForLatestRun] SET [Value] = [Value] + @Stat18_PolygonsIn WHERE StatID = 18

		SET @Status = 'success'
	END TRY

	BEGIN CATCH		

		SET @Status = 'ERROR - ' + ERROR_MESSAGE(); 

		--Gather any error stats available.
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR NUMBER: ' + CONVERT(VARCHAR(MAX), ERROR_NUMBER()))
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR PROCEDURE: ' + ERROR_PROCEDURE())
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR LINE NUMBER: ' + CONVERT(VARCHAR(MAX), ERROR_LINE()))
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR MESSAGE: ' + ERROR_MESSAGE())

	END CATCH
END

--######################################################################################################################
--########################################## Post Polygon process operations ###########################################
IF @ProcessAction = 'P'
BEGIN
	--Main stat collection variables
	DECLARE @Stat16_OrphanPolys INT = 0

	BEGIN TRY

		--============================ Update the FreeholdToLeasehold table ===============================
		--PRINT 'Updating the [FreeholdToLeashold] table.'

		--DELETE FROM [TEST_FreeholdToLeasehold]
		--WHERE 
		--[Freehold] IN (SELECT [TitleNumber] FROM  [analysis_AllChangedTitles]) --!!!!!! analysis_AllChangedTitles needs deduping
		--OR 
		--[Leasehold] IN (SELECT [TitleNumber] FROM  [analysis_AllChangedTitles])

		--INSERT INTO UpdateResults ([timeof], [description])
		--VALUES (GETDATE(), CONVERT(varchar(10), @@ROWCOUNT) + ' Freehold to Leasehold records deleted.')

		----Update
		--INSERT INTO [TEST_FreeholdToLeasehold] ([Freehold], [Leasehold])
		--SELECT 
		--	fh_poly.[TitleNumber],
		--	lh_point.[TitleNumber]
		--FROM 
		--[Polygon] fh_poly
		--INNER JOIN 
		--[Polygon] lh_point
		--ON
		--fh_poly.[Freehold] = 1
		--AND
		--lh_point.[Freehold] <> 1
		--AND
		--fh_poly.PolygonGeography.STIntersects(lh_point.LatLng) = 1
		--AND 
		--(
		--	fh_poly.[TitleNumber] in (SELECT [TitleNumber] FROM  [analysis_AllChangedTitles])
		--	OR
		--	lh_point.[TitleNumber] in (SELECT [TitleNumber] FROM  [analysis_AllChangedTitles])
		--)

		--INSERT INTO UpdateResults ([timeof], [description])
		--VALUES (GETDATE(), CONVERT(varchar(10), @@ROWCOUNT) + ' Freehold to Leasehold records inserted.')

		--PRINT 'Finished updating the [FreeholdToLeashold] table.'



		--=============================== Update TitleInfo PostCode column ======================================
		
		----!!!!!! Incredibly SLOW
		----Which polygon should you use? The one with the biggest intersect into a postcode polygon?
		--SELECT TOP 1 c.[postcode] 
		--FROM [TitleInfo] i
		--INNER JOIN [Polygon] p ON p.[TitleNumber] = i.[TitleNumber]	 
		--INNER JOIN [CodePoint].[dbo].[Polygon] c ON p.[PolygonGeography].STIntersects(c.PostcodeGeography) = 1
		--WHERE i.[Titlenumber] = 'BL99495' --##################################
		--ORDER BY  p.[PolygonGeography].STIntersection(c.PostcodeGeography).STArea() DESC;
	
		--===================================== Listed Buildings ============================================
		--Update

		--UPDATE [TitleInfo] 
		--SET [HasListed] = 1
		--FROM [TitleInfo] ti
		--INNER JOIN [Polygon] tp on tp.[TitleNumber] = ti.[TitleNumber]
		--INNER JOIN [Environmental].[dbo].[ListedBuilding] lb
		--ON tp.PolygonGeography.STIntersects(lb.LatLong) = 1

		--Test code
		--SELECT ti.[titleNumber], lb.Easting, lb.Northing, tp.wkt4326
		--	FROM [TitleInfo] ti
		--INNER JOIN [Polygon] tp on tp.[TitleNumber] = ti.[TitleNumber]
		--INNER JOIN [Environmental].[dbo].[ListedBuilding] lb
		--ON tp.PolygonGeography.STIntersects(lb.LatLong) = 1

	
		--#################### Orphan polygon check ###############################
		DECLARE @orphanpolygons INT = (SELECT COUNT(DISTINCT [InspireID]) FROM [Polygon] WHERE [TitleNumber]	
										NOT IN (SELECT [TitleNumber] FROM [TitleInfo]))
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), CONVERT(varchar(20), @orphanpolygons) + ' orphan Polygons detected.')

			
		SET @Stat16_OrphanPolys = @orphanpolygons
		UPDATE [StatsForLatestRun] SET [Value] = @Stat16_OrphanPolys WHERE StatID = 16

		--End of update process, update transfer the stats for current run to the stats historical table.
		DECLARE @BuildType CHAR(4) = 'COU'
		IF @IsFullBuild = '1'
		BEGIN
			SET @BuildType = 'FULL'
		END

		INSERT INTO [StatsHistorical] 
		SELECT
			@StartTime,
			@BuildType,
			[StatID],
			[StatDescription],
			[Value],
			GETDATE()
		FROM [StatsForLatestRun] 

		SET @Status = 'success'
	END TRY

	BEGIN CATCH

		SET @Status = 'ERROR - ' + ERROR_MESSAGE(); 

		--Gather any error stats available.
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR NUMBER: ' + CONVERT(VARCHAR(MAX), ERROR_NUMBER()))
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR PROCEDURE: ' + ERROR_PROCEDURE())
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR LINE NUMBER: ' + CONVERT(VARCHAR(MAX), ERROR_LINE()))
		INSERT INTO UpdateResults ([timeof], [description])
		VALUES (GETDATE(), 'ERROR MESSAGE: ' + ERROR_MESSAGE())

	END CATCH
END
END
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [SPIX_PGeog]    Script Date: 18/03/2021 12:07:10 ******/
CREATE SPATIAL INDEX [SPIX_PGeog] ON [dbo].[Polygon]
(
	[PolygonGeography]
)USING  GEOGRAPHY_AUTO_GRID 
WITH (
CELLS_PER_OBJECT = 12, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [NPSUpdate] SET  READ_WRITE 
GO
