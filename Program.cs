﻿using System;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Globalization;
using System.Collections.Generic;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Text;
using Newtonsoft.Json.Linq;
using System.IO;
using System.IO.Compression;
using OSGeo.OSR;
using OSGeo.OGR;
using System.Linq;
using Azure.Storage.Blobs;

namespace NPSUpdate
{
	//Return codes
	// 1 = Could not retrieve last Nimbus update
	// 2 = Connection to the NPS API failed
	// 3 = Could not parse NPS API response.
	// 4 = Could not download NPS COU zip file
	// 5 = Could not download or unpack FULL NPS zip file.
	// 6 = Error sending shapefile block to SQL.
	// 7 = NOT USED
	// 8 = Could not retrieve email recipient list or last update ID.
	// 9 = SQL error starting update
	// 10 = Error during SQL shapefile process.
	// 11 = Error reading SQL [UpdateResults] table.
	// 12 = Could not read NPSUpdate configuration file.
	// 13 = Error during post polygon processing.
	// 14 = Error during .NET shapefile processing.
	// 15 = Error getting final stats.
	// 16 = Could not evaluate previous months UPRN file.
	// 17 = Could not find previous months UPRN file.
	// 18 = SQL error generating COU file from Land Registry UPRN data.
	// 19 = Safety catch is on.

	// 99 = No new data available test

	// 0 = Success new data processed


	class Program
	{
		static async Task<int> Main(string[] args)
		{
			DateTime startTime = DateTime.Now;

			//Dictionaries for manipulating dates during UPRN COU file generation.
			Dictionary<string, int> monthsToNums = new Dictionary<string, int>();
			monthsToNums.Add("JAN", 1);	monthsToNums.Add("FEB", 2);	monthsToNums.Add("MAR", 3);	monthsToNums.Add("APR", 4);
			monthsToNums.Add("MAY", 5);	monthsToNums.Add("JUN", 6);	monthsToNums.Add("JUL", 7);	monthsToNums.Add("AUG", 8);
			monthsToNums.Add("SEP", 9);	monthsToNums.Add("OCT", 10); monthsToNums.Add("NOV", 11); monthsToNums.Add("DEC", 12);
			Dictionary<int, string> numsToMonths = monthsToNums.ToDictionary((i) => i.Value, (i) => i.Key);

			//Logging variables
			StringBuilder sbLog = new StringBuilder();
			string sqlStatus;
			string emailBody;

			//Read in the configuration file.
			Dictionary<string, string> configSettings = new Dictionary<string, string>();
			string line;
			string[] row;
			try
			{
				StreamReader sreader = new StreamReader("NPSUpdateConfig.txt");
				while (!sreader.EndOfStream)
				{
					line = sreader.ReadLine();
					if (line.Trim() != "" && line[0] != '#')
					{
						row = line.Split('|');
						configSettings.Add(row[0].Trim(), row[1].Trim());
					}
				}
				int numTest;
				bool portIsNum = int.TryParse(configSettings["SMTPPort"], out numTest);
				bool flushIsNum = int.TryParse(configSettings["FlushPolyPoint"], out numTest);
				if (!portIsNum || !flushIsNum)
				{
					throw new Exception("Either the SMTP Port or the Flush Point are not numerical. Check the config file.");
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Could not read NPSUpdate configuration file.");
				Console.WriteLine(ex.Message);
				Console.WriteLine("Press any key to exit."); Console.ReadKey();
				return 12; // Could not read NPSUpdate configuration file.
			}

			WriteToScreenAndLog("\r\n################# NPSUpdate Update Log #################", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("NPS Update starting.", configSettings["LogFilePath"], ref sbLog, true);
			WriteToScreenAndLog("\r\nCONFIGURATION SETTINGS:", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Log File Path: " + configSettings["LogFilePath"], configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("NPS COU Data Storage Path: " + configSettings["COU_Directory_Path"], configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("NPS Full Storage Path: " + configSettings["Full_Directory_Path"], configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Historical UPRN Path: " + configSettings["HistoricalUPRN_Path"], configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Polygon Flush Point: " + configSettings["FlushPolyPoint"].ToString(), configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("GDAL PROJ4 String: " + configSettings["SourceGDALProj4"].ToString(), configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Destination PROJ4 String: " + configSettings["DestinationProj4"].ToString(), configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("\r\nTest Mode Enabled: " + configSettings["TestMode"], configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Full Rebuild Enabled: " + configSettings["FullMode"], configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Using Existing Predownloaded NPS Full Data: " + configSettings["UseExistingFull"].ToString(), configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Truncate Analysis Tables: " + configSettings["TruncateAnalysis"].ToString(), configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Blob Mode: " + configSettings["BlobMode"].ToString(), configSettings["LogFilePath"], ref sbLog);

			int useTestMode = 0;
			if (configSettings["TestMode"].ToUpper() == "YES")
			{
				useTestMode = 1;
			}

			//GDAL re-projection configurations.
			GdalConfiguration.ConfigureGdal();
			GdalConfiguration.ConfigureOgr();
			SpatialReference src = new SpatialReference("");
			src.ImportFromProj4(configSettings["SourceGDALProj4"].ToString());
			SpatialReference dst = new SpatialReference("");
			dst.ImportFromProj4(configSettings["DestinationProj4"]);
			CoordinateTransformation ct = new CoordinateTransformation(src, dst);

			//--------------------------------------------------------------------------------
			//Test section for quick testing of the current re-projection settings.
			if (configSettings["GDALReprojectionTest"].ToUpper() == "YES")
			{
				List<double[]> liTestData = new List<double[]>();
				liTestData.Add(new double[] { 422242.186, 433818.701 });
				liTestData.Add(new double[] { 267056.768, 846176.972 });
				liTestData.Add(new double[] { 395999.668, 1138728.951 });
				Console.WriteLine("-------------------------------------------------------");
				int tcount = 0;
				foreach (double[] td in liTestData)
				{
					tcount++;
					Console.WriteLine("{0}. Easting: {1} Northing: {1}", tcount, td[0], td[1]);
					ct.TransformPoint(td);
					Console.WriteLine("{0}. Lat: {2} Long: {1}", tcount, td[0], td[1]);
					Console.WriteLine("-------------------------------------------------------");

				}
				Console.ReadKey();
				return 888;
			}
			//--------------------------------------------------------------------------------

			//Get the email recipients for admin mailing and the ID number of the last entry in the [UpdateResults] table (the end of the previous updates logging).
			List<string> emailRecipients = new List<string>();
			int lastUpdateID = 0;
			using (SqlConnection conn = new SqlConnection(configSettings["DataBaseConnectionString"]))
			{
				try
				{
					conn.Open();
					SqlCommand command;

					/* Check the safety catch. */
					if (configSettings["FullMode"].ToUpper() == "YES")
					{
						command = new SqlCommand("SELECT [SafetyCatch_ON] FROM [SafetyCatch]", conn);
						command.CommandTimeout = 600;
						string safetyCatch = "True";
						using (SqlDataReader sqlreader = command.ExecuteReader())
						{
							while (sqlreader.Read())
							{
								safetyCatch = sqlreader["SafetyCatch_ON"].ToString();
							}
						}
						if (safetyCatch == "True")
						{
							WriteToScreenAndLog("The Safety Catch is on.", configSettings["LogFilePath"], ref sbLog);
							WriteToScreenAndLog("To carry out a full build you must manually set the [SafetyCatch_ON]" +
												" field in the [SafetyCatch] table to 'False'.", configSettings["LogFilePath"], ref sbLog);
							Console.WriteLine("Press any key to exit."); Console.ReadKey();
							return 19;
						}
					}

					command = new SqlCommand("SELECT [RecipientEmail] FROM [EmailRecipients] ", conn);
					command.CommandTimeout = 600;
					using (SqlDataReader sqlreader = command.ExecuteReader())
					{
						while (sqlreader.Read())
						{
							emailRecipients.Add(sqlreader["RecipientEmail"].ToString());
						}
					}

					command = new SqlCommand("SELECT MAX(ID) FROM [UpdateResults]", conn);
					command.CommandTimeout = 600;
					var serverRepsonse = command.ExecuteScalar().ToString();
					if (serverRepsonse != "")
					{
						lastUpdateID = Convert.ToInt32(command.ExecuteScalar());
					}
					else
					{
						lastUpdateID = 0;
					}
				}
				catch (Exception ex)
				{
					WriteToScreenAndLog("Could not retrieve email recipient list or last update ID.", configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
					Console.WriteLine("Press any key to exit."); Console.ReadKey();
					return 8;
				}
			}

			if (configSettings["FullMode"].ToUpper() == "YES")
			{
				WriteToScreenAndLog("A full rebuild will be perfomed.", configSettings["LogFilePath"], ref sbLog);
			}
			else
			{
				WriteToScreenAndLog("A COU update will be perfomed.", configSettings["LogFilePath"], ref sbLog);
			}

			//Get the required files for update.
			string[] allShapeFiles = new string[] { "" };
			string tenureFile = "";
			string uprnFile = "";
			string prevUPRNFile = "";
			//======================================== TEST MODE ====================================================================//
			if (configSettings["TestMode"].ToUpper() == "YES")
			{
				WriteToScreenAndLog("\r\n<<<<< TEST MODE ON >>>>>", configSettings["LogFilePath"], ref sbLog);
				WriteToScreenAndLog("No new data is downloaded in test mode. Existing data is used.", configSettings["LogFilePath"], ref sbLog);
			}
			else
			{
				//Check the log table to see when the last NPS update was applied
				DateTime lastNimbusNPSUpdate;
				using (SqlConnection conn = new SqlConnection(configSettings["DataBaseConnectionString"]))
				{
					try
					{
						conn.Open();
						SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM [UpdateResults] WHERE [description] = 'SUCCESS'", conn);
						command.CommandTimeout = 600; //in seconds
						if (Convert.ToInt32(command.ExecuteScalar()) == 0)
						{
							lastNimbusNPSUpdate = DateTime.MinValue;
						}
						else
						{
							command = new SqlCommand("SELECT MAX(timeof) FROM [UpdateResults] WHERE [description] = 'SUCCESS'", conn);
							lastNimbusNPSUpdate = Convert.ToDateTime(command.ExecuteScalar());
						}
					}
					catch (Exception ex)
					{
						WriteToScreenAndLog("\r\nCould not retrieve the last Nimbus update date from the database.", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
						emailBody = "NPSUpdate - Could not retrieve last Nimbus update from database.\r\n";
						emailBody += "Error message: " + ex.Message;
						emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
						sendInformationEmail(emailBody, "ERROR 1: NPSUpdate - Could not retrieve last Nimbus update date.", emailRecipients, configSettings);
						Console.WriteLine("Press any key to exit."); Console.ReadKey();
						return 1;
					}
				}

				WriteToScreenAndLog("\r\nLast update of Nimbus NPS data was : " + lastNimbusNPSUpdate.ToString(), configSettings["LogFilePath"], ref sbLog);


				string responseBody = "";
				HttpClient client = new HttpClient();
				JObject npsResponseJason;
				string remoteDownloadFilename = null;
				string remoteDownloadFilenameFULL = null;


				// Query the NPS API using the Nimbus API passed in the Authentication header
				WriteToScreenAndLog("Querying NPS API.", configSettings["LogFilePath"], ref sbLog);

				client.DefaultRequestHeaders.Add("user-agent", "Mozilla");
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(configSettings["LRAPIKey"]);
				try
				{
					HttpResponseMessage response = await client.GetAsync(new Uri(configSettings["npsAPI"]));
					response.EnsureSuccessStatusCode();
					responseBody = await response.Content.ReadAsStringAsync();
				}
				catch (Exception ex)
				{
					WriteToScreenAndLog("\r\nConnection to the NPS API failed.", configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
					emailBody = "NPSUpdate - Connection to the Land Registry NPS API failed.\r\n";
					emailBody += "Error message: " + ex.Message;
					emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
					sendInformationEmail(emailBody, "ERROR 2: NPSUpdate - Land Registry NPS API Call Failed.", emailRecipients, configSettings);
					Console.WriteLine("Press any key to exit."); Console.ReadKey();
					return 2;
				}

				//Parse the returned JSON and establish if there are any new updates.
				npsResponseJason = JObject.Parse(responseBody);
				DateTime lastApiNPSUpdate = DateTime.MinValue;
				try
				{
					lastApiNPSUpdate = DateTime.Parse((string)npsResponseJason["result"]["last_updated"], new CultureInfo("en-GB"));
					foreach (JObject resource in npsResponseJason["result"]["resources"])
					{
						if (resource["name"].ToString() == "Change Only File")
						{
							remoteDownloadFilename = resource["file_name"].ToString();
							break;
						}
					}
					foreach (JObject resource in npsResponseJason["result"]["resources"])
					{
						if (resource["name"].ToString() == "Full File")
						{
							remoteDownloadFilenameFULL = resource["file_name"].ToString();
							break;
						}
					}
				}
				catch (Exception ex)
				{
					WriteToScreenAndLog("\r\nCould not parse NPS API response.", configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
					emailBody = "NPSUpdate - Could not parse NPS API response.\r\n";
					emailBody += "Error message: " + ex.Message;
					emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
					sendInformationEmail(emailBody, "ERROR 3: NPSUpdate - Could Not parse API Response.", emailRecipients, configSettings);
					Console.WriteLine("Press any key to exit."); Console.ReadKey();
					return 3;
				}

				// Check if new data is available.
				if (configSettings["FullMode"].ToUpper() != "YES" && DateTime.Compare(lastNimbusNPSUpdate, lastApiNPSUpdate) > 0)
				{
					WriteToScreenAndLog("No new data available.", configSettings["LogFilePath"], ref sbLog);
					sendInformationEmail(sbLog.ToString(), "NPSUpdate - No New Data AVailable", emailRecipients, configSettings);
					Console.WriteLine("Press any key to exit."); Console.ReadKey();
					return 99; //No new data available
				}

				//An update is available 
				WriteToScreenAndLog("NPS Data Last Update Date: " + lastApiNPSUpdate.ToString(), configSettings["LogFilePath"], ref sbLog);
				WriteToScreenAndLog("NEW DATA AVAILABLE.\r\n", configSettings["LogFilePath"], ref sbLog);

				//################################ Get FULL files from Land Registry.########################################
				//(Currently always needed as COU update needs the UPRN file that is only in the full data.)

				try
				{
					WriteToScreenAndLog("Downloading FULL data set (currently needed for COU updates also).", configSettings["LogFilePath"], ref sbLog, true);

					if (configSettings["UseExistingFull"].ToUpper() != "YES")
					{
						//Clear any old data.
						if (Directory.Exists(configSettings["Full_Directory_Path"]))
						{
							Directory.Delete(configSettings["Full_Directory_Path"], true);
						}
						Directory.CreateDirectory(configSettings["Full_Directory_Path"]);

						//Query API to get authorised download link (valid for 10 seconds).
						HttpResponseMessage response = await client.GetAsync(new Uri(configSettings["npsAPI"] + "/" + remoteDownloadFilenameFULL));
						response.EnsureSuccessStatusCode();
						responseBody = await response.Content.ReadAsStringAsync();
						npsResponseJason = JObject.Parse(responseBody);
						string downloadURL = npsResponseJason["result"]["download_url"].ToString();

						//Download the zip file from the authorised link.
						client.DefaultRequestHeaders.Remove("Authorization");
						response = await client.GetAsync(downloadURL, HttpCompletionOption.ResponseHeadersRead);
						using (var fs = new FileStream(configSettings["Full_Directory_Path"] + "\\" + remoteDownloadFilenameFULL, FileMode.CreateNew))
						{
							await response.Content.CopyToAsync(fs);
						}

						WriteToScreenAndLog("Full file downloaded. Unzipping...", configSettings["LogFilePath"], ref sbLog, true);
						ZipFile.ExtractToDirectory(configSettings["Full_Directory_Path"] + "\\" + remoteDownloadFilenameFULL, configSettings["Full_Directory_Path"]);
						WriteToScreenAndLog("Full file unzipped.", configSettings["LogFilePath"], ref sbLog, true);
					}
					else
					{
						WriteToScreenAndLog("\r\n<<<<< USING EXISTING FULL FILES >>>>>", configSettings["LogFilePath"], ref sbLog);
					}
				}
				catch (Exception ex)
				{
					WriteToScreenAndLog("\r\nCould not download or unpack FULL NPS zip file.", configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
					emailBody = "NPSUpdate - Could not download or unpack FULL NPS zip file.\r\n";
					emailBody += "Error message: " + ex.Message;
					emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
					sendInformationEmail(emailBody, "ERROR 5: NPSUpdate - Could not Download or Unpack FULL NPS zip File.", emailRecipients, configSettings);
					Console.WriteLine("Press any key to exit."); Console.ReadKey();
					return 5;
				}

			

				//#################################### Get COU files from Land Registry ###########################################
				if (configSettings["FullMode"].ToUpper() != "YES")
				{
					try
					{
						WriteToScreenAndLog("Downloading new COU data.", configSettings["LogFilePath"], ref sbLog, true);
						//Ensure receiving directory is clean before attempting download and unzip
						if (Directory.Exists(configSettings["COU_Directory_Path"]))
						{
							Directory.Delete(configSettings["COU_Directory_Path"], true);
						}
						Directory.CreateDirectory(configSettings["COU_Directory_Path"]);

						//Query API to get download authorised link.

						client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(configSettings["LRAPIKey"]);
						client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
						HttpResponseMessage response = await client.GetAsync(new Uri(configSettings["npsAPI"] + "/" + remoteDownloadFilename));
						response.EnsureSuccessStatusCode();
						responseBody = await response.Content.ReadAsStringAsync();
						npsResponseJason = JObject.Parse(responseBody);
						string downloadURL = npsResponseJason["result"]["download_url"].ToString();

						//Download the zip file from the authorised link.
						client.DefaultRequestHeaders.Remove("Authorization");
						response = await client.GetAsync(downloadURL);
						using (var fs = new FileStream(configSettings["COU_Directory_Path"] + "\\" + remoteDownloadFilename, FileMode.CreateNew))
						{
							await response.Content.CopyToAsync(fs);
						}
						WriteToScreenAndLog("NPS COU file downloaded. Unzipping...", configSettings["LogFilePath"], ref sbLog, true);
						ZipFile.ExtractToDirectory(configSettings["COU_Directory_Path"] + "\\" + remoteDownloadFilename, configSettings["COU_Directory_Path"]);
						WriteToScreenAndLog("NPS COU unzipped.", configSettings["LogFilePath"], ref sbLog, true);
					}
					catch (Exception ex)
					{
						WriteToScreenAndLog("Could not download or unpack NPS COU zip file.", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
						emailBody = "NPSUpdate - Could not download or unpack NPS COU zip file.\r\n";
						emailBody += "Error message: " + ex.Message;
						emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
						sendInformationEmail(emailBody, "ERROR 4: NPSUpdate - Could Download or Unpack NPS COU file.", emailRecipients, configSettings);
						Console.WriteLine("Press any key to exit."); Console.ReadKey();
						return 4;
					}
				}
			} //End of ----- TEST MODE ----- IF


			if (configSettings["SkipTenureUPRN"].ToUpper() != "YES")
			{
				//##################################### Get UPRN and Tenure files from downloaded data #########################################
				//UPRN file is currently only available in the full data set.
				uprnFile = Directory.GetFiles(configSettings["Full_Directory_Path"], "LR_UPRN*.csv", SearchOption.AllDirectories)[0];

				//Copy the UPRN file to the historical storage folder (previous versions are used to generate the UPRN change only update file).
				File.Copy(uprnFile, configSettings["HistoricalUPRN_Path"] + "\\" + Path.GetFileName(uprnFile), true);

				//Delete historical files.
				string[] files = Directory.GetFiles(configSettings["HistoricalUPRN_Path"]);
				foreach (string file in files)
				{
					FileInfo fi = new FileInfo(file);
					if (fi.LastWriteTime < DateTime.Now.AddDays(-90))
						fi.Delete();
				}

				//Only generate find the previous months Land Registry UPRN file if this is a COU update.
				//This file will be sent to SQL and used to generate a COU table for the Land Registry UPRN data.
				StringBuilder prevMonthFileName = new StringBuilder();
				if (configSettings["FullMode"].ToUpper() != "YES")
				{
					try
					{
						//Generate Land Registry TitleNumber/UPRN COU file.
						//Get current month of land registry UPRN file from the filename of the latest file.
						string[] currentParts = Path.GetFileName(uprnFile).Split('_');

						int prevMonthInt = 0;
						int currentMonthInt = monthsToNums[currentParts[3].ToUpper()];
						int prevYear = Int32.Parse(currentParts[4].Split('.')[0]);
						if (currentMonthInt == 1)
						{
							prevMonthInt = 12;
							prevYear = prevYear - 1;
						}
						else
						{
							prevMonthInt = currentMonthInt - 1;
						}

						//Build the file name for the previous months file.
						prevMonthFileName.Append(configSettings["HistoricalUPRN_Path"] + "\\");
						prevMonthFileName.Append("LR_UPRN_FULL_");
						prevMonthFileName.Append(numsToMonths[prevMonthInt] + "_");
						prevMonthFileName.Append(prevYear.ToString() + ".csv");
						prevUPRNFile = prevMonthFileName.ToString();                  
					}
					catch (Exception ex)
					{
						WriteToScreenAndLog("Could not generate the path of the previous months Land Registry UPRN file.", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog("Generation of a COU Land Registry UPRN file can not reliably take place.", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
						emailBody = "NPSUpdate - Could not generate LR UPRN COU file.\r\n";
						emailBody += "Could not generate a path for the previous months Land Registry UPRN file.";
						emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
						sendInformationEmail(emailBody, "ERROR 16: NPSUpdate - Could not generate the previous months UPRN file.", emailRecipients, configSettings);
						Console.WriteLine("Press any key to exit."); Console.ReadKey();
						return 16;
					}

					//Check that the previous months file is available for generation of the COU LR UPRN file.
					if (!File.Exists(prevUPRNFile))
					{
						WriteToScreenAndLog("Could not find the previous months Land Registry UPRN file.", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog("Generation of a COU Land Registry UPRN file can not reliably take place.", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog("The missing filename is: " + prevUPRNFile, configSettings["LogFilePath"], ref sbLog);
						emailBody = "NPSUpdate - Could not find the previous LR UPRN COU file.\r\n";
						emailBody += "\r\nThe missing filename is: " + prevUPRNFile;
						sendInformationEmail(emailBody, "ERROR 17: NPSUpdate - Could not find previous months UPRN file.", emailRecipients, configSettings);
						Console.WriteLine("Press any key to exit."); Console.ReadKey();
						return 17;
					}
				}

				//There are COU and FULL versions available for the TENURE file, select accordingly.
				if (configSettings["FullMode"].ToUpper() == "YES")
				{
					tenureFile = Directory.GetFiles(configSettings["Full_Directory_Path"], "LR_TENURE_FULL*.csv", SearchOption.AllDirectories)[0];
				}
				else //COU update only
				{
					tenureFile = Directory.GetFiles(configSettings["COU_Directory_Path"], "LR_TENURE_COU*.csv", SearchOption.AllDirectories)[0];
				}


				// ############### Copy the UPRN and Tenure files to a set location and filename that SQL will bulk upload from. ############### 

				//---UPRN
				//Copy the UPRN file to a location where SQL will bulk upload from.
				//Only copy the UPRN file if this is a FULL update.
				//COU updates will use a UPRN COU table generated during the SQL process that follows shortly.
				if (configSettings["FullMode"].ToUpper() == "YES")
				{
					File.Delete(configSettings["Full_Directory_Path"] + "\\NewUPRNData.csv");
					File.Copy(uprnFile, configSettings["Full_Directory_Path"] + "\\NewUPRNData.csv");
				}

				//---TENURE FILE 
				if (configSettings["FullMode"].ToUpper() == "YES")
				{
					File.Delete(configSettings["Full_Directory_Path"] + "\\NewTenureData.csv");
					File.Copy(tenureFile, configSettings["Full_Directory_Path"] + "\\NewTenureData.csv");

				}
				else //COU update only
				{
					File.Delete(configSettings["COU_Directory_Path"] + "\\NewTenureData.csv");
					File.Copy(tenureFile, configSettings["COU_Directory_Path"] + "\\NewTenureData.csv");
				}
			}

			//###################################### Get shapefile locations ##############################################
			if (configSettings["FullMode"].ToUpper() == "YES")
			{
				allShapeFiles = Directory.GetFiles(configSettings["Full_Directory_Path"], "LR_POLY_FULL*.shp", SearchOption.AllDirectories);
			}
			else //COU update only
			{
				allShapeFiles = Directory.GetFiles(configSettings["COU_Directory_Path"], "LR_POLY_COU*.shp", SearchOption.AllDirectories);
			}
			//################################ End of file download ###########################################


			//If blob mode is enabled (cloud based update), copy all files to the blob. 
			if (configSettings["BlobMode"].ToUpper() == "YES")
			{
				WriteToScreenAndLog("\r\nTransferring data to the blob...", configSettings["LogFilePath"], ref sbLog);
				WriteToScreenAndLog("\r\nBlob write start: " + DateTime.Now.ToString(), configSettings["LogFilePath"], ref sbLog);
				var options = new BlobClientOptions();
				options.Retry.NetworkTimeout = TimeSpan.FromMinutes(30);
				// Create a BlobServiceClient object to link to blob storage.
				BlobServiceClient blobServiceClient = new BlobServiceClient(configSettings["BlobConnectionString"]);
				BlobClient blobClient;

				if (configSettings["FullMode"].ToUpper() == "YES")
				{
					BlobContainerClient existingcontainerClient = blobServiceClient.GetBlobContainerClient("npsupdate-full");

					blobClient = existingcontainerClient.GetBlobClient("NewTenureData.csv");
					await blobClient.UploadAsync(configSettings["Full_Directory_Path"] + "\\NewTenureData.csv", true);

					blobClient = existingcontainerClient.GetBlobClient("NewUPRNData.csv");
					await blobClient.UploadAsync(configSettings["Full_Directory_Path"] + "\\NewUPRNData.csv", true);
				}
				else //COU mode.
				{
					BlobContainerClient existingcontainerClient = blobServiceClient.GetBlobContainerClient("npsupdate-cou");

					blobClient = existingcontainerClient.GetBlobClient("NewTenureData.csv");
					await blobClient.UploadAsync(configSettings["COU_Directory_Path"] + "\\NewTenureData.csv", true);

					//prevUPRNFile
					//uprnFile


				}
			}


				if (configSettings["SkipTenureUPRN"].ToUpper() != "YES")
			{
				//Trigger SQL Stored Procedure to set-up for a new update.
				//SQL side this sets up the tables ready for the update operation and carries out Tenure and UPRN updates.
				WriteToScreenAndLog("\r\n################################################################################", configSettings["LogFilePath"], ref sbLog);
				WriteToScreenAndLog("################################################################################", configSettings["LogFilePath"], ref sbLog);
				WriteToScreenAndLog("Starting SQL processing of Tenure/UPRN File.", configSettings["LogFilePath"], ref sbLog, true);
				using (SqlConnection conn = new SqlConnection(configSettings["DataBaseConnectionString"]))
				{
					try
					{
						conn.Open();
						SqlCommand command = new SqlCommand("spNPSUpdate", conn);
						command.CommandTimeout = 72000; //in seconds
						command.CommandType = CommandType.StoredProcedure;
						command.Parameters.Add("@ProcessAction", SqlDbType.Char, 1).Value = 'N';

						//==================================================================================
						//Local debug hack - this should have been removed...
						//configSettings["Full_Directory_Path"] = configSettings["Full_Directory_Path"].Replace("B:\\", "F:\\DataUpdateApplications\\");
						//configSettings["COU_Directory_Path"] = configSettings["COU_Directory_Path"].Replace("B:\\", "F:\\DataUpdateApplications\\");
						//prevUPRNFile = prevUPRNFile.Replace("B:\\", "F:\\DataUpdateApplications\\");
						//uprnFile = uprnFile.Replace("B:\\", "F:\\DataUpdateApplications\\");
						//============================================================================

						command.Parameters.Add("@FullFileLocation", SqlDbType.VarChar, 250).Value = configSettings["Full_Directory_Path"];
						command.Parameters.Add("@COUFileLocation", SqlDbType.VarChar, 250).Value = configSettings["COU_Directory_Path"];
						command.Parameters.Add("@PreviousMonthLocation", SqlDbType.VarChar, 250).Value = prevUPRNFile;
						command.Parameters.Add("@CurrentMonthLocation", SqlDbType.VarChar, 250).Value = uprnFile;
						command.Parameters.Add("@testMode", SqlDbType.Bit, 1).Value = useTestMode;
						command.Parameters.Add("@Status", SqlDbType.VarChar, 2000).Direction = ParameterDirection.Output;
						if (configSettings["FullMode"].ToUpper() == "YES")
						{
							command.Parameters.Add("@IsFullBuild", SqlDbType.Bit).Value = 1;
						}
						else
						{
							command.Parameters.Add("@IsFullBuild", SqlDbType.Bit).Value = 0;
						}
						if (configSettings["TruncateAnalysis"].ToUpper() == "YES")
						{
							command.Parameters.Add("@TruncateAnalysis", SqlDbType.Bit).Value = 1;
						}
						else
						{
							command.Parameters.Add("@TruncateAnalysis", SqlDbType.Bit).Value = 0;
						}
						command.ExecuteNonQuery();
						sqlStatus = command.Parameters["@Status"].Value.ToString();
						if (sqlStatus != "success")
						{
							throw new Exception(sqlStatus);
						}

						//Get the SQL log.
						WriteToScreenAndLog("\r\nSQL log of Tenure/UPRN File Processing:", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog("---------------------------------------", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog(GetDBLog(configSettings["DataBaseConnectionString"], ref lastUpdateID).ToString(), configSettings["LogFilePath"], ref sbLog);

					}
					catch (Exception ex)
					{
						WriteToScreenAndLog(GetDBLog(configSettings["DataBaseConnectionString"], ref lastUpdateID).ToString(), configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog("SQL error starting update.", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
						emailBody = "NPSUpdate - SQL Error Processing Tenure/UPRN data.\r\n";
						emailBody += "Error message: " + ex.Message;
						emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
						sendInformationEmail(emailBody, "ERROR 9: NPSUpdate - SQL Error Processing Tenure/UPRN data.", emailRecipients, configSettings);
						Console.WriteLine("Press any key to exit."); Console.ReadKey();
						return 9;
					}
				}
			}

			// Load shapefile(s)
			WriteToScreenAndLog("\r\n################################################################################", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("################################################################################", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Starting Shapefile Processing.", configSettings["LogFilePath"], ref sbLog, true);
			DataTable polygonDataTable = GetPolygonDatatable();
			DataRow polyRow;
			Layer layer;
			Feature feature;
			Geometry parentGeometry;
			Geometry childGeometry;

			//Processing stats
			int statsPolygonsAdded = 0;
			int statsShapefileCounter = 0;
			int statsPolygonsProcessed = 0;
			int statsTotalPolygonsProcessed = 0;

			//Capture features with no geography.
			//This can happen when the .dbf and .shp file are out of sync.
			//I.E. There is a PolygonID in the .dbf file that has no corresponding geometry in the .shp file.
			//This has been seen in the Land Registry sample data but not in their live data (yet).
			int statsNoGeography = 0;
			int statsTotalNoGeography = 0;

			//Shapefile processing variables.
			string blockSendResult = "";
			long shapefileRows = 0;
			double[] geomPoint = new double[2];
			string wkt27700Output = "";
			string wkt4326Output = "";
			string wktLatLongOutput = "";
			int centroidValue = 0;
			int numRows = 0;
			int progressCounter = 0;
			double[] singleCoordinate = new double[2];

			Ogr.RegisterAll();
			Driver drv = Ogr.GetDriverByName("ESRI Shapefile");
			DataSource ds;

			//Loop through all shapefiles downloaded from the Land Registry.
			foreach (string shapeFileName in allShapeFiles)
			{
				try
				{
					statsShapefileCounter++;
					WriteToScreenAndLog("Opening shapefile " + statsShapefileCounter.ToString() + " of " + allShapeFiles.Length.ToString()
																+ " - " + shapeFileName, configSettings["LogFilePath"], ref sbLog, true);

					ds = drv.Open(shapeFileName, 0);
					layer = ds.GetLayerByIndex(0);
					layer.ResetReading();
					shapefileRows = layer.GetFeatureCount(numRows);
					WriteToScreenAndLog("Number of polygons: " + shapefileRows, configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog("Processing shapefile.", configSettings["LogFilePath"], ref sbLog);
					progressCounter = 0;

					while ((feature = layer.GetNextFeature()) != null)
					{

						polyRow = polygonDataTable.NewRow();

						statsPolygonsProcessed++;
						progressCounter++;

						//Progress - visible on command line only (for when testing).
						if (progressCounter % 50000 == 0) { Console.Write("\r{0}% done", progressCounter / (shapefileRows / 100));  }

						//-----Inspire ID
						polyRow[(int)NPS_PolygonInHeaders.InspireID] = feature.GetFieldAsString((int)NPS_ShapefileDataRowHeaders.PolyID);

						//-----Inspire IDPart
						//This is set to 0 here and is used on the SQL processing side when splitting multi-polygons to identify individual polygon parts.
						polyRow[(int)NPS_PolygonInHeaders.InspireIDPart] = 0;

						//-----Title Number
						polyRow[(int)NPS_PolygonInHeaders.TitleNumber] = feature.GetFieldAsString((int)NPS_ShapefileDataRowHeaders.TitleNo);

						//-----INSERT/UPDATE
						polyRow[(int)NPS_PolygonInHeaders.InsertDate] = DateTime.ParseExact(feature.GetFieldAsString((int)NPS_ShapefileDataRowHeaders.InsertDate), "yyyy-MM-ddTHH:mm:ss", null);
						polyRow[(int)NPS_PolygonInHeaders.UpdateDate] = DateTime.ParseExact(feature.GetFieldAsString((int)NPS_ShapefileDataRowHeaders.UpdateDate), "yyyy-MM-ddTHH:mm:ss", null);

						//-----Record Status
						polyRow[(int)NPS_PolygonInHeaders.Rec_Status] = feature.GetFieldAsString((int)NPS_ShapefileDataRowHeaders.RecStatus);

						//Get main parent geometry reference using feature.GetGeometryRef().
						//Any multi-part polygons (e.g. shapes with holes in them) will have child geometries that can be accessed
						//  using GetGeometryRef(x) again on the resultant object where x is the index of the child.
						parentGeometry = feature.GetGeometryRef();

						//See note at definition of statsNoGeography. This can happen with corrupt shapefiles.
						if (parentGeometry == null)
						{
							statsNoGeography++;
							Console.WriteLine(feature.GetFieldAsString((int)NPS_ShapefileDataRowHeaders.PolyID) + " had no geography. SKIPPED.");
							continue;
						}

						// Get the geographic data in the default un-projected OSGB36 (27700) format.
						//-----Single Easting and Northing
						polyRow[(int)NPS_PolygonInHeaders.SingleEasting] = parentGeometry.Centroid().GetX(centroidValue);
						polyRow[(int)NPS_PolygonInHeaders.SingleNorthing] = parentGeometry.Centroid().GetY(centroidValue);

						//-----Area (square m)
						polyRow[(int)NPS_PolygonInHeaders.Area] = parentGeometry.GetArea();

						//-----WKT 27700
						parentGeometry.ExportToWkt(out wkt27700Output);
						polyRow[(int)NPS_PolygonInHeaders.WKT27700] = wkt27700Output;

						//Re-project from OSGB to 4326.
						//Note: all child geometries must be converted individually (hence the loop).
						for (int ix = 0; ix < parentGeometry.GetGeometryCount(); ix++)
						{
							childGeometry = parentGeometry.GetGeometryRef(ix);
							for (int feature_ind = 0; feature_ind < childGeometry.GetPointCount(); feature_ind++)
							{
								childGeometry.GetPoint(feature_ind, singleCoordinate);
								ct.TransformPoint(singleCoordinate);
								childGeometry.SetPoint(feature_ind, singleCoordinate[0], singleCoordinate[1], 1);
							}
						}

						//--- Lat & Long
						polyRow[(int)NPS_PolygonInHeaders.Lat] = parentGeometry.Centroid().GetY(centroidValue);
						polyRow[(int)NPS_PolygonInHeaders.Long] = parentGeometry.Centroid().GetX(centroidValue);

						//--- WKT4326
						parentGeometry.ExportToWkt(out wkt4326Output);
						polyRow[(int)NPS_PolygonInHeaders.WKT4326] = wkt4326Output;

						//--- PolygonGeography
						polyRow[(int)NPS_PolygonInHeaders.PolygonGeography] = polyRow[(int)NPS_PolygonInHeaders.WKT4326].ToString();

						//--- LatLng
						parentGeometry.Centroid().ExportToWkt(out wktLatLongOutput);
						polyRow[(int)NPS_PolygonInHeaders.LatLng] = wktLatLongOutput;

						polygonDataTable.Rows.InsertAt(polyRow, statsPolygonsAdded);
						statsPolygonsAdded++;

						//Check to see if the data-table is at flush point.
						if (polygonDataTable.Rows.Count >= Convert.ToInt32(configSettings["FlushPolyPoint"]))
						{
							Console.WriteLine(" Sending block.");
							blockSendResult = SendBlockToDatabase(polygonDataTable, configSettings["DataBaseConnectionString"]);
							if (blockSendResult == "success")
							{
								polygonDataTable.Rows.Clear();
							}
							else
							{
								WriteToScreenAndLog("\r\nError sending shapefile block to SQL.", configSettings["LogFilePath"], ref sbLog);
								WriteToScreenAndLog(blockSendResult, configSettings["LogFilePath"], ref sbLog);
								emailBody = "NPSUpdate - Error sending shapefile block to SQL.\r\n";
								emailBody += "Error message: " + blockSendResult;
								emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
								sendInformationEmail(emailBody, "ERROR 6: NPSUpdate - Error sending shapefile block to SQL.", emailRecipients, configSettings);
								Console.WriteLine("Press any key to exit."); Console.ReadKey();
								return 6;
							}
							Console.Write("Block sent.");
						}
					}
				}
				catch (Exception ex)
				{
					WriteToScreenAndLog("Error during .NET shapefile processing.", configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
					emailBody = "NPSUpdate - Error during .NET shapefile process.\r\n";
					emailBody += "Error message: " + ex.Message;
					emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
					sendInformationEmail(emailBody, "ERROR 14: NPSUpdate - Error during .NET shapefile processing.", emailRecipients, configSettings);
					Console.WriteLine("Press any key to exit."); Console.ReadKey();
					return 14;
				}
				//End of shapefile processing - send whatever is left un-flushed in the polygon data-table.
				blockSendResult = SendBlockToDatabase(polygonDataTable, configSettings["DataBaseConnectionString"]);
				if (blockSendResult == "success")
				{
					polygonDataTable.Rows.Clear();
				}
				else
				{
					WriteToScreenAndLog("\r\nError sending last shapefile block to SQL.", configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog(blockSendResult, configSettings["LogFilePath"], ref sbLog);
					emailBody = "NPSUpdate - Error sending shapefile block to SQL.\r\n";
					emailBody += "Error message: " + blockSendResult;
					emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
					sendInformationEmail(emailBody, "ERROR 6: NPSUpdate - Error sending last shapefile block to SQL.", emailRecipients, configSettings);
					Console.WriteLine("Press any key to exit."); Console.ReadKey();
					return 6;
				}
				Console.WriteLine("100% done.\r\n");
				WriteToScreenAndLog("Processing complete, polygons processed: " + statsPolygonsProcessed.ToString(), configSettings["LogFilePath"], ref sbLog, true);
				WriteToScreenAndLog(statsNoGeography.ToString() + " features skipped due to having no geography.\r\n", configSettings["LogFilePath"], ref sbLog);
				WriteToScreenAndLog(statsPolygonsAdded.ToString() + " polygons sent to SQL.\r\n", configSettings["LogFilePath"], ref sbLog);


				//Calculate total update process stats and reset per shapefile counters.
				statsTotalPolygonsProcessed = statsTotalPolygonsProcessed + statsPolygonsProcessed;
				statsTotalNoGeography = statsTotalNoGeography + statsNoGeography;

				statsPolygonsAdded = 0;
				statsPolygonsProcessed = 0;
				statsNoGeography = 0;


				WriteToScreenAndLog("--- Starting SQL processing of Shapefile ---.", configSettings["LogFilePath"], ref sbLog, true);
				//----------- Trigger shapefile process ---------------//
				using (SqlConnection conn = new SqlConnection(configSettings["DataBaseConnectionString"]))
				{
					try
					{
						SqlCommand command = new SqlCommand("spNPSUpdate", conn);
						command.CommandTimeout = 72000; //in seconds
						command.CommandType = CommandType.StoredProcedure;
						command.Parameters.Add("@ProcessAction", SqlDbType.Char, 1).Value = 'S';
						command.Parameters.Add("@Status", SqlDbType.VarChar, 2000).Direction = ParameterDirection.Output;
						conn.Open();
						command.ExecuteNonQuery();
						sqlStatus = command.Parameters["@Status"].Value.ToString();
						if (sqlStatus != "success")
						{
							throw new Exception(sqlStatus);
						}
						//Get the SQL log.
						WriteToScreenAndLog("\r\nSQL log of shapefile processing:", configSettings["LogFilePath"], ref sbLog);
						SqlCommand getSqlLogCommand = new SqlCommand("SELECT * FROM UpdateResults WHERE [id] > @lastID ORDER BY [id] ASC", conn);
						WriteToScreenAndLog(GetDBLog(configSettings["DataBaseConnectionString"], ref lastUpdateID).ToString(), configSettings["LogFilePath"], ref sbLog);

					}
					catch (Exception ex)
					{
						WriteToScreenAndLog(GetDBLog(configSettings["DataBaseConnectionString"], ref lastUpdateID).ToString(), configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog("Error during SQL shapefile process.", configSettings["LogFilePath"], ref sbLog);
						WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
						emailBody = "NPSUpdate - Error during SQL shapefile process.\r\n";
						emailBody += "Error message: " + ex.Message;
						emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
						sendInformationEmail(emailBody, "ERROR 10: NPSUpdate - Error during SQL shapefile process.", emailRecipients, configSettings);
						Console.WriteLine("Press any key to exit."); Console.ReadKey();
						return 10;
					}
				}

				//---------------------------------- End of current individual shapefile processing --------------------------------------------//
			} //end shapefile loop

			WriteToScreenAndLog("================================================================================", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Shapefile processing complete. " + allShapeFiles.Length + " shapefile(s) processed.", configSettings["LogFilePath"], ref sbLog, true);
			WriteToScreenAndLog("Total polygons processed: " + statsTotalPolygonsProcessed.ToString(), configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog(statsTotalNoGeography.ToString() + " features were skipped due to having no geography", configSettings["LogFilePath"], ref sbLog);

			//----------- Trigger post polygon processing  ---------------//
			WriteToScreenAndLog("\r\n################################################################################", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("################################################################################", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("Starting SQL post polygon processing", configSettings["LogFilePath"], ref sbLog, true);
			using (SqlConnection conn2 = new SqlConnection(configSettings["DataBaseConnectionString"]))
			{
				try
				{
					SqlCommand command = new SqlCommand("spNPSUpdate", conn2);
					command.CommandTimeout = 72000; //in seconds
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.Add("@ProcessAction", SqlDbType.Char, 1).Value = 'P';
					command.Parameters.Add("@StartTime", SqlDbType.DateTime).Value =  startTime;
					command.Parameters.Add("@Status", SqlDbType.VarChar, 2000).Direction = ParameterDirection.Output;
					if (configSettings["FullMode"].ToUpper() == "YES")
					{
						command.Parameters.Add("@IsFullBuild", SqlDbType.Bit).Value = 1;
					}
					else
					{
						command.Parameters.Add("@IsFullBuild", SqlDbType.Bit).Value = 0;
					}
					conn2.Open();
					command.ExecuteNonQuery();
					sqlStatus = command.Parameters["@Status"].Value.ToString();
					if (sqlStatus != "success")
					{
						throw new Exception(sqlStatus);
					}

					//Get the SQL log.
					WriteToScreenAndLog("\r\nSQL log of post polygon processing:", configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog(GetDBLog(configSettings["DataBaseConnectionString"], ref lastUpdateID).ToString(), configSettings["LogFilePath"], ref sbLog);

				}
				catch (Exception ex)
				{
					WriteToScreenAndLog(GetDBLog(configSettings["DataBaseConnectionString"], ref lastUpdateID).ToString(), configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog("Error during SQL post polygon processing.", configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
					emailBody = "NPSUpdate - Error during SQL post polygon processing.\r\n";
					emailBody += "Error message: " + ex.Message;
					emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
					sendInformationEmail(emailBody, "ERROR 13: NPSUpdate - Error during SQL post polygon processing.", emailRecipients, configSettings);
					Console.WriteLine("Press any key to exit."); Console.ReadKey();
					return 13;
				}
			}

			//Get final stats
			WriteToScreenAndLog("\r\n################################################################################", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("################################################################################", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("FINAL STATS:", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("\r\nDot Net Shapefile processing Stats. " + allShapeFiles.Length + " shapefile(s) processed.", configSettings["LogFilePath"], ref sbLog, true);
			WriteToScreenAndLog("Total polygons processed: " + statsTotalPolygonsProcessed.ToString(), configSettings["LogFilePath"], ref sbLog);

			WriteToScreenAndLog("\r\nSQL processing Stats:", configSettings["LogFilePath"], ref sbLog);
			WriteToScreenAndLog("NOTE: The Added, Deleted and Changed stats here may not add up to the total number of incoming records. This is because the incoming data may contain deletions of records that are already not prsent in our data." , configSettings["LogFilePath"], ref sbLog);
			StringBuilder finalStats = new StringBuilder();
			using (SqlConnection connection = new SqlConnection(configSettings["DataBaseConnectionString"]))
			{
				try
				{
					connection.Open();
					SqlCommand command = new SqlCommand("SELECT * FROM [StatsForLatestRun]", connection);
					command.CommandTimeout = 72000; //in seconds
					using (SqlDataReader sqlreader = command.ExecuteReader())
					{
						while (sqlreader.Read())
						{
							WriteToScreenAndLog(sqlreader["StatDescription"] + " : " + sqlreader["Value"].ToString(), configSettings["LogFilePath"], ref sbLog);
							finalStats.AppendLine(sqlreader["StatDescription"] + " : " + sqlreader["Value"].ToString());
						}

					}
					//Stamp success message to SQL (used by the code to see when new data is available from the Land Registy)
					DateTime successDate = DateTime.Now;
					SqlCommand successcommand = new SqlCommand("INSERT INTO [UpdateResults] ([timeof], [description]) VALUES (@successDateTime, 'SUCCESS')", connection);
					successcommand.Parameters.AddWithValue("@successDateTime", successDate);
					successcommand.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					WriteToScreenAndLog("Error getting final stats and stamping success date.", configSettings["LogFilePath"], ref sbLog);
					WriteToScreenAndLog(ex.ToString(), configSettings["LogFilePath"], ref sbLog);
					emailBody = "NPSUpdate - Error getting final stats.\r\n";
					emailBody += "Error message: " + ex.Message;
					emailBody += "\r\nFor more information check the full log at: " + configSettings["LogFilePath"];
					sendInformationEmail(sbLog.ToString(), "ERROR 15: NPSUpdate - Error getting final stats.", emailRecipients, configSettings);
					Console.WriteLine("Press any key to exit."); Console.ReadKey();
					return 14;
				}
			}

			WriteToScreenAndLog("\r\nALL DONE. Press a key to exit.", configSettings["LogFilePath"], ref sbLog);
			emailBody = "NPSUpdate Complete - No Errors.\r\n";
			emailBody += "\r\nSee the full log at: " + configSettings["LogFilePath"] + "\r\n";
			emailBody += finalStats.ToString();
			sendInformationEmail(emailBody, "NPSUpdate - Update Complete.", emailRecipients, configSettings);

			Console.WriteLine("Press any key to exit."); Console.ReadKey();
			return 0; //Success		
		}

		//############################################## Supporting functions ###############################################
		//Function that gets the SQL processing log.
		public static StringBuilder GetDBLog(string connString, ref int lastUpdateResultsID)
		{
			StringBuilder returnText = new StringBuilder();
			using (SqlConnection conn = new SqlConnection(connString))
			{
				try
				{
					conn.Open();
					SqlCommand getSqlLogCommand = new SqlCommand("SELECT * FROM UpdateResults WHERE [id] > @lastID ORDER BY [id] ASC", conn);
					SqlParameter param = new SqlParameter();
					param.ParameterName = "@lastID";
					param.Value = lastUpdateResultsID;
					getSqlLogCommand.Parameters.Add(param);
					using (SqlDataReader sqlreader = getSqlLogCommand.ExecuteReader())
					{
						while (sqlreader.Read())
						{
							returnText.AppendLine(sqlreader["timeof"].ToString() + " : " + sqlreader["description"].ToString());
						}
					}

					SqlCommand command = new SqlCommand("SELECT MAX(ID) FROM [UpdateResults]", conn);
					command.CommandTimeout = 600;
					var serverRepsonse = command.ExecuteScalar().ToString();
					lastUpdateResultsID = Convert.ToInt32(command.ExecuteScalar());

					conn.Close();
				}
				catch  (Exception ex)
                {
					returnText.AppendLine(ex.Message);
                }
			}
			return returnText;
		}


		//Function that sends a block of shapefile data from RAM (a datatable) to SQL server.
		public static string SendBlockToDatabase(DataTable blockToSend, string dbConnection)
		{
			using (SqlConnection conn = new SqlConnection(dbConnection))
			{
				try
				{
					SqlCommand command = new SqlCommand("spNPSUpdate", conn);
					conn.Open();
					command.CommandTimeout = 7200; //in seconds
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.Add("@newPolygonData", SqlDbType.Structured).Value = blockToSend;
					command.Parameters.Add("@ProcessAction", SqlDbType.Char, 1).Value = 'B';
					command.Parameters.Add("@Status", SqlDbType.VarChar, 2000).Direction = ParameterDirection.Output;
					command.ExecuteNonQuery();
					string sqlStatus = command.Parameters["@Status"].Value.ToString();
					if (sqlStatus != "success")
					{
						throw new Exception(sqlStatus);
					}

				}
				catch (Exception ex)
				{
					return ex.Message; //Error passing new data to SQL Server
				}
				return "success";
			}
		}

		//Method for sending emails to admin recipients providing news about the update process.
		static string sendInformationEmail(string messageText, string messageSubject, List<string> emailRecipients, Dictionary<string, string> configSettings)
		{

			string returnMsg = "";
			MailMessage msg = new MailMessage();
			foreach (string emailRecipient in emailRecipients)
			{
				msg.To.Add(new MailAddress(emailRecipient));
			}

			msg.From = new MailAddress(configSettings["EmailFromAddress"], configSettings["EmailSubject"]);
			msg.Subject = messageSubject;
			msg.Body = messageText;
			msg.IsBodyHtml = false;

			SmtpClient mailClient = new SmtpClient();
			mailClient.UseDefaultCredentials = false;
			mailClient.Credentials = new System.Net.NetworkCredential(configSettings["SMTPCredentialsUN"], configSettings["SMTPCredentialsPWD"]);
			mailClient.Port = Convert.ToInt32(configSettings["SMTPPort"]);
			mailClient.Host = configSettings["SMTPHost"];
			mailClient.EnableSsl = true;
			try
			{
				mailClient.Send(msg);
				returnMsg = "success";
			}
			catch (Exception ex)
			{
				returnMsg = ex.ToString();
			}
			return returnMsg;
		}

		//Function that writes information to both the screen and to the application log. 
		public static void WriteToScreenAndLog(string textToWrite, string logPath, ref StringBuilder sbLog, bool addTime = false)
		{
			if (addTime)
			{
				textToWrite = DateTime.UtcNow.ToString("dd-MM-yyyy HH:mm:ss") + " : " + textToWrite;
			}
			Console.WriteLine(textToWrite);
			sbLog.AppendLine(textToWrite);

			try
			{
				StreamWriter sw = new StreamWriter(logPath, append: true);
				sw.WriteLine(textToWrite);
				sw.Close();
			}
			catch (Exception e)
			{
				Console.WriteLine("Log file Exception: " + e.Message);
				sbLog.AppendLine("Log file Exception: " + e.Message);
			}

		}


		//File column header definitions
		public enum NPS_PolygonInHeaders
		{
			InspireID,
			InspireIDPart,
			TitleNumber,
			InsertDate,
			UpdateDate,
			Rec_Status,
			SingleEasting,
			SingleNorthing,
			Area,
			Long,
			Lat,
			WKT4326,
			PolygonGeography,
			LatLng,
			WKT27700
		}

		public enum NPS_ShapefileDataRowHeaders
		{
			PolyID = 0,
			TitleNo = 1,
			InsertDate = 2,
			UpdateDate = 3,
			RecStatus = 4
		}
		public enum NPS_TenureFileHeaders
		{
			TITLE_NO = 0,
			ESTATE_INTRST_CODE = 1,
			CLASS_TITLE_CODE = 2,
			PEND_NT_CODE = 3,
			REC_STATUS = 4
		}

		public enum NPS_UPRNHeaders
		{
			TITLE_NO = 0,
			UPRN = 1
		}


		// Datatable definitions
		public static DataTable GetPolygonDatatable()
		{
			// Create an output .NET DataTable for population with converted data and export to SQL server.
			DataTable dtPolygonTable = new DataTable();
			DataColumn column;

			column = new DataColumn();
			column.DataType = Type.GetType("System.Int64");
			column.ColumnName = "InspireID";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.Int32");
			column.ColumnName = "InspireIDPart";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.String");
			column.ColumnName = "TitleNumber";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.DateTime");
			column.ColumnName = "INSERT";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.DateTime");
			column.ColumnName = "UPDATE";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.Char");
			column.ColumnName = "REC_STATUS";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.Double");
			column.ColumnName = "SingleEasting";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.Double");
			column.ColumnName = "SingleNorthing";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.Double");
			column.ColumnName = "Area";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.Double");
			column.ColumnName = "Long";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.Double");
			column.ColumnName = "Lat";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.String");
			column.ColumnName = "WKT4326";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.String");
			column.ColumnName = "PolygonGeography";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.String");
			column.ColumnName = "LatLng";
			dtPolygonTable.Columns.Add(column);

			column = new DataColumn();
			column.DataType = Type.GetType("System.String");
			column.ColumnName = "WKT27700";
			dtPolygonTable.Columns.Add(column);

			return dtPolygonTable;
		}

	}
}

